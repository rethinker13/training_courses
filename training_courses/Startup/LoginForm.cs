﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class LoginForm : Form
    {
        private string adminLogin = "SYSDBA";
        private string adminPassword = "masterkey";

        public string user { get; private set; }
        public string password { get; private set; }
        public string dbPath { get; private set; }
        public string roleName { get; private set; }

        public LoginForm ()
        {
            InitializeComponent();

            // Прочитать настройки подключения к БД
            Global.DB_CS = Global.LoadConnectionSettings(Global.CONNECTION_SETTINGS_FILE);
            // Попытка подключиться к БД
            if (!Global.SetConnectionWithDB())
            {
                // Показать панель настройки подключения к БД
                Global.ShowConnectionSettings();
            }
        }

        private void btnOk_Click (object sender, EventArgs e)
        {
            this.user = txtUser.Text;
            this.password = txtPassword.Text;
            this.dbPath = Global.DB_CS.DataBaseName;//Path.Combine("C:\\db\\", "training.fdb;");
            QueryDatabase.buildConnectionString(adminLogin, adminPassword, dbPath);

            StringBuilder sb = new StringBuilder();
            sb.Append("select user_name, role_id, user_password_hash from t_user where upper(t_user.user_name) = ");
            sb.Append("'");
            sb.Append(this.user.ToUpper());
            sb.Append("'");
            DataTable data = QueryDatabase.getData(sb.ToString());

            if (data == null)
                return;

            if (data.Rows.Count == 0)
            {
                MessageBox.Show("Неверное имя пользователя или пароль",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow row = data.Rows[0];

            if (String.Equals(row["user_name"].ToString().ToUpper(), "SYSDBA"))
            {
                roleName = "Администратор";
                AccessLevel.level = AccessLevel.Level.Admin;
                this.DialogResult = DialogResult.OK;
                //this.Close();
                runMain();
                return;
            }

            long roleId = (long)row["role_id"];
            string savedPasswordHash = (string)row["user_password_hash"];

            if (!verifyPassword(this.password, savedPasswordHash))
            {
                MessageBox.Show("Неверное имя пользователя или пароль",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            sb.Clear();
            sb.Append("select role_name from t_role where t_role.role_id = ");
            sb.Append("'");
            sb.Append(roleId);
            sb.Append("'");
            data = QueryDatabase.getData(sb.ToString());
            roleName = (string)data.Rows[0]["role_name"];
            switch (roleId)
            {
                case 1:
                    AccessLevel.level = AccessLevel.Level.Admin;
                    break;
                case 2:
                    AccessLevel.level = AccessLevel.Level.Courses;
                    break;
                case 3:
                    AccessLevel.level = AccessLevel.Level.Students;
                    break;
                case 4:
                    AccessLevel.level = AccessLevel.Level.Manager;
                    break;
                case 5:
                    AccessLevel.level = AccessLevel.Level.Engineer;
                    break;
                case 6:
                    AccessLevel.level = AccessLevel.Level.Accounting;
                    break;
                default:
                    MessageBox.Show("Несуществующая или неизвестная роль",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.DialogResult = DialogResult.Cancel;
                    return;
            }

            this.DialogResult = DialogResult.OK;
            runMain();
        }

        private void runMain ()
        {
            Global.user = new User(this.user, DataProtection.Protect(Encoding.UTF8.GetBytes(this.password)));
            this.Hide();
            FrmMain main = new FrmMain(this.user, this.dbPath, this.roleName);
            main.ShowDialog();
            if (!main.waitLogin)
                this.Close();
            this.Show();
        }

        private bool verifyPassword (string password, string passwordHash)
        {
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(passwordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
            return true;
        }

        private void btnCancel_Click (object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void LoginForm_KeyPress (object sender, KeyPressEventArgs e)
        {

        }
    }
}
