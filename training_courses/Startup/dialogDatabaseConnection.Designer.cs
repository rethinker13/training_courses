﻿namespace training_courses
{
    partial class dialogDatabaseConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dialogDatabaseConnection));
            this.tstrCommandMenu = new System.Windows.Forms.ToolStrip();
            this.tsbtnOK = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCancel = new System.Windows.Forms.ToolStripButton();
            this.lblServerPort = new System.Windows.Forms.Label();
            this.txtServerPort = new System.Windows.Forms.TextBox();
            this.txtServerPassword = new System.Windows.Forms.TextBox();
            this.txtServerAddress = new System.Windows.Forms.TextBox();
            this.lblServerLogin = new System.Windows.Forms.Label();
            this.lblDataBaseName = new System.Windows.Forms.Label();
            this.lblServerPassword = new System.Windows.Forms.Label();
            this.lblServerAddress = new System.Windows.Forms.Label();
            this.txtServerLogin = new System.Windows.Forms.TextBox();
            this.txtDataBaseName = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.tstrCommandMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tstrCommandMenu
            // 
            this.tstrCommandMenu.BackColor = System.Drawing.Color.White;
            this.tstrCommandMenu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tstrCommandMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tstrCommandMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tstrCommandMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnOK,
            this.tsbtnCancel});
            this.tstrCommandMenu.Location = new System.Drawing.Point(0, 132);
            this.tstrCommandMenu.Name = "tstrCommandMenu";
            this.tstrCommandMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tstrCommandMenu.Size = new System.Drawing.Size(418, 27);
            this.tstrCommandMenu.TabIndex = 0;
            // 
            // tsbtnOK
            // 
            this.tsbtnOK.Image = global::training_courses.Properties.Resources.ok;
            this.tsbtnOK.Name = "tsbtnOK";
            this.tsbtnOK.Size = new System.Drawing.Size(47, 24);
            this.tsbtnOK.Text = "ОК";
            this.tsbtnOK.Click += new System.EventHandler(this.tsbtnOK_Click);
            // 
            // tsbtnCancel
            // 
            this.tsbtnCancel.Image = global::training_courses.Properties.Resources.cancel;
            this.tsbtnCancel.Name = "tsbtnCancel";
            this.tsbtnCancel.Size = new System.Drawing.Size(73, 24);
            this.tsbtnCancel.Text = "Отмена";
            this.tsbtnCancel.Click += new System.EventHandler(this.tsbtnCancel_Click);
            // 
            // lblServerPort
            // 
            this.lblServerPort.AutoSize = true;
            this.lblServerPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblServerPort.ForeColor = System.Drawing.Color.Black;
            this.lblServerPort.Location = new System.Drawing.Point(89, 102);
            this.lblServerPort.Name = "lblServerPort";
            this.lblServerPort.Size = new System.Drawing.Size(32, 13);
            this.lblServerPort.TabIndex = 78;
            this.lblServerPort.Text = "Порт";
            // 
            // txtServerPort
            // 
            this.txtServerPort.BackColor = System.Drawing.Color.White;
            this.txtServerPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtServerPort.Location = new System.Drawing.Point(127, 98);
            this.txtServerPort.MaxLength = 15;
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Size = new System.Drawing.Size(279, 20);
            this.txtServerPort.TabIndex = 73;
            this.txtServerPort.Text = "3050";
            // 
            // txtServerPassword
            // 
            this.txtServerPassword.BackColor = System.Drawing.Color.White;
            this.txtServerPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtServerPassword.Location = new System.Drawing.Point(127, 52);
            this.txtServerPassword.MaxLength = 15;
            this.txtServerPassword.Name = "txtServerPassword";
            this.txtServerPassword.PasswordChar = '*';
            this.txtServerPassword.Size = new System.Drawing.Size(279, 20);
            this.txtServerPassword.TabIndex = 71;
            this.txtServerPassword.UseSystemPasswordChar = true;
            // 
            // txtServerAddress
            // 
            this.txtServerAddress.BackColor = System.Drawing.Color.White;
            this.txtServerAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtServerAddress.Location = new System.Drawing.Point(127, 7);
            this.txtServerAddress.MaxLength = 15;
            this.txtServerAddress.Name = "txtServerAddress";
            this.txtServerAddress.Size = new System.Drawing.Size(279, 20);
            this.txtServerAddress.TabIndex = 69;
            this.txtServerAddress.Text = "localhost";
            // 
            // lblServerLogin
            // 
            this.lblServerLogin.AutoSize = true;
            this.lblServerLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblServerLogin.ForeColor = System.Drawing.Color.Black;
            this.lblServerLogin.Location = new System.Drawing.Point(83, 33);
            this.lblServerLogin.Name = "lblServerLogin";
            this.lblServerLogin.Size = new System.Drawing.Size(38, 13);
            this.lblServerLogin.TabIndex = 75;
            this.lblServerLogin.Text = "Логин";
            // 
            // lblDataBaseName
            // 
            this.lblDataBaseName.AutoSize = true;
            this.lblDataBaseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDataBaseName.ForeColor = System.Drawing.Color.Black;
            this.lblDataBaseName.Location = new System.Drawing.Point(98, 78);
            this.lblDataBaseName.Name = "lblDataBaseName";
            this.lblDataBaseName.Size = new System.Drawing.Size(23, 13);
            this.lblDataBaseName.TabIndex = 77;
            this.lblDataBaseName.Text = "БД";
            // 
            // lblServerPassword
            // 
            this.lblServerPassword.AutoSize = true;
            this.lblServerPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblServerPassword.ForeColor = System.Drawing.Color.Black;
            this.lblServerPassword.Location = new System.Drawing.Point(76, 55);
            this.lblServerPassword.Name = "lblServerPassword";
            this.lblServerPassword.Size = new System.Drawing.Size(45, 13);
            this.lblServerPassword.TabIndex = 76;
            this.lblServerPassword.Text = "Пароль";
            // 
            // lblServerAddress
            // 
            this.lblServerAddress.AutoSize = true;
            this.lblServerAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblServerAddress.ForeColor = System.Drawing.Color.Black;
            this.lblServerAddress.Location = new System.Drawing.Point(8, 10);
            this.lblServerAddress.Name = "lblServerAddress";
            this.lblServerAddress.Size = new System.Drawing.Size(114, 13);
            this.lblServerAddress.TabIndex = 74;
            this.lblServerAddress.Text = "Адрес / имя сервера";
            // 
            // txtServerLogin
            // 
            this.txtServerLogin.BackColor = System.Drawing.Color.White;
            this.txtServerLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtServerLogin.Location = new System.Drawing.Point(127, 30);
            this.txtServerLogin.MaxLength = 15;
            this.txtServerLogin.Name = "txtServerLogin";
            this.txtServerLogin.Size = new System.Drawing.Size(279, 20);
            this.txtServerLogin.TabIndex = 70;
            this.txtServerLogin.Text = "SYSDBA";
            // 
            // txtDataBaseName
            // 
            this.txtDataBaseName.BackColor = System.Drawing.Color.White;
            this.txtDataBaseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDataBaseName.Location = new System.Drawing.Point(127, 75);
            this.txtDataBaseName.MaxLength = 2500;
            this.txtDataBaseName.Name = "txtDataBaseName";
            this.txtDataBaseName.Size = new System.Drawing.Size(198, 20);
            this.txtDataBaseName.TabIndex = 72;
            this.txtDataBaseName.Text = "C:\\db\\training.fdb";
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(331, 73);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 79;
            this.btnOpen.Text = "Открыть";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // dialogDatabaseConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(418, 159);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.lblServerPort);
            this.Controls.Add(this.txtServerPort);
            this.Controls.Add(this.txtServerPassword);
            this.Controls.Add(this.txtServerAddress);
            this.Controls.Add(this.lblServerLogin);
            this.Controls.Add(this.lblDataBaseName);
            this.Controls.Add(this.lblServerPassword);
            this.Controls.Add(this.lblServerAddress);
            this.Controls.Add(this.txtServerLogin);
            this.Controls.Add(this.txtDataBaseName);
            this.Controls.Add(this.tstrCommandMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dialogDatabaseConnection";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подключение к источнику данных";
            this.tstrCommandMenu.ResumeLayout(false);
            this.tstrCommandMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tstrCommandMenu;
        private System.Windows.Forms.ToolStripButton tsbtnCancel;
        private System.Windows.Forms.ToolStripButton tsbtnOK;
        private System.Windows.Forms.Label lblServerPort;
        private System.Windows.Forms.TextBox txtServerPort;
        private System.Windows.Forms.TextBox txtServerPassword;
        private System.Windows.Forms.TextBox txtServerAddress;
        private System.Windows.Forms.Label lblServerLogin;
        private System.Windows.Forms.Label lblDataBaseName;
        private System.Windows.Forms.Label lblServerPassword;
        private System.Windows.Forms.Label lblServerAddress;
        private System.Windows.Forms.TextBox txtServerLogin;
        private System.Windows.Forms.TextBox txtDataBaseName;
        private System.Windows.Forms.Button btnOpen;
    }
}