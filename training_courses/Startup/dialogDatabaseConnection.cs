﻿using System;
using System.Windows.Forms;

namespace training_courses
{
    /// <summary>
    /// Форма подключения к БД
    /// </summary>
    public partial class dialogDatabaseConnection : Form
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public dialogDatabaseConnection()
        {
            // Создание и инициализация компонентов
            InitializeComponent();
            // Отображение параметров подключения
            txtServerAddress.Text = Global.DB_CS.ServerAddress;
            txtServerLogin.Text = Global.DB_CS.ServerLogin;
            txtServerPassword.Text = Global.DB_CS.ServerPassword;
            txtDataBaseName.Text = Global.DB_CS.DataBaseName;
            txtServerPort.Text = Global.DB_CS.ServerPort;
        }
        /// <summary>
        /// Применение диалога
        /// </summary>
        /// <param name="sender">Объект, вызвавший событие</param>
        /// <param name="e">Параметры события</param>
        private void tsbtnOK_Click(object sender, EventArgs e)
        {
            // Установка новых параметров соединения
            Global.DB_CS.Set(txtServerAddress.Text, txtServerLogin.Text, txtServerPassword.Text,
                             txtDataBaseName.Text, txtServerPort.Text);
            // Попытка установить соединение
            if (Global.SetConnectionWithDB())
            {
                // Сохранить в файл
                Global.SaveConnectionSettings(Global.CONNECTION_SETTINGS_FILE);
                // Перейти к авторизации
                //Global.ShowLoginForm();
                // Закрыть форму
                Close();             
            }            
        }
        /// <summary>
        /// Отмена диалога
        /// </summary>
        /// <param name="sender">Объект, вызвавший событие</param>
        /// <param name="e">Параметры события</param>
        private void tsbtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "Firebird database (*.fdb)|*.fdb|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    txtDataBaseName.Text = openFileDialog.FileName;
                }
            }
        }
    }
}