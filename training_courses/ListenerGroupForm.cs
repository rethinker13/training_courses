﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class ListenerGroupForm : Form
    {

        public ListenerGroupForm ()
        {
            InitializeComponent();

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                tbExcel.Visible = false;
            }
            
        }

        private void t_GROUP_LISTENERBindingNavigatorSaveItem_Click (object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.t_GROUP_LISTENERBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dBDataSet);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    System.Windows.Forms.Application.ProductName, ex.Message);
                message.ShowDialog();
            }

            ListenerGroupForm_Load(sender, e);
        }

        private void ListenerGroupForm_Load (object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_GROUP_LISTENER' table. You can move, or remove it, as needed.
            this.t_GROUP_LISTENERTableAdapter.Fill(this.dBDataSet.T_GROUP_LISTENER);
            if (AccessLevel.level == AccessLevel.Level.Courses)
            {
                tbExcel.Visible = false;
            }
        }

        private void tsEditLGroup_Click (object sender, EventArgs e)
        {
            if (t_GROUP_LISTENERDataGridView.SelectedCells.Count != 0)
            {
                if (t_GROUP_LISTENERDataGridView.SelectedCells[0].RowIndex != -1)
                {
                    int idx = t_GROUP_LISTENERDataGridView.SelectedCells[0].RowIndex;
                    if (t_GROUP_LISTENERDataGridView.Rows[idx].Cells[0].Value != null && t_GROUP_LISTENERDataGridView.Rows[idx].Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        long groupID = (long)t_GROUP_LISTENERDataGridView.Rows[idx].Cells[0].Value;
                        string groupName = t_GROUP_LISTENERDataGridView.Rows[idx].Cells[1].Value.ToString();
                        ShortListenerGroupForm listenerForm = new ShortListenerGroupForm(groupID, groupName);
                        foreach (Form form in ((FrmMain)this.ParentForm).MdiChildren)
                        {
                            if (form.GetType() == typeof(ShortListenerGroupForm))
                                listenerForm = (ShortListenerGroupForm)form;
                        }
                        listenerForm.Text = "Подготовительная группа " + groupName;
                        listenerForm.MdiParent = (FrmMain)this.ParentForm;
                        listenerForm.Show();
                    }
                }
            }
        }

        private void tbExcel_Click (object sender, EventArgs e)
        {
            if (t_GROUP_LISTENERDataGridView.CurrentRow != null)
            {
                if (t_GROUP_LISTENERDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (t_GROUP_LISTENERDataGridView.CurrentRow.Cells[0].Value != null && t_GROUP_LISTENERDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        long groupID = (long)t_GROUP_LISTENERDataGridView.CurrentRow.Cells[0].Value;
                        if (t_GROUP_LISTENERDataGridView.CurrentRow.Cells[2].Value == null)
                            return;
                        byte[] xlsBin = (byte[])t_GROUP_LISTENERDataGridView.CurrentRow.Cells[2].Value;

                        string group_name = t_GROUP_LISTENERDataGridView.CurrentRow.Cells[1].Value.ToString();
                        if (!Directory.Exists(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Подготовительные группы\\"))
                            Directory.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Подготовительные группы\\");
                        if (!Directory.Exists(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Подготовительные группы\\" +
                            group_name + "\\"))
                            Directory.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Подготовительные группы\\" +
                            group_name + "\\");

                        string tmpFile = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Подготовительные группы\\" +
                             group_name + "\\Группа " + group_name + ".xls";
                        File.WriteAllBytes(tmpFile, xlsBin);

                        Process.Start(tmpFile);
                    }
                }
            }
        }
    }
}
