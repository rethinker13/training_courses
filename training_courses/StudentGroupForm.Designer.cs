﻿namespace training_courses
{
    partial class StudentGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudentGroupForm));
            this.dBDataSet = new training_courses.DBDataSet();
            this.t_GROUPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.t_GROUPTableAdapter = new training_courses.DBDataSetTableAdapters.T_GROUPTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.t_GROUPBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.t_GROUPBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tsEditGroup = new System.Windows.Forms.ToolStripButton();
            this.tbExcel = new System.Windows.Forms.ToolStripButton();
            this.t_GROUPDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.IS_MASTERS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_SVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_GROUP_EXT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUPBindingNavigator)).BeginInit();
            this.t_GROUPBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUPDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // t_GROUPBindingSource
            // 
            this.t_GROUPBindingSource.DataMember = "T_GROUP";
            this.t_GROUPBindingSource.DataSource = this.dBDataSet;
            // 
            // t_GROUPTableAdapter
            // 
            this.t_GROUPTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = this.t_GROUPTableAdapter;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // t_GROUPBindingNavigator
            // 
            this.t_GROUPBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.t_GROUPBindingNavigator.BindingSource = this.t_GROUPBindingSource;
            this.t_GROUPBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.t_GROUPBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.t_GROUPBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.t_GROUPBindingNavigatorSaveItem,
            this.tsEditGroup,
            this.tbExcel});
            this.t_GROUPBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.t_GROUPBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.t_GROUPBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.t_GROUPBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.t_GROUPBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.t_GROUPBindingNavigator.Name = "t_GROUPBindingNavigator";
            this.t_GROUPBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.t_GROUPBindingNavigator.Size = new System.Drawing.Size(774, 25);
            this.t_GROUPBindingNavigator.TabIndex = 0;
            this.t_GROUPBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // t_GROUPBindingNavigatorSaveItem
            // 
            this.t_GROUPBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.t_GROUPBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("t_GROUPBindingNavigatorSaveItem.Image")));
            this.t_GROUPBindingNavigatorSaveItem.Name = "t_GROUPBindingNavigatorSaveItem";
            this.t_GROUPBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.t_GROUPBindingNavigatorSaveItem.Text = "Save Data";
            this.t_GROUPBindingNavigatorSaveItem.Click += new System.EventHandler(this.t_GROUPBindingNavigatorSaveItem_Click);
            // 
            // tsEditGroup
            // 
            this.tsEditGroup.Image = global::training_courses.Properties.Resources.edit_128;
            this.tsEditGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsEditGroup.Name = "tsEditGroup";
            this.tsEditGroup.Size = new System.Drawing.Size(186, 22);
            this.tsEditGroup.Text = "Просмотр и редактирование";
            this.tsEditGroup.Click += new System.EventHandler(this.tsEditGroup_Click);
            // 
            // tbExcel
            // 
            this.tbExcel.Image = global::training_courses.Properties.Resources.excel;
            this.tbExcel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbExcel.Name = "tbExcel";
            this.tbExcel.Size = new System.Drawing.Size(164, 22);
            this.tbExcel.Text = "Просмотр Excel таблицы";
            this.tbExcel.ToolTipText = "Просмотр Excel таблицы";
            this.tbExcel.Click += new System.EventHandler(this.tbExcel_Click);
            // 
            // t_GROUPDataGridView
            // 
            this.t_GROUPDataGridView.AutoGenerateColumns = false;
            this.t_GROUPDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.t_GROUPDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.t_GROUPDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewImageColumn1,
            this.IS_MASTERS,
            this.IS_SVE,
            this.T_GROUP_EXT});
            this.t_GROUPDataGridView.DataSource = this.t_GROUPBindingSource;
            this.t_GROUPDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t_GROUPDataGridView.Location = new System.Drawing.Point(0, 25);
            this.t_GROUPDataGridView.Name = "t_GROUPDataGridView";
            this.t_GROUPDataGridView.Size = new System.Drawing.Size(774, 587);
            this.t_GROUPDataGridView.TabIndex = 1;
            this.t_GROUPDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.t_GROUPDataGridView_CellBeginEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "GR_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "GR_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "GR_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Группа";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.DataPropertyName = "GR_XLS";
            this.dataGridViewImageColumn1.HeaderText = "Excel таблица";
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Visible = false;
            // 
            // IS_MASTERS
            // 
            this.IS_MASTERS.DataPropertyName = "IS_MASTERS";
            this.IS_MASTERS.HeaderText = "IS_MASTERS";
            this.IS_MASTERS.Name = "IS_MASTERS";
            this.IS_MASTERS.Visible = false;
            // 
            // IS_SVE
            // 
            this.IS_SVE.DataPropertyName = "IS_SVE";
            this.IS_SVE.HeaderText = "IS_SVE";
            this.IS_SVE.Name = "IS_SVE";
            this.IS_SVE.Visible = false;
            // 
            // T_GROUP_EXT
            // 
            this.T_GROUP_EXT.DataPropertyName = "T_GROUP_EXT";
            this.T_GROUP_EXT.HeaderText = "T_GROUP_EXT";
            this.T_GROUP_EXT.Name = "T_GROUP_EXT";
            this.T_GROUP_EXT.Visible = false;
            // 
            // StudentGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 612);
            this.Controls.Add(this.t_GROUPDataGridView);
            this.Controls.Add(this.t_GROUPBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StudentGroupForm";
            this.Text = "Группы заочного обучения";
            this.Load += new System.EventHandler(this.StudentGroupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUPBindingNavigator)).EndInit();
            this.t_GROUPBindingNavigator.ResumeLayout(false);
            this.t_GROUPBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUPDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource t_GROUPBindingSource;
        private DBDataSetTableAdapters.T_GROUPTableAdapter t_GROUPTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator t_GROUPBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton t_GROUPBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView t_GROUPDataGridView;
        private System.Windows.Forms.ToolStripButton tsEditGroup;
        private System.Windows.Forms.ToolStripButton tbExcel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IS_MASTERS;
        private System.Windows.Forms.DataGridViewTextBoxColumn IS_SVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_GROUP_EXT;
    }
}