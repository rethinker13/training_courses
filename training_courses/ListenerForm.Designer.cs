﻿namespace training_courses
{
    partial class ListenerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListenerForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.v_LISTENERSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.v_LISTENERSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dBDataSet = new training_courses.DBDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbAddListener = new System.Windows.Forms.ToolStripButton();
            this.tbEditListener = new System.Windows.Forms.ToolStripButton();
            this.tbDeleteListener = new System.Windows.Forms.ToolStripButton();
            this.tbExcelExport = new System.Windows.Forms.ToolStripButton();
            this.tbReport = new System.Windows.Forms.ToolStripButton();
            this.tbReceipt = new System.Windows.Forms.ToolStripButton();
            this.v_LISTENERSDataGridView = new System.Windows.Forms.DataGridView();
            this.L_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.v_LISTENERSTableAdapter = new training_courses.DBDataSetTableAdapters.V_LISTENERSTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            ((System.ComponentModel.ISupportInitialize)(this.v_LISTENERSBindingNavigator)).BeginInit();
            this.v_LISTENERSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_LISTENERSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_LISTENERSDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // v_LISTENERSBindingNavigator
            // 
            this.v_LISTENERSBindingNavigator.AddNewItem = null;
            this.v_LISTENERSBindingNavigator.BindingSource = this.v_LISTENERSBindingSource;
            this.v_LISTENERSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.v_LISTENERSBindingNavigator.DeleteItem = null;
            this.v_LISTENERSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.tbAddListener,
            this.tbEditListener,
            this.tbDeleteListener,
            this.tbExcelExport,
            this.tbReport,
            this.tbReceipt});
            this.v_LISTENERSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.v_LISTENERSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.v_LISTENERSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.v_LISTENERSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.v_LISTENERSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.v_LISTENERSBindingNavigator.Name = "v_LISTENERSBindingNavigator";
            this.v_LISTENERSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.v_LISTENERSBindingNavigator.Size = new System.Drawing.Size(1264, 25);
            this.v_LISTENERSBindingNavigator.TabIndex = 0;
            this.v_LISTENERSBindingNavigator.Text = "bindingNavigator1";
            // 
            // v_LISTENERSBindingSource
            // 
            this.v_LISTENERSBindingSource.DataMember = "V_LISTENERS";
            this.v_LISTENERSBindingSource.DataSource = this.dBDataSet;
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbAddListener
            // 
            this.tbAddListener.Image = global::training_courses.Properties.Resources.if_sign_add_299068;
            this.tbAddListener.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbAddListener.Name = "tbAddListener";
            this.tbAddListener.Size = new System.Drawing.Size(79, 22);
            this.tbAddListener.Text = "Добавить";
            this.tbAddListener.Click += new System.EventHandler(this.tbAddListener_Click);
            // 
            // tbEditListener
            // 
            this.tbEditListener.Image = global::training_courses.Properties.Resources.edit_128;
            this.tbEditListener.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbEditListener.Name = "tbEditListener";
            this.tbEditListener.Size = new System.Drawing.Size(107, 22);
            this.tbEditListener.Text = "Редактировать";
            this.tbEditListener.Click += new System.EventHandler(this.tbEditListener_Click);
            // 
            // tbDeleteListener
            // 
            this.tbDeleteListener.Image = global::training_courses.Properties.Resources.error;
            this.tbDeleteListener.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbDeleteListener.Name = "tbDeleteListener";
            this.tbDeleteListener.Size = new System.Drawing.Size(71, 22);
            this.tbDeleteListener.Text = "Удалить";
            this.tbDeleteListener.Click += new System.EventHandler(this.tbDeleteListener_Click);
            // 
            // tbExcelExport
            // 
            this.tbExcelExport.Image = global::training_courses.Properties.Resources.excel;
            this.tbExcelExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbExcelExport.Name = "tbExcelExport";
            this.tbExcelExport.Size = new System.Drawing.Size(123, 22);
            this.tbExcelExport.Text = "Сохранить в Excel";
            this.tbExcelExport.Click += new System.EventHandler(this.tbExcelExport_Click);
            // 
            // tbReport
            // 
            this.tbReport.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.tbReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbReport.Name = "tbReport";
            this.tbReport.Size = new System.Drawing.Size(159, 22);
            this.tbReport.Text = "Сформировать договор";
            this.tbReport.ToolTipText = "Сформировать договор";
            this.tbReport.Click += new System.EventHandler(this.tbReport_Click);
            // 
            // tbReceipt
            // 
            this.tbReceipt.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.tbReceipt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbReceipt.Name = "tbReceipt";
            this.tbReceipt.Size = new System.Drawing.Size(175, 22);
            this.tbReceipt.Text = "Сформировать квитанцию";
            this.tbReceipt.Click += new System.EventHandler(this.tbReceipt_Click);
            // 
            // v_LISTENERSDataGridView
            // 
            this.v_LISTENERSDataGridView.AllowUserToAddRows = false;
            this.v_LISTENERSDataGridView.AllowUserToDeleteRows = false;
            this.v_LISTENERSDataGridView.AutoGenerateColumns = false;
            this.v_LISTENERSDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.v_LISTENERSDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.v_LISTENERSDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.v_LISTENERSDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.v_LISTENERSDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.v_LISTENERSDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.v_LISTENERSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.v_LISTENERSDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.L_ID,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            this.v_LISTENERSDataGridView.DataSource = this.v_LISTENERSBindingSource;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LimeGreen;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.v_LISTENERSDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.v_LISTENERSDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.v_LISTENERSDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.v_LISTENERSDataGridView.EnableHeadersVisualStyles = false;
            this.v_LISTENERSDataGridView.GridColor = System.Drawing.Color.Silver;
            this.v_LISTENERSDataGridView.Location = new System.Drawing.Point(0, 25);
            this.v_LISTENERSDataGridView.Name = "v_LISTENERSDataGridView";
            this.v_LISTENERSDataGridView.RowHeadersVisible = false;
            this.v_LISTENERSDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.v_LISTENERSDataGridView.Size = new System.Drawing.Size(1264, 656);
            this.v_LISTENERSDataGridView.TabIndex = 1;
            // 
            // L_ID
            // 
            this.L_ID.DataPropertyName = "L_ID";
            this.L_ID.HeaderText = "L_ID";
            this.L_ID.Name = "L_ID";
            this.L_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "P_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "P_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "S_ID";
            this.dataGridViewTextBoxColumn2.HeaderText = "S_ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "P_LAST_NAME";
            this.dataGridViewTextBoxColumn3.HeaderText = "Фамилия";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "P_FIRST_NAME";
            this.dataGridViewTextBoxColumn4.HeaderText = "Имя";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "P_SECOND_NAME";
            this.dataGridViewTextBoxColumn5.HeaderText = "Отчество";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "L_EDU_PLACE";
            this.dataGridViewTextBoxColumn6.HeaderText = "Учебное заведение";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "L_CONTRACT_NUMBER";
            this.dataGridViewTextBoxColumn7.HeaderText = "№ Контракта";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "L_TIME";
            this.dataGridViewTextBoxColumn8.HeaderText = "Кол-во часов";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "L_REAL_TIME";
            this.dataGridViewTextBoxColumn9.HeaderText = "Кол-во посещенных часов";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "GL_ID";
            this.dataGridViewTextBoxColumn10.HeaderText = "GL_ID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "S_NAME";
            this.dataGridViewTextBoxColumn11.HeaderText = "Предмет";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // v_LISTENERSTableAdapter
            // 
            this.v_LISTENERSTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // ListenerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.v_LISTENERSDataGridView);
            this.Controls.Add(this.v_LISTENERSBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ListenerForm";
            this.Text = "Listeners";
            this.Load += new System.EventHandler(this.Listeners_Load);
            ((System.ComponentModel.ISupportInitialize)(this.v_LISTENERSBindingNavigator)).EndInit();
            this.v_LISTENERSBindingNavigator.ResumeLayout(false);
            this.v_LISTENERSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_LISTENERSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_LISTENERSDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource v_LISTENERSBindingSource;
        private DBDataSetTableAdapters.V_LISTENERSTableAdapter v_LISTENERSTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator v_LISTENERSBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.DataGridView v_LISTENERSDataGridView;
        private System.Windows.Forms.ToolStripButton tbAddListener;
        private System.Windows.Forms.ToolStripButton tbEditListener;
        private System.Windows.Forms.ToolStripButton tbDeleteListener;
        private System.Windows.Forms.ToolStripButton tbReport;
        private System.Windows.Forms.DataGridViewTextBoxColumn L_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.ToolStripButton tbExcelExport;
        private System.Windows.Forms.ToolStripButton tbReceipt;
    }
}