﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class ContractForm : Form
    {
        private DBDataSetTableAdapters.V_LISTENER_SUBJECTTableAdapter listenerAdapter;

        private int listenerID;
        private int personID;
        private string edu;
        private string firstName;
        private string lastName;
        private string secondName;
        private string contract;

        private List<KeyValuePair<int, double>> sub_prices; 

        public ContractForm ()
        {
            InitializeComponent();
            sub_prices = new List<KeyValuePair<int, double>>();
        }

        public ContractForm (int listenerID, int personID, string lastName, string firstName, string secondName, string edu, string contract) : this()
        {
            this.listenerID = listenerID;
            this.personID = personID;
            this.lastName = lastName;
            this.firstName = firstName;
            this.secondName = secondName;
            this.edu = edu;
            this.contract = contract;

            listenerAdapter = new DBDataSetTableAdapters.V_LISTENER_SUBJECTTableAdapter();
            listenerAdapter.ClearBeforeFill = true;

            listenerAdapter.Fill(dBDataSet.V_LISTENER_SUBJECT);

            this.Validate();

            this.dBDataSet.T_LISTENER.AcceptChanges();

            txtNumber.Text = contract;
        }

        private void ContractForm_Load (object sender, EventArgs e)
        {          
            
        }

        private void btnRebuild_Click (object sender, EventArgs e)
        {
            make_contract();
        }

        private void make_contract()
        {
            if (String.IsNullOrEmpty(txtParent.Text))
            {
                MessageBox.Show("Введите корректное ФИО заказчика!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtNumber.Text))
            {
                MessageBox.Show("Введите номер договора!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ((int)numContribution.Value <= 0)
            {
                MessageBox.Show("Введите корректную сумму взноса!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String file = File.ReadAllText("reports\\train_doc.rdl");

            file = file.Replace("_REP_NUMBER_", txtNumber.Text);

            file = file.Replace("_REP_DATE_", datePc.Value.ToShortDateString());

            file = file.Replace("_REP_PARENT_", txtParent.Text);

            StringBuilder sb = new StringBuilder();
            sb.Append(lastName);
            sb.Append(" ");
            sb.Append(firstName);
            sb.Append(" ");
            sb.Append(secondName);

            file = file.Replace("_REP_NAME_", sb.ToString());

            file = file.Replace("_REP_EDU_", edu);

            DBDataSet.V_LISTENER_SUBJECTDataTable dataByLID = listenerAdapter.GetDataByPID(personID);

            int sub_idx = 0;
            for (int i = 0; i < dataByLID.Rows.Count && i < 7; i++)
            {
                DBDataSet.V_LISTENER_SUBJECTRow row = (DBDataSet.V_LISTENER_SUBJECTRow)dataByLID.Rows[i];
                file = file.Replace("_REP_S" + (i + 1), row.S_NAME);
                file = file.Replace("_REP_H" + (i + 1), row.L_TIME.ToString());
                file = file.Replace("_REP_P" + (i + 1), row.S_HOUR_PRICE.ToString());
                sub_idx++;

                sub_prices.Add(new KeyValuePair<int, double>(row.L_TIME, row.S_HOUR_PRICE));
            }

            for (int i = sub_idx; i < 7; i++)
            {
                file = file.Replace("_REP_S" + (i + 1), "");
                file = file.Replace("_REP_H" + (i + 1), "");
                file = file.Replace("_REP_P" + (i + 1), "");
            }

            double all_price = 0;
            foreach (KeyValuePair<int, double> value in sub_prices)
            {
                all_price += value.Key * value.Value;
            }

            file = file.Replace("_REP_ALL_SUM_", all_price.ToString());

            file = file.Replace("_REP_FIRST_SUM_", numContribution.Value.ToString());

            File.WriteAllText("reports\\train_doc_tmp.rdl", file);

            string filepath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "reports\\train_doc_tmp.rdl");
            rdlViewer1.SourceFile = new Uri(filepath);

            rdlViewer1.Rebuild();
        }
    }
}
