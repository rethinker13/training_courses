﻿namespace training_courses
{
    partial class FullStudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FullStudentForm));
            this.v_F_STUDENTSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.v_F_STUDENTSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dBDataSet = new training_courses.DBDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.v_F_STUDENTSBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tbAddStudent = new System.Windows.Forms.ToolStripButton();
            this.tbEditStudent = new System.Windows.Forms.ToolStripButton();
            this.tbDeleteStudent = new System.Windows.Forms.ToolStripButton();
            this.tbExcelExport = new System.Windows.Forms.ToolStripButton();
            this.tbReceipt = new System.Windows.Forms.ToolStripButton();
            this.v_F_STUDENTSDataGridView = new System.Windows.Forms.DataGridView();
            this.F_S_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_G_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_S_COURSE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_S_NUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_S_PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_S_ADDRESS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_S_INFO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.numCourse = new System.Windows.Forms.NumericUpDown();
            this.btnUpdateCourse = new System.Windows.Forms.Button();
            this.v_F_STUDENTSTableAdapter = new training_courses.DBDataSetTableAdapters.V_F_STUDENTSTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.v_STUDENTSTableAdapter = new training_courses.DBDataSetTableAdapters.V_STUDENTSTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.v_F_STUDENTSBindingNavigator)).BeginInit();
            this.v_F_STUDENTSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_F_STUDENTSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_F_STUDENTSDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).BeginInit();
            this.SuspendLayout();
            // 
            // v_F_STUDENTSBindingNavigator
            // 
            this.v_F_STUDENTSBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.v_F_STUDENTSBindingNavigator.BindingSource = this.v_F_STUDENTSBindingSource;
            this.v_F_STUDENTSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.v_F_STUDENTSBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.v_F_STUDENTSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.v_F_STUDENTSBindingNavigatorSaveItem,
            this.tbAddStudent,
            this.tbEditStudent,
            this.tbDeleteStudent,
            this.tbExcelExport,
            this.tbReceipt});
            this.v_F_STUDENTSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.v_F_STUDENTSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.v_F_STUDENTSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.v_F_STUDENTSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.v_F_STUDENTSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.v_F_STUDENTSBindingNavigator.Name = "v_F_STUDENTSBindingNavigator";
            this.v_F_STUDENTSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.v_F_STUDENTSBindingNavigator.Size = new System.Drawing.Size(977, 25);
            this.v_F_STUDENTSBindingNavigator.TabIndex = 0;
            this.v_F_STUDENTSBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // v_F_STUDENTSBindingSource
            // 
            this.v_F_STUDENTSBindingSource.DataMember = "V_F_STUDENTS";
            this.v_F_STUDENTSBindingSource.DataSource = this.dBDataSet;
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // v_F_STUDENTSBindingNavigatorSaveItem
            // 
            this.v_F_STUDENTSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.v_F_STUDENTSBindingNavigatorSaveItem.Enabled = false;
            this.v_F_STUDENTSBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("v_F_STUDENTSBindingNavigatorSaveItem.Image")));
            this.v_F_STUDENTSBindingNavigatorSaveItem.Name = "v_F_STUDENTSBindingNavigatorSaveItem";
            this.v_F_STUDENTSBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.v_F_STUDENTSBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // tbAddStudent
            // 
            this.tbAddStudent.Image = global::training_courses.Properties.Resources.if_sign_add_299068;
            this.tbAddStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbAddStudent.Name = "tbAddStudent";
            this.tbAddStudent.Size = new System.Drawing.Size(79, 22);
            this.tbAddStudent.Text = "Добавить";
            this.tbAddStudent.Click += new System.EventHandler(this.tbAddStudent_Click);
            // 
            // tbEditStudent
            // 
            this.tbEditStudent.Image = global::training_courses.Properties.Resources.edit_128;
            this.tbEditStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbEditStudent.Name = "tbEditStudent";
            this.tbEditStudent.Size = new System.Drawing.Size(107, 22);
            this.tbEditStudent.Text = "Редактировать";
            this.tbEditStudent.Click += new System.EventHandler(this.tbEditStudent_Click);
            // 
            // tbDeleteStudent
            // 
            this.tbDeleteStudent.Image = global::training_courses.Properties.Resources.error;
            this.tbDeleteStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbDeleteStudent.Name = "tbDeleteStudent";
            this.tbDeleteStudent.Size = new System.Drawing.Size(71, 22);
            this.tbDeleteStudent.Text = "Удалить";
            this.tbDeleteStudent.Click += new System.EventHandler(this.tbDeleteStudent_Click);
            // 
            // tbExcelExport
            // 
            this.tbExcelExport.Image = global::training_courses.Properties.Resources.excel;
            this.tbExcelExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbExcelExport.Name = "tbExcelExport";
            this.tbExcelExport.Size = new System.Drawing.Size(123, 22);
            this.tbExcelExport.Text = "Сохранить в Excel";
            this.tbExcelExport.Click += new System.EventHandler(this.tbExcelExport_Click);
            // 
            // tbReceipt
            // 
            this.tbReceipt.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.tbReceipt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbReceipt.Name = "tbReceipt";
            this.tbReceipt.Size = new System.Drawing.Size(175, 22);
            this.tbReceipt.Text = "Сформировать квитанцию";
            this.tbReceipt.Click += new System.EventHandler(this.tbReceipt_Click);
            // 
            // v_F_STUDENTSDataGridView
            // 
            this.v_F_STUDENTSDataGridView.AllowUserToAddRows = false;
            this.v_F_STUDENTSDataGridView.AllowUserToDeleteRows = false;
            this.v_F_STUDENTSDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.v_F_STUDENTSDataGridView.AutoGenerateColumns = false;
            this.v_F_STUDENTSDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.v_F_STUDENTSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.v_F_STUDENTSDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F_S_ID,
            this.F_G_ID,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.F_S_COURSE,
            this.F_S_NUMBER,
            this.F_S_PRICE,
            this.F_S_ADDRESS,
            this.F_S_INFO,
            this.dataGridViewTextBoxColumn11});
            this.v_F_STUDENTSDataGridView.DataSource = this.v_F_STUDENTSBindingSource;
            this.v_F_STUDENTSDataGridView.Location = new System.Drawing.Point(0, 54);
            this.v_F_STUDENTSDataGridView.Name = "v_F_STUDENTSDataGridView";
            this.v_F_STUDENTSDataGridView.RowHeadersVisible = false;
            this.v_F_STUDENTSDataGridView.Size = new System.Drawing.Size(977, 598);
            this.v_F_STUDENTSDataGridView.TabIndex = 1;
            // 
            // F_S_ID
            // 
            this.F_S_ID.DataPropertyName = "F_S_ID";
            this.F_S_ID.HeaderText = "F_S_ID";
            this.F_S_ID.Name = "F_S_ID";
            this.F_S_ID.Visible = false;
            // 
            // F_G_ID
            // 
            this.F_G_ID.DataPropertyName = "F_G_ID";
            this.F_G_ID.HeaderText = "F_G_ID";
            this.F_G_ID.Name = "F_G_ID";
            this.F_G_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "P_LAST_NAME";
            this.dataGridViewTextBoxColumn3.HeaderText = "Фамилия";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "P_FIRST_NAME";
            this.dataGridViewTextBoxColumn4.HeaderText = "Имя";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "P_SECOND_NAME";
            this.dataGridViewTextBoxColumn5.HeaderText = "Отчество";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // F_S_COURSE
            // 
            this.F_S_COURSE.DataPropertyName = "F_S_COURSE";
            this.F_S_COURSE.HeaderText = "F_S_COURSE";
            this.F_S_COURSE.Name = "F_S_COURSE";
            this.F_S_COURSE.Visible = false;
            // 
            // F_S_NUMBER
            // 
            this.F_S_NUMBER.DataPropertyName = "F_S_NUMBER";
            this.F_S_NUMBER.HeaderText = "№ контракта";
            this.F_S_NUMBER.Name = "F_S_NUMBER";
            // 
            // F_S_PRICE
            // 
            this.F_S_PRICE.DataPropertyName = "F_S_PRICE";
            this.F_S_PRICE.HeaderText = "F_S_PRICE";
            this.F_S_PRICE.Name = "F_S_PRICE";
            this.F_S_PRICE.Visible = false;
            // 
            // F_S_ADDRESS
            // 
            this.F_S_ADDRESS.DataPropertyName = "F_S_ADDRESS";
            this.F_S_ADDRESS.HeaderText = "Адрес";
            this.F_S_ADDRESS.Name = "F_S_ADDRESS";
            // 
            // F_S_INFO
            // 
            this.F_S_INFO.DataPropertyName = "F_S_INFO";
            this.F_S_INFO.HeaderText = "Информация";
            this.F_S_INFO.Name = "F_S_INFO";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "P_ID";
            this.dataGridViewTextBoxColumn11.HeaderText = "P_ID";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Курс студентов:";
            // 
            // numCourse
            // 
            this.numCourse.Location = new System.Drawing.Point(106, 28);
            this.numCourse.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numCourse.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCourse.Name = "numCourse";
            this.numCourse.Size = new System.Drawing.Size(66, 20);
            this.numCourse.TabIndex = 3;
            this.numCourse.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnUpdateCourse
            // 
            this.btnUpdateCourse.Location = new System.Drawing.Point(179, 25);
            this.btnUpdateCourse.Name = "btnUpdateCourse";
            this.btnUpdateCourse.Size = new System.Drawing.Size(94, 23);
            this.btnUpdateCourse.TabIndex = 4;
            this.btnUpdateCourse.Text = "Обновить курс";
            this.btnUpdateCourse.UseVisualStyleBackColor = true;
            this.btnUpdateCourse.Click += new System.EventHandler(this.btnUpdateCourse_Click);
            // 
            // v_F_STUDENTSTableAdapter
            // 
            this.v_F_STUDENTSTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // v_STUDENTSTableAdapter
            // 
            this.v_STUDENTSTableAdapter.ClearBeforeFill = true;
            // 
            // FullStudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 652);
            this.Controls.Add(this.btnUpdateCourse);
            this.Controls.Add(this.numCourse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.v_F_STUDENTSDataGridView);
            this.Controls.Add(this.v_F_STUDENTSBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FullStudentForm";
            this.Text = "FullStudentForm";
            this.Load += new System.EventHandler(this.FullStudentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.v_F_STUDENTSBindingNavigator)).EndInit();
            this.v_F_STUDENTSBindingNavigator.ResumeLayout(false);
            this.v_F_STUDENTSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_F_STUDENTSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_F_STUDENTSDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource v_F_STUDENTSBindingSource;
        private DBDataSetTableAdapters.V_F_STUDENTSTableAdapter v_F_STUDENTSTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator v_F_STUDENTSBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton v_F_STUDENTSBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView v_F_STUDENTSDataGridView;
        private System.Windows.Forms.ToolStripButton tbAddStudent;
        private System.Windows.Forms.ToolStripButton tbEditStudent;
        private System.Windows.Forms.ToolStripButton tbDeleteStudent;
        private System.Windows.Forms.ToolStripButton tbExcelExport;
        private System.Windows.Forms.ToolStripButton tbReceipt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numCourse;
        private System.Windows.Forms.Button btnUpdateCourse;
        private DBDataSetTableAdapters.V_STUDENTSTableAdapter v_STUDENTSTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_S_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_G_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_S_COURSE;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_S_NUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_S_PRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_S_ADDRESS;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_S_INFO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
    }
}