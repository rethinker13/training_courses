﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class MastersForm : Form
    {
        public MastersForm()
        {
            InitializeComponent();

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                
                tbExcel.Visible = false;
            }
        }

        private void t_GROUPBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {

                this.Validate();
                this.t_GROUPBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dBDataSet);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    Application.ProductName, ex.Message);
                message.ShowDialog();
            }

            MastersForm_Load(sender, e);
        }

        private void MastersForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_GROUP' table. You can move, or remove it, as needed.
            this.t_GROUPTableAdapter.FillMasters(this.dBDataSet.T_GROUP);
            if (AccessLevel.level == AccessLevel.Level.Students)
            {
                tbExcel.Visible = false;
            }
            if (AccessLevel.level == AccessLevel.Level.Manager)
            {
                t_GROUPDataGridView.ReadOnly = true;
            }
        }

        private void tsEditGroup_Click(object sender, EventArgs e)
        {
            if (t_GROUPDataGridView.SelectedCells.Count != 0)
            {
                if (t_GROUPDataGridView.SelectedCells[0].RowIndex != -1)
                {
                    int idx = t_GROUPDataGridView.SelectedCells[0].RowIndex;
                    if (t_GROUPDataGridView.Rows[idx].Cells[0].Value != null && t_GROUPDataGridView.Rows[idx].Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        long groupID = (long)t_GROUPDataGridView.Rows[idx].Cells[0].Value;
                        string groupName = t_GROUPDataGridView.Rows[idx].Cells[1].Value.ToString();
                        MasterStudentForm studentForm = new MasterStudentForm(groupID, groupName);
                        foreach (Form form in ((FrmMain)this.ParentForm).MdiChildren)
                        {
                            if (form.GetType() == typeof(MasterStudentForm))
                                studentForm = (MasterStudentForm)form;
                        }
                        studentForm.MdiParent = (FrmMain)this.ParentForm;
                        studentForm.Show();
                    }
                }
            }
        }

        private void tbExcel_Click(object sender, EventArgs e)
        {
            if (t_GROUPDataGridView.CurrentRow != null)
            {
                if (t_GROUPDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (t_GROUPDataGridView.CurrentRow.Cells[0].Value != null && t_GROUPDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        long groupID = (long)t_GROUPDataGridView.CurrentRow.Cells[0].Value;
                        if (t_GROUPDataGridView.CurrentRow.Cells[2].Value == null || t_GROUPDataGridView.CurrentRow.Cells[2].Value.ToString() == "")
                            return;
                        byte[] xlsBin = (byte[])t_GROUPDataGridView.CurrentRow.Cells[2].Value;

                        string group_name = t_GROUPDataGridView.CurrentRow.Cells[1].Value.ToString();
                        if (!Directory.Exists(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Магистры\\"))
                            Directory.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Магистры\\");
                        if (!Directory.Exists(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Магистры\\" +
                            group_name + "\\"))
                            Directory.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Магистры\\" +
                            group_name + "\\");

                        string tmpFile = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Магистры\\" +
                             group_name + "\\Студенты " + group_name + ".xls";
                        File.WriteAllBytes(tmpFile, xlsBin);

                        Process.Start(tmpFile);

                    }
                }
            }
        }

        private void t_GROUPDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (t_GROUPDataGridView.CurrentRow != null)
            {
                t_GROUPDataGridView.CurrentRow.Cells[3].Value = 1;
                t_GROUPDataGridView.CurrentRow.Cells[4].Value = 0;
            }
        }
    }
}
