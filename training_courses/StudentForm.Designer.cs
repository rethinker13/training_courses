﻿namespace training_courses
{
    partial class StudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudentForm));
            this.dBDataSet = new training_courses.DBDataSet();
            this.v_STUDENTSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.v_STUDENTSTableAdapter = new training_courses.DBDataSetTableAdapters.V_STUDENTSTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.v_STUDENTSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.v_STUDENTSBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tbAddStudent = new System.Windows.Forms.ToolStripButton();
            this.tbEditStudent = new System.Windows.Forms.ToolStripButton();
            this.tbDeleteStudent = new System.Windows.Forms.ToolStripButton();
            this.tbExcelExport = new System.Windows.Forms.ToolStripButton();
            this.tbReceipt = new System.Windows.Forms.ToolStripButton();
            this.v_STUDENTSDataGridView = new System.Windows.Forms.DataGridView();
            this.ST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnUpdateCourse = new System.Windows.Forms.Button();
            this.numCourse = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_STUDENTSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_STUDENTSBindingNavigator)).BeginInit();
            this.v_STUDENTSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_STUDENTSDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // v_STUDENTSBindingSource
            // 
            this.v_STUDENTSBindingSource.DataMember = "V_STUDENTS";
            this.v_STUDENTSBindingSource.DataSource = this.dBDataSet;
            // 
            // v_STUDENTSTableAdapter
            // 
            this.v_STUDENTSTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // v_STUDENTSBindingNavigator
            // 
            this.v_STUDENTSBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.v_STUDENTSBindingNavigator.BindingSource = this.v_STUDENTSBindingSource;
            this.v_STUDENTSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.v_STUDENTSBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.v_STUDENTSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.v_STUDENTSBindingNavigatorSaveItem,
            this.tbAddStudent,
            this.tbEditStudent,
            this.tbDeleteStudent,
            this.tbExcelExport,
            this.tbReceipt});
            this.v_STUDENTSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.v_STUDENTSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.v_STUDENTSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.v_STUDENTSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.v_STUDENTSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.v_STUDENTSBindingNavigator.Name = "v_STUDENTSBindingNavigator";
            this.v_STUDENTSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.v_STUDENTSBindingNavigator.Size = new System.Drawing.Size(1264, 25);
            this.v_STUDENTSBindingNavigator.TabIndex = 0;
            this.v_STUDENTSBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // v_STUDENTSBindingNavigatorSaveItem
            // 
            this.v_STUDENTSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.v_STUDENTSBindingNavigatorSaveItem.Enabled = false;
            this.v_STUDENTSBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("v_STUDENTSBindingNavigatorSaveItem.Image")));
            this.v_STUDENTSBindingNavigatorSaveItem.Name = "v_STUDENTSBindingNavigatorSaveItem";
            this.v_STUDENTSBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.v_STUDENTSBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // tbAddStudent
            // 
            this.tbAddStudent.Image = global::training_courses.Properties.Resources.if_sign_add_299068;
            this.tbAddStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbAddStudent.Name = "tbAddStudent";
            this.tbAddStudent.Size = new System.Drawing.Size(79, 22);
            this.tbAddStudent.Text = "Добавить";
            this.tbAddStudent.Click += new System.EventHandler(this.tbAddStudent_Click);
            // 
            // tbEditStudent
            // 
            this.tbEditStudent.Image = global::training_courses.Properties.Resources.edit_128;
            this.tbEditStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbEditStudent.Name = "tbEditStudent";
            this.tbEditStudent.Size = new System.Drawing.Size(107, 22);
            this.tbEditStudent.Text = "Редактировать";
            this.tbEditStudent.Click += new System.EventHandler(this.tbEditStudent_Click);
            // 
            // tbDeleteStudent
            // 
            this.tbDeleteStudent.Image = global::training_courses.Properties.Resources.error;
            this.tbDeleteStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbDeleteStudent.Name = "tbDeleteStudent";
            this.tbDeleteStudent.Size = new System.Drawing.Size(71, 22);
            this.tbDeleteStudent.Text = "Удалить";
            this.tbDeleteStudent.Click += new System.EventHandler(this.tbDeleteStudent_Click);
            // 
            // tbExcelExport
            // 
            this.tbExcelExport.Image = global::training_courses.Properties.Resources.excel;
            this.tbExcelExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbExcelExport.Name = "tbExcelExport";
            this.tbExcelExport.Size = new System.Drawing.Size(123, 22);
            this.tbExcelExport.Text = "Сохранить в Excel";
            this.tbExcelExport.Click += new System.EventHandler(this.tbExcelExport_Click);
            // 
            // tbReceipt
            // 
            this.tbReceipt.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.tbReceipt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbReceipt.Name = "tbReceipt";
            this.tbReceipt.Size = new System.Drawing.Size(175, 22);
            this.tbReceipt.Text = "Сформировать квитанцию";
            this.tbReceipt.Click += new System.EventHandler(this.tbReceipt_Click);
            // 
            // v_STUDENTSDataGridView
            // 
            this.v_STUDENTSDataGridView.AllowUserToAddRows = false;
            this.v_STUDENTSDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.v_STUDENTSDataGridView.AutoGenerateColumns = false;
            this.v_STUDENTSDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.v_STUDENTSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.v_STUDENTSDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ST_ID,
            this.GR_ID,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.v_STUDENTSDataGridView.DataSource = this.v_STUDENTSBindingSource;
            this.v_STUDENTSDataGridView.Location = new System.Drawing.Point(0, 57);
            this.v_STUDENTSDataGridView.Name = "v_STUDENTSDataGridView";
            this.v_STUDENTSDataGridView.RowHeadersVisible = false;
            this.v_STUDENTSDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.v_STUDENTSDataGridView.Size = new System.Drawing.Size(1264, 624);
            this.v_STUDENTSDataGridView.TabIndex = 1;
            // 
            // ST_ID
            // 
            this.ST_ID.DataPropertyName = "ST_ID";
            this.ST_ID.HeaderText = "ST_ID";
            this.ST_ID.Name = "ST_ID";
            this.ST_ID.Visible = false;
            // 
            // GR_ID
            // 
            this.GR_ID.DataPropertyName = "GR_ID";
            this.GR_ID.HeaderText = "GR_ID";
            this.GR_ID.Name = "GR_ID";
            this.GR_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "P_LAST_NAME";
            this.dataGridViewTextBoxColumn1.HeaderText = "Фамилия";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "P_FIRST_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Имя";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "P_SECOND_NAME";
            this.dataGridViewTextBoxColumn3.HeaderText = "Отчество";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ST_COURSE";
            this.dataGridViewTextBoxColumn4.HeaderText = "Курс";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ST_CONTRACT_NUMBER";
            this.dataGridViewTextBoxColumn5.HeaderText = "№ контракта";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ST_PRICE";
            this.dataGridViewTextBoxColumn6.HeaderText = "Сумма";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ST_ADDRESS";
            this.dataGridViewTextBoxColumn7.HeaderText = "Адрес";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ST_INFO";
            this.dataGridViewTextBoxColumn8.HeaderText = "Информация";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "P_ID";
            this.dataGridViewTextBoxColumn9.HeaderText = "P_ID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // btnUpdateCourse
            // 
            this.btnUpdateCourse.Location = new System.Drawing.Point(179, 28);
            this.btnUpdateCourse.Name = "btnUpdateCourse";
            this.btnUpdateCourse.Size = new System.Drawing.Size(96, 23);
            this.btnUpdateCourse.TabIndex = 7;
            this.btnUpdateCourse.Text = "Обновить курс";
            this.btnUpdateCourse.UseVisualStyleBackColor = true;
            this.btnUpdateCourse.Click += new System.EventHandler(this.btnUpdateCourse_Click);
            // 
            // numCourse
            // 
            this.numCourse.Location = new System.Drawing.Point(106, 31);
            this.numCourse.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numCourse.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCourse.Name = "numCourse";
            this.numCourse.Size = new System.Drawing.Size(66, 20);
            this.numCourse.TabIndex = 6;
            this.numCourse.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Курс студентов:";
            // 
            // StudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.btnUpdateCourse);
            this.Controls.Add(this.numCourse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.v_STUDENTSDataGridView);
            this.Controls.Add(this.v_STUDENTSBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StudentForm";
            this.Text = "StundentForm";
            this.Load += new System.EventHandler(this.StudentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_STUDENTSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_STUDENTSBindingNavigator)).EndInit();
            this.v_STUDENTSBindingNavigator.ResumeLayout(false);
            this.v_STUDENTSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_STUDENTSDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource v_STUDENTSBindingSource;
        private DBDataSetTableAdapters.V_STUDENTSTableAdapter v_STUDENTSTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator v_STUDENTSBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton v_STUDENTSBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView v_STUDENTSDataGridView;
        private System.Windows.Forms.ToolStripButton tbEditStudent;
        private System.Windows.Forms.ToolStripButton tbDeleteStudent;
        private System.Windows.Forms.ToolStripButton tbExcelExport;
        private System.Windows.Forms.ToolStripButton tbAddStudent;
        private System.Windows.Forms.ToolStripButton tbReceipt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn GR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Button btnUpdateCourse;
        private System.Windows.Forms.NumericUpDown numCourse;
        private System.Windows.Forms.Label label1;
    }
}