﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses.Submission
{
    public partial class SubmissionStatusForm : Form
    {
        public SubmissionStatusForm()
        {
            InitializeComponent();
        }

        private void dOC_SUB_STATUSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            this.Validate();
            this.dOC_SUB_STATUSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDataSet);

        }

        private void SubmissionStatusForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.DOC_SUB_STATUS' table. You can move, or remove it, as needed.
            this.dOC_SUB_STATUSTableAdapter.Fill(this.dBDataSet.DOC_SUB_STATUS);

        }
    }
}
