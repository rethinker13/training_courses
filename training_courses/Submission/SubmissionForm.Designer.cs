﻿namespace training_courses.Submission
{
    partial class SubmissionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubmissionForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtContract = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSecondName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.sUBJ_DOC_SUBMBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.sUBJ_DOC_SUBMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dBDataSet = new training_courses.DBDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.clPic = new System.Windows.Forms.DataGridViewImageColumn();
            this.sDSIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sDSSUBJIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tSUBJECTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sDSDSIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sDSSCOREDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numCourse = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.sUBJ_DOC_SUBMTableAdapter = new training_courses.DBDataSetTableAdapters.SUBJ_DOC_SUBMTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.t_SUBJECTTableAdapter = new training_courses.DBDataSetTableAdapters.T_SUBJECTTableAdapter();
            this.txtCustFace = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBasis = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSpeciality = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbForm = new System.Windows.Forms.ComboBox();
            this.nPeriod = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.nPeriodA = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbDiplom = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbRole = new System.Windows.Forms.ComboBox();
            this.np = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.sp = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFAddress = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.dtBdate = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.txtHead = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.nSemestr = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.nAllSum = new System.Windows.Forms.NumericUpDown();
            this.nSemSum = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.nSubSum = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtPInfo = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sUBJ_DOC_SUBMBindingNavigator)).BeginInit();
            this.sUBJ_DOC_SUBMBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sUBJ_DOC_SUBMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSUBJECTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nPeriodA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.np)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSemestr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nAllSum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSemSum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSubSum)).BeginInit();
            this.SuspendLayout();
            // 
            // txtInfo
            // 
            this.txtInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInfo.Location = new System.Drawing.Point(89, 557);
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(591, 20);
            this.txtInfo.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 560);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Информация";
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddress.Location = new System.Drawing.Point(124, 506);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(556, 20);
            this.txtAddress.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 509);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Адрес регистрации";
            // 
            // txtContract
            // 
            this.txtContract.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContract.Location = new System.Drawing.Point(115, 116);
            this.txtContract.Name = "txtContract";
            this.txtContract.Size = new System.Drawing.Size(568, 20);
            this.txtContract.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Номер контракта";
            // 
            // txtSecondName
            // 
            this.txtSecondName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecondName.Location = new System.Drawing.Point(75, 64);
            this.txtSecondName.Name = "txtSecondName";
            this.txtSecondName.Size = new System.Drawing.Size(608, 20);
            this.txtSecondName.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Отчество";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFirstName.Location = new System.Drawing.Point(75, 38);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(608, 20);
            this.txtFirstName.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Имя";
            // 
            // txtLastName
            // 
            this.txtLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLastName.Location = new System.Drawing.Point(75, 12);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(608, 20);
            this.txtLastName.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Фамилия";
            // 
            // sUBJ_DOC_SUBMBindingNavigator
            // 
            this.sUBJ_DOC_SUBMBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.sUBJ_DOC_SUBMBindingNavigator.BindingSource = this.sUBJ_DOC_SUBMBindingSource;
            this.sUBJ_DOC_SUBMBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.sUBJ_DOC_SUBMBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.sUBJ_DOC_SUBMBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem});
            this.sUBJ_DOC_SUBMBindingNavigator.Location = new System.Drawing.Point(3, 16);
            this.sUBJ_DOC_SUBMBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.sUBJ_DOC_SUBMBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.sUBJ_DOC_SUBMBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.sUBJ_DOC_SUBMBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.sUBJ_DOC_SUBMBindingNavigator.Name = "sUBJ_DOC_SUBMBindingNavigator";
            this.sUBJ_DOC_SUBMBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.sUBJ_DOC_SUBMBindingNavigator.Size = new System.Drawing.Size(661, 25);
            this.sUBJ_DOC_SUBMBindingNavigator.TabIndex = 32;
            this.sUBJ_DOC_SUBMBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // sUBJ_DOC_SUBMBindingSource
            // 
            this.sUBJ_DOC_SUBMBindingSource.DataMember = "SUBJ_DOC_SUBM";
            this.sUBJ_DOC_SUBMBindingSource.DataSource = this.dBDataSet;
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // sUBJ_DOC_SUBMBindingNavigatorSaveItem
            // 
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("sUBJ_DOC_SUBMBindingNavigatorSaveItem.Image")));
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem.Name = "sUBJ_DOC_SUBMBindingNavigatorSaveItem";
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem.Text = "Save Data";
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem.Visible = false;
            this.sUBJ_DOC_SUBMBindingNavigatorSaveItem.Click += new System.EventHandler(this.sUBJ_DOC_SUBMBindingNavigatorSaveItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dgvData);
            this.groupBox1.Controls.Add(this.sUBJ_DOC_SUBMBindingNavigator);
            this.groupBox1.Location = new System.Drawing.Point(16, 583);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(667, 135);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Предметы";
            // 
            // dgvData
            // 
            this.dgvData.AutoGenerateColumns = false;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clPic,
            this.sDSIDDataGridViewTextBoxColumn,
            this.sDSSUBJIDDataGridViewTextBoxColumn,
            this.sDSDSIDDataGridViewTextBoxColumn,
            this.sDSSCOREDataGridViewTextBoxColumn});
            this.dgvData.DataSource = this.sUBJ_DOC_SUBMBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LimeGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.EnableHeadersVisualStyles = false;
            this.dgvData.GridColor = System.Drawing.Color.Silver;
            this.dgvData.Location = new System.Drawing.Point(3, 41);
            this.dgvData.MultiSelect = false;
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvData.Size = new System.Drawing.Size(661, 91);
            this.dgvData.TabIndex = 33;
            this.dgvData.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvData_CellBeginEdit);
            // 
            // clPic
            // 
            this.clPic.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.clPic.HeaderText = "";
            this.clPic.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.clPic.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.clPic.Name = "clPic";
            this.clPic.ReadOnly = true;
            this.clPic.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clPic.Width = 20;
            // 
            // sDSIDDataGridViewTextBoxColumn
            // 
            this.sDSIDDataGridViewTextBoxColumn.DataPropertyName = "SDS_ID";
            this.sDSIDDataGridViewTextBoxColumn.HeaderText = "SDS_ID";
            this.sDSIDDataGridViewTextBoxColumn.Name = "sDSIDDataGridViewTextBoxColumn";
            this.sDSIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // sDSSUBJIDDataGridViewTextBoxColumn
            // 
            this.sDSSUBJIDDataGridViewTextBoxColumn.DataPropertyName = "SDS_SUBJ_ID";
            this.sDSSUBJIDDataGridViewTextBoxColumn.DataSource = this.tSUBJECTBindingSource;
            this.sDSSUBJIDDataGridViewTextBoxColumn.DisplayMember = "S_NAME";
            this.sDSSUBJIDDataGridViewTextBoxColumn.HeaderText = "Предмет";
            this.sDSSUBJIDDataGridViewTextBoxColumn.Name = "sDSSUBJIDDataGridViewTextBoxColumn";
            this.sDSSUBJIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sDSSUBJIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sDSSUBJIDDataGridViewTextBoxColumn.ValueMember = "S_ID";
            // 
            // tSUBJECTBindingSource
            // 
            this.tSUBJECTBindingSource.DataMember = "T_SUBJECT";
            this.tSUBJECTBindingSource.DataSource = this.dBDataSet;
            // 
            // sDSDSIDDataGridViewTextBoxColumn
            // 
            this.sDSDSIDDataGridViewTextBoxColumn.DataPropertyName = "SDS_DS_ID";
            this.sDSDSIDDataGridViewTextBoxColumn.HeaderText = "SDS_DS_ID";
            this.sDSDSIDDataGridViewTextBoxColumn.Name = "sDSDSIDDataGridViewTextBoxColumn";
            this.sDSDSIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // sDSSCOREDataGridViewTextBoxColumn
            // 
            this.sDSSCOREDataGridViewTextBoxColumn.DataPropertyName = "SDS_SCORE";
            this.sDSSCOREDataGridViewTextBoxColumn.HeaderText = "Баллы";
            this.sDSSCOREDataGridViewTextBoxColumn.Name = "sDSSCOREDataGridViewTextBoxColumn";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(436, 724);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(166, 23);
            this.btnOk.TabIndex = 35;
            this.btnOk.Text = "Внести в систему";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(608, 724);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 34;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // numCourse
            // 
            this.numCourse.Location = new System.Drawing.Point(75, 90);
            this.numCourse.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numCourse.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCourse.Name = "numCourse";
            this.numCourse.Size = new System.Drawing.Size(120, 20);
            this.numCourse.TabIndex = 37;
            this.numCourse.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Курс";
            // 
            // sUBJ_DOC_SUBMTableAdapter
            // 
            this.sUBJ_DOC_SUBMTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DOC_SUB_STATUSTableAdapter = null;
            this.tableAdapterManager.DOC_SUBMISSIONTableAdapter = null;
            this.tableAdapterManager.ENROLLMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.PAY_QUEUE_STATUSTableAdapter = null;
            this.tableAdapterManager.PAYMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.STUDENT_PAYMENTSTableAdapter = null;
            this.tableAdapterManager.SUBJ_DOC_SUBMTableAdapter = this.sUBJ_DOC_SUBMTableAdapter;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = this.t_SUBJECTTableAdapter;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // t_SUBJECTTableAdapter
            // 
            this.t_SUBJECTTableAdapter.ClearBeforeFill = true;
            // 
            // txtCustFace
            // 
            this.txtCustFace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCustFace.Location = new System.Drawing.Point(52, 142);
            this.txtCustFace.Name = "txtCustFace";
            this.txtCustFace.Size = new System.Drawing.Size(631, 20);
            this.txtCustFace.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Лицо";
            // 
            // txtBasis
            // 
            this.txtBasis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBasis.Location = new System.Drawing.Point(82, 168);
            this.txtBasis.Name = "txtBasis";
            this.txtBasis.Size = new System.Drawing.Size(601, 20);
            this.txtBasis.TabIndex = 41;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 171);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "Основание";
            // 
            // txtSpeciality
            // 
            this.txtSpeciality.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSpeciality.Location = new System.Drawing.Point(104, 194);
            this.txtSpeciality.Name = "txtSpeciality";
            this.txtSpeciality.Size = new System.Drawing.Size(579, 20);
            this.txtSpeciality.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Специальность";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "Форма";
            // 
            // cmbForm
            // 
            this.cmbForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbForm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbForm.FormattingEnabled = true;
            this.cmbForm.Items.AddRange(new object[] {
            "Очная",
            "Заочная"});
            this.cmbForm.Location = new System.Drawing.Point(63, 220);
            this.cmbForm.Name = "cmbForm";
            this.cmbForm.Size = new System.Drawing.Size(620, 21);
            this.cmbForm.TabIndex = 45;
            // 
            // nPeriod
            // 
            this.nPeriod.Location = new System.Drawing.Point(52, 247);
            this.nPeriod.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nPeriod.Name = "nPeriod";
            this.nPeriod.Size = new System.Drawing.Size(57, 20);
            this.nPeriod.TabIndex = 47;
            this.nPeriod.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nPeriod.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 249);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 46;
            this.label12.Text = "Срок";
            // 
            // nPeriodA
            // 
            this.nPeriodA.Location = new System.Drawing.Point(244, 247);
            this.nPeriodA.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nPeriodA.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nPeriodA.Name = "nPeriodA";
            this.nPeriodA.Size = new System.Drawing.Size(57, 20);
            this.nPeriodA.TabIndex = 49;
            this.nPeriodA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(117, 249);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = "Индивидуальный срок";
            // 
            // cmbDiplom
            // 
            this.cmbDiplom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDiplom.FormattingEnabled = true;
            this.cmbDiplom.Items.AddRange(new object[] {
            "Бакалавра",
            "Магистра"});
            this.cmbDiplom.Location = new System.Drawing.Point(153, 273);
            this.cmbDiplom.Name = "cmbDiplom";
            this.cmbDiplom.Size = new System.Drawing.Size(530, 21);
            this.cmbDiplom.TabIndex = 51;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 276);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(134, 13);
            this.label14.TabIndex = 50;
            this.label14.Text = "Диплом при завершении";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(307, 249);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 52;
            this.label15.Text = "Должность";
            // 
            // cmbRole
            // 
            this.cmbRole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbRole.FormattingEnabled = true;
            this.cmbRole.Items.AddRange(new object[] {
            "Студент",
            "Бакалавр",
            "Магистр"});
            this.cmbRole.Location = new System.Drawing.Point(381, 247);
            this.cmbRole.Name = "cmbRole";
            this.cmbRole.Size = new System.Drawing.Size(302, 21);
            this.cmbRole.TabIndex = 53;
            // 
            // np
            // 
            this.np.Location = new System.Drawing.Point(269, 454);
            this.np.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.np.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.np.Name = "np";
            this.np.Size = new System.Drawing.Size(71, 20);
            this.np.TabIndex = 57;
            this.np.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(175, 456);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 13);
            this.label16.TabIndex = 56;
            this.label16.Text = "Серия паспорта";
            // 
            // sp
            // 
            this.sp.Location = new System.Drawing.Point(110, 454);
            this.sp.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.sp.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sp.Name = "sp";
            this.sp.Size = new System.Drawing.Size(57, 20);
            this.sp.TabIndex = 55;
            this.sp.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 456);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 54;
            this.label17.Text = "Номер паспорта";
            // 
            // txtFAddress
            // 
            this.txtFAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFAddress.Location = new System.Drawing.Point(252, 532);
            this.txtFAddress.Name = "txtFAddress";
            this.txtFAddress.Size = new System.Drawing.Size(428, 20);
            this.txtFAddress.TabIndex = 59;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(143, 535);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 13);
            this.label18.TabIndex = 58;
            this.label18.Text = "Адрес проживания";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 303);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 13);
            this.label19.TabIndex = 60;
            this.label19.Text = "Телефон";
            // 
            // txtPhone
            // 
            this.txtPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPhone.Location = new System.Drawing.Point(71, 300);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(612, 20);
            this.txtPhone.TabIndex = 61;
            // 
            // dtBdate
            // 
            this.dtBdate.Location = new System.Drawing.Point(101, 326);
            this.dtBdate.Name = "dtBdate";
            this.dtBdate.Size = new System.Drawing.Size(200, 20);
            this.dtBdate.TabIndex = 62;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 332);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 13);
            this.label20.TabIndex = 63;
            this.label20.Text = "Дата рождения";
            // 
            // txtHead
            // 
            this.txtHead.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHead.Location = new System.Drawing.Point(104, 352);
            this.txtHead.Name = "txtHead";
            this.txtHead.Size = new System.Drawing.Size(579, 20);
            this.txtHead.TabIndex = 65;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 355);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 13);
            this.label21.TabIndex = 64;
            this.label21.Text = "Представитель";
            // 
            // nSemestr
            // 
            this.nSemestr.Location = new System.Drawing.Point(70, 378);
            this.nSemestr.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nSemestr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nSemestr.Name = "nSemestr";
            this.nSemestr.Size = new System.Drawing.Size(57, 20);
            this.nSemestr.TabIndex = 67;
            this.nSemestr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(13, 380);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 13);
            this.label22.TabIndex = 66;
            this.label22.Text = "Семестр";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 407);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(151, 13);
            this.label23.TabIndex = 68;
            this.label23.Text = "Полная стоимость обучения";
            // 
            // nAllSum
            // 
            this.nAllSum.Location = new System.Drawing.Point(170, 405);
            this.nAllSum.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nAllSum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nAllSum.Name = "nAllSum";
            this.nAllSum.Size = new System.Drawing.Size(57, 20);
            this.nAllSum.TabIndex = 69;
            this.nAllSum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nSemSum
            // 
            this.nSemSum.Location = new System.Drawing.Point(496, 405);
            this.nSemSum.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nSemSum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nSemSum.Name = "nSemSum";
            this.nSemSum.Size = new System.Drawing.Size(57, 20);
            this.nSemSum.TabIndex = 71;
            this.nSemSum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(233, 407);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(257, 13);
            this.label24.TabIndex = 70;
            this.label24.Text = "Стоимость обучения в первом семетре договора";
            // 
            // nSubSum
            // 
            this.nSubSum.Location = new System.Drawing.Point(269, 428);
            this.nSubSum.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nSubSum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nSubSum.Name = "nSubSum";
            this.nSubSum.Size = new System.Drawing.Size(57, 20);
            this.nSubSum.TabIndex = 73;
            this.nSubSum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(13, 430);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(251, 13);
            this.label25.TabIndex = 72;
            this.label25.Text = "Сумма возврата при переводе/восстановлении";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(13, 534);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(124, 17);
            this.checkBox1.TabIndex = 74;
            this.checkBox1.Text = "Равен регистрации";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtPInfo
            // 
            this.txtPInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPInfo.Location = new System.Drawing.Point(59, 480);
            this.txtPInfo.Name = "txtPInfo";
            this.txtPInfo.Size = new System.Drawing.Size(624, 20);
            this.txtPInfo.TabIndex = 76;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 483);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 13);
            this.label26.TabIndex = 75;
            this.label26.Text = "Выдан";
            // 
            // SubmissionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 759);
            this.Controls.Add(this.txtPInfo);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.nSubSum);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.nSemSum);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.nAllSum);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.nSemestr);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtHead);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.dtBdate);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtFAddress);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.np);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.sp);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cmbRole);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cmbDiplom);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.nPeriodA);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.nPeriod);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cmbForm);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtSpeciality);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtBasis);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCustFace);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numCourse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtContract);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtSecondName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SubmissionForm";
            this.Text = "Заявка на поступление";
            this.Load += new System.EventHandler(this.SubmissionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sUBJ_DOC_SUBMBindingNavigator)).EndInit();
            this.sUBJ_DOC_SUBMBindingNavigator.ResumeLayout(false);
            this.sUBJ_DOC_SUBMBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sUBJ_DOC_SUBMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSUBJECTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nPeriodA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.np)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSemestr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nAllSum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSemSum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nSubSum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtContract;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSecondName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label1;
        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource sUBJ_DOC_SUBMBindingSource;
        private DBDataSetTableAdapters.SUBJ_DOC_SUBMTableAdapter sUBJ_DOC_SUBMTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator sUBJ_DOC_SUBMBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton sUBJ_DOC_SUBMBindingNavigatorSaveItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private DBDataSetTableAdapters.T_SUBJECTTableAdapter t_SUBJECTTableAdapter;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.BindingSource tSUBJECTBindingSource;
        private System.Windows.Forms.DataGridViewImageColumn clPic;
        private System.Windows.Forms.DataGridViewTextBoxColumn sDSIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn sDSSUBJIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sDSDSIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sDSSCOREDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown numCourse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCustFace;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBasis;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSpeciality;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbForm;
        private System.Windows.Forms.NumericUpDown nPeriod;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nPeriodA;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbDiplom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbRole;
        private System.Windows.Forms.NumericUpDown np;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown sp;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFAddress;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.DateTimePicker dtBdate;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtHead;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown nSemestr;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown nAllSum;
        private System.Windows.Forms.NumericUpDown nSemSum;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown nSubSum;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtPInfo;
        private System.Windows.Forms.Label label26;
    }
}