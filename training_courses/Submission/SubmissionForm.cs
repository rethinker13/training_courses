﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses.Submission
{
    public partial class SubmissionForm : Form
    {
        private int __ds_id;
        private int __p_id;
        private int __ds_status;

        private DBDataSetTableAdapters.T_PERSONTableAdapter personAdapter;
        private DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter docSubAdapter;
        private DBDataSetTableAdapters.PAYMENT_QUEUETableAdapter paymentAdapter;

        public SubmissionForm(int __ds_id_ = 0)
        {
            InitializeComponent();

            personAdapter = new DBDataSetTableAdapters.T_PERSONTableAdapter();
            personAdapter.ClearBeforeFill = true;

            docSubAdapter = new DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter();
            docSubAdapter.ClearBeforeFill = true;

            paymentAdapter = new DBDataSetTableAdapters.PAYMENT_QUEUETableAdapter();
            paymentAdapter.ClearBeforeFill = true;

            this.__ds_id = __ds_id_;
            // TODO: This line of code loads data into the 'dBDataSet.T_SUBJECT' table. You can move, or remove it, as needed.
            this.t_SUBJECTTableAdapter.Fill(this.dBDataSet.T_SUBJECT);
            // TODO: This line of code loads data into the 'dBDataSet.SUBJ_DOC_SUBM' table. You can move, or remove it, as needed.
            this.sUBJ_DOC_SUBMTableAdapter.FillByDS_ID(this.dBDataSet.SUBJ_DOC_SUBM, this.__ds_id);
            if (this.__ds_id != 0)
            {
                DBDataSet.DOC_SUBMISSIONDataTable table = docSubAdapter.GetDataByID(__ds_id);
                DBDataSet.DOC_SUBMISSIONRow row = table.Rows[0] as DBDataSet.DOC_SUBMISSIONRow;
                DBDataSet.T_PERSONDataTable p_table = personAdapter.GetDataByID((int)row.DS_P_ID);
                DBDataSet.T_PERSONRow p_row = p_table.Rows[0] as DBDataSet.T_PERSONRow;
                txtFirstName.Text = p_row.P_FIRST_NAME;
                txtLastName.Text = p_row.P_LAST_NAME;
                txtSecondName.Text = p_row.P_SECOND_NAME;
                __p_id = (int)p_row.P_ID;
                txtContract.Text = row.DS_CONTRACT;
                txtAddress.Text = row.DS_ADDRESS;
                txtInfo.Text = row.DS_INFO;
                numCourse.Value = row.DS_COURSE;
                __ds_status = row.DS_DSS_ID;

                btnOk.Text = "Сохранить в системе";
                updateTable();
            }
        }

        private void sUBJ_DOC_SUBMBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.sUBJ_DOC_SUBMBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDataSet);
        }

        private void SubmissionForm_Load(object sender, EventArgs e)
        {


        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtLastName.Text) || String.IsNullOrEmpty(txtFirstName.Text)
                || String.IsNullOrEmpty(txtSecondName.Text))
            {
                MessageBox.Show("Введите корректное ФИО или заполните все поля!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ((int)numCourse.Value <= 0)
            {
                MessageBox.Show("Введите корректный курс!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtContract.Text))
            {
                MessageBox.Show("Введите номер контракта!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtAddress.Text))
            {
                MessageBox.Show("Введите адрес!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtLastName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Фамилия содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtFirstName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Имя содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtSecondName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Отчество содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // сначала диалог распечатки договора

            if (this.__ds_id == 0)
            {
                string basis = "";
                if (!String.IsNullOrEmpty(txtBasis.Text))
                {
                    basis = " действующего на основании " + txtBasis.Text + ",";
                }
                String full_name = txtLastName.Text + " " + txtFirstName.Text + " " + txtSecondName.Text;
                MasterContractForm mcf = new MasterContractForm(txtContract.Text,
                    txtCustFace.Text,
                    basis,
                    txtSpeciality.Text,
                    cmbForm.SelectedItem.ToString(),
                    nPeriod.Value.ToString(),
                    nPeriodA.Value.ToString(),
                    cmbDiplom.SelectedItem.ToString(),
                    cmbRole.SelectedItem.ToString(),
                    sp.Value.ToString(),
                    np.Value.ToString(),
                    txtPInfo.Text,
                    txtAddress.Text,
                    txtFAddress.Text,
                    txtPhone.Text,
                    dtBdate.Value.ToShortDateString(),
                    txtHead.Text,
                    txtInfo.Text,
                    nSemestr.Value.ToString(),
                    DateTime.Now.Year.ToString(),
                    Convert.ToString(DateTime.Now.Year + 1),
                    nAllSum.Value.ToString(),
                    nSubSum.Value.ToString(),
                    nSemSum.Value.ToString(),
                    full_name, full_name, DateTime.Now, (int)numCourse.Value);
                mcf.ShowDialog();
            }

            if (this.__ds_id == 0)
            {
                DBDataSet.T_PERSONDataTable personTable = personAdapter.GetDataByName(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                long pID = 0;
                if (personTable.Rows.Count == 0)
                {
                    int res = personAdapter.Insert(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                    personTable = personAdapter.GetDataByName(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                    DBDataSet.T_PERSONRow personRow = (DBDataSet.T_PERSONRow)personTable.Rows[0];
                    pID = personRow.P_ID;
                }
                else
                {
                    DBDataSet.T_PERSONRow personRow = (DBDataSet.T_PERSONRow)personTable.Rows[0];
                    pID = personRow.P_ID;
                }

                docSubAdapter.Insert((int)pID, 1, DateTime.Now, txtAddress.Text.Trim(), txtContract.Text.Trim(), (int)numCourse.Value,
                    txtInfo.Text.Trim());
                DBDataSet.DOC_SUBMISSIONDataTable tbl = docSubAdapter.GetDataByP_ID((int)pID);
                if (tbl.Rows.Count != 0)
                {
                    DBDataSet.DOC_SUBMISSIONRow row = (DBDataSet.DOC_SUBMISSIONRow)tbl.Rows[0];
                    this.__ds_id = row.DS_ID;
                }

                paymentAdapter.Insert((int)pID, 0, 1, DateTime.Now);
            }
            else
            {
                personAdapter.UpdateQueryByID(txtLastName.Text, txtFirstName.Text, txtSecondName.Text, __p_id);
                docSubAdapter.Update(__p_id, __ds_status, DateTime.Now, txtAddress.Text.Trim(), txtContract.Text.Trim(), (int)numCourse.Value,
                    txtInfo.Text.Trim(), __ds_id);
                DBDataSet.PAYMENT_QUEUEDataTable t = paymentAdapter.GetDataByP_ID((int)__p_id);
                if (t.Rows.Count == 0)
                {
                    paymentAdapter.Insert((int)__p_id, 0, 1, DateTime.Now);
                }
            }
            updateTable();
            sUBJ_DOC_SUBMBindingNavigatorSaveItem_Click(sender, e);
            this.Close();
        }

        private void dgvData_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (dgvData.CurrentRow != null && this.__ds_id != 0)
            {
                dgvData.CurrentRow.Cells[3].Value = this.__ds_id;
            }
        }

        private void updateTable()
        {
            foreach(DataGridViewRow row in dgvData.Rows)
            {
                if (row.Cells[1].Value != null && row.Cells[1].Value.ToString() != "")
                {
                    row.Cells[3].Value = this.__ds_id;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtFAddress.Text = txtAddress.Text;
                txtFAddress.Enabled = false;
            }
            else
            {
                txtFAddress.Enabled = true;
            }
        }
    }
}
