﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses.Submission
{
    public partial class SubmissionView : Form
    {
        private DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter adapter;
        private DBDataSetTableAdapters.DOC_SUB_STATUSTableAdapter status_adapter;
        private DBDataSetTableAdapters.T_PERSONTableAdapter personAdapter;

        private bool __firstLoaded;

        public SubmissionView()
        {
            InitializeComponent();

            adapter = new DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter();
            adapter.ClearBeforeFill = true;

            status_adapter = new DBDataSetTableAdapters.DOC_SUB_STATUSTableAdapter();
            status_adapter.ClearBeforeFill = true;

            personAdapter = new DBDataSetTableAdapters.T_PERSONTableAdapter();
            personAdapter.ClearBeforeFill = true;

        }

        private void SubmissionView_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.DOC_SUBMISSION_VIEW' table. You can move, or remove it, as needed.
            this.dOC_SUBMISSION_VIEWTableAdapter.Fill(this.dBDataSet.DOC_SUBMISSION_VIEW);

            __firstLoaded = true;
            updateTable();
        }

        private void tsbtnCreateNew_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            SubmissionForm sf = new SubmissionForm();
            sf.ShowDialog();
            SubmissionView_Load(sender, e);
        }

        private void tsbtnEditSelected_Click(object sender, EventArgs e)
        {

            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                if (!Global.UserCheck())
                {
                    MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                       "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                SubmissionForm sf = new SubmissionForm(current_id);
                sf.ShowDialog();
                SubmissionView_Load(sender, e);
            }
        }

        private void tsbtnDeleteSelected_Click(object sender, EventArgs e)
        {
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                if (!Global.UserCheck())
                {
                    MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                       "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);
                personAdapter.Delete(p_id);
                SubmissionView_Load(sender, e);
            }
        }

        private void updateTable()
        {
            foreach (DataGridViewRow row in dgvData.Rows)
            {
                if (row.Cells[1].Value != null && row.Cells[1].Value.ToString() != "")
                {
                    DBDataSet.DOC_SUB_STATUSDataTable table = status_adapter.GetDataByID(Convert.ToInt32(row.Cells[7].Value));
                    DBDataSet.DOC_SUB_STATUSRow st_row = table.Rows[0] as DBDataSet.DOC_SUB_STATUSRow;
                    row.DefaultCellStyle.BackColor = Color.FromName(st_row.DSS_COLOR.Trim());
                }
            }
        }

        private void dgvData_VisibleChanged(object sender, EventArgs e)
        {
            if (__firstLoaded && dgvData.Visible)
            {
                __firstLoaded = false;
                updateTable();
            }
        }
    }
}
