﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class FullStudentForm : Form
    {
        private long _groupID;
        private string _groupName;

        private DBDataSetTableAdapters.T_GROUPTableAdapter groupAdapter;
        private DBDataSetTableAdapters.T_STUDENTTableAdapter studentsAdapter;

        public FullStudentForm(long groupID, string groupName)
        {
            InitializeComponent();

            this._groupID = groupID;
            this._groupName = groupName;

            groupAdapter = new DBDataSetTableAdapters.T_GROUPTableAdapter();
            groupAdapter.ClearBeforeFill = true;

            studentsAdapter = new DBDataSetTableAdapters.T_STUDENTTableAdapter();
            studentsAdapter.ClearBeforeFill = true;

            groupAdapter.Fill(dBDataSet.T_GROUP);
            studentsAdapter.Fill(dBDataSet.T_STUDENT, (int)this._groupID);

            this.Validate();

            this.groupAdapter.Update(this.dBDataSet);
            this.studentsAdapter.Update(this.dBDataSet);

            this.dBDataSet.T_GROUP.AcceptChanges();
            this.dBDataSet.T_STUDENT.AcceptChanges();

            this.Text = "Группа очного обучения: " + groupName;
        }

        private void t_F_STUDENTBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.tableAdapterManager.UpdateAll(this.dBDataSet);

        }

        private void FullStudentForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.V_STUDENTS' table. You can move, or remove it, as needed.
            this.v_STUDENTSTableAdapter.Fill(this.dBDataSet.V_STUDENTS);
            // TODO: This line of code loads data into the 'dBDataSet.V_F_STUDENTS' table. You can move, or remove it, as needed.
            this.v_F_STUDENTSTableAdapter.FillByGrID(this.dBDataSet.V_F_STUDENTS, (int)_groupID);

            if (AccessLevel.level == AccessLevel.Level.Students)
            {
                tbExcelExport.Visible = tbReceipt.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                tbReceipt.Visible = true;
                tbAddStudent.Visible = tbEditStudent.Visible = tbDeleteStudent.Visible = false;
                numCourse.Enabled = false;
                btnUpdateCourse.Enabled = false;
                tbExcelExport.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Manager)
            {
                tbReceipt.Visible = false;
                tbAddStudent.Visible = tbEditStudent.Visible = tbDeleteStudent.Visible = false;
                numCourse.Enabled = false;
                btnUpdateCourse.Enabled = false;
                tbExcelExport.Visible = true;
            }

            if (AccessLevel.level == AccessLevel.Level.Engineer)
            {
                tbReceipt.Visible = false;
                tbAddStudent.Visible = tbEditStudent.Visible = tbDeleteStudent.Visible = true;
                numCourse.Enabled = true;
                btnUpdateCourse.Enabled = true;
                tbExcelExport.Visible = true;
            }

            DBDataSet.T_STUDENTDataTable table = studentsAdapter.GetData((int)this._groupID);
            if (table.Rows.Count != 0)
            {
                DBDataSet.T_STUDENTRow row = table.Rows[0] as DBDataSet.T_STUDENTRow;
                numCourse.Value = row.ST_COURSE;
            }
        }

        private void tbExcelExport_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string tmpFile = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + DateTime.Now.ToLongDateString() + "fullStudentGroupTmp.xls";
            ExcelExport ex = new ExcelExport();
            ex.ExportToExcel(this.v_F_STUDENTSDataGridView, _groupName, tmpFile);

            byte[] xlsBin = File.ReadAllBytes(tmpFile);
            groupAdapter.UpdateQueryXlsBin(xlsBin, (int)_groupID);

            File.Delete(tmpFile);

            FullStudentForm_Load(sender, e);
        }

        private void tbReceipt_Click(object sender, EventArgs e)
        {
            if (v_F_STUDENTSDataGridView.CurrentRow != null)
            {
                if (v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null && v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        int stID = (int)v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value;
                        int groupID = (int)v_F_STUDENTSDataGridView.CurrentRow.Cells[1].Value;
                        string lastName = v_F_STUDENTSDataGridView.CurrentRow.Cells[2].Value.ToString();
                        string firstName = v_F_STUDENTSDataGridView.CurrentRow.Cells[3].Value.ToString();
                        string secondName = v_F_STUDENTSDataGridView.CurrentRow.Cells[4].Value.ToString();
                        int course = (int)v_F_STUDENTSDataGridView.CurrentRow.Cells[5].Value;
                        string contract = v_F_STUDENTSDataGridView.CurrentRow.Cells[6].Value.ToString();
                        // 7
                                                
                        string address = v_F_STUDENTSDataGridView.CurrentRow.Cells[8].Value.ToString();
                        string info = v_F_STUDENTSDataGridView.CurrentRow.Cells[9].Value.ToString();

                        int pID = (int)(long)v_F_STUDENTSDataGridView.CurrentRow.Cells[10].Value;

                        StudentReceiptForm studentReceiptForm = new StudentReceiptForm(_groupID, stID, pID, _groupName,
                            lastName, firstName, secondName, course, contract, address, info, "Очные группы");
                        studentReceiptForm.ShowDialog();

                        FullStudentForm_Load(sender, e);
                    }
                }
            }
        }

        private void tbAddStudent_Click(object sender, EventArgs e)
        {
            DBDataSet.T_STUDENTDataTable table = studentsAdapter.GetData((int)this._groupID);
            if (table.Rows.Count != 0)
            {
                DBDataSet.T_STUDENTRow row = table.Rows[0] as DBDataSet.T_STUDENTRow;
                numCourse.Value = row.ST_COURSE;
            }

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            AddEditFullStudent addStudentForm = new AddEditFullStudent(_groupID, (int)numCourse.Value);
            addStudentForm.ShowDialog();

            FullStudentForm_Load(sender, e);
        }

        private void tbEditStudent_Click(object sender, EventArgs e)
        {
            if (v_F_STUDENTSDataGridView.CurrentRow != null)
            {
                if (v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null &&
                        v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        int stID = (int)v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value;
                        int groupID = (int)v_F_STUDENTSDataGridView.CurrentRow.Cells[1].Value;
                        string lastName = v_F_STUDENTSDataGridView.CurrentRow.Cells[2].Value.ToString();
                        string firstName = v_F_STUDENTSDataGridView.CurrentRow.Cells[3].Value.ToString();
                        string secondName = v_F_STUDENTSDataGridView.CurrentRow.Cells[4].Value.ToString();
                        int course = (int)v_F_STUDENTSDataGridView.CurrentRow.Cells[5].Value;
                        string contract = v_F_STUDENTSDataGridView.CurrentRow.Cells[6].Value.ToString();
                        // 7
                        string address = v_F_STUDENTSDataGridView.CurrentRow.Cells[8].Value.ToString();
                        string info = v_F_STUDENTSDataGridView.CurrentRow.Cells[9].Value.ToString();

                        int pID = (int)(long)v_F_STUDENTSDataGridView.CurrentRow.Cells[10].Value;

                        AddEditFullStudent addStudentForm = new AddEditFullStudent(_groupID, stID, pID,
                            lastName, firstName, secondName, course, contract, address, info);
                        addStudentForm.ShowDialog();

                        FullStudentForm_Load(sender, e);
                    }
                }
            }
        }

        private void tbDeleteStudent_Click(object sender, EventArgs e)
        {
            if (v_F_STUDENTSDataGridView.CurrentRow != null)
            {
                if (v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null &&
                        v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        studentsAdapter.Delete((int)v_F_STUDENTSDataGridView.CurrentRow.Cells[0].Value);

                        FullStudentForm_Load(sender, e);
                    }
                }
            }
        }

        private void btnUpdateCourse_Click(object sender, EventArgs e)
        {
            studentsAdapter.UpdateCourse((int)numCourse.Value, (int)this._groupID);
            FullStudentForm_Load(sender, e);
        }
    }
}
