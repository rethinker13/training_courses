﻿namespace training_courses.Payment
{
    partial class PaymentQueueView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentQueueView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dBDataSet = new training_courses.DBDataSet();
            this.pAYMENT_QUEUE_VIEWBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAYMENT_QUEUE_VIEWTableAdapter = new training_courses.DBDataSetTableAdapters.PAYMENT_QUEUE_VIEWTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.pAYMENT_QUEUE_VIEWBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCreateNew = new System.Windows.Forms.ToolStripButton();
            this.tsbtnMarkAsAccept = new System.Windows.Forms.ToolStripButton();
            this.tsbtnMarkAsDenied = new System.Windows.Forms.ToolStripButton();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.clPic = new System.Windows.Forms.DataGridViewImageColumn();
            this.pQIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pLASTNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pFIRSTNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pSECONDNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pQPIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pQSUMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pSQNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pQPQSIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pQDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnDeleteSelected = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAYMENT_QUEUE_VIEWBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAYMENT_QUEUE_VIEWBindingNavigator)).BeginInit();
            this.pAYMENT_QUEUE_VIEWBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pAYMENT_QUEUE_VIEWBindingSource
            // 
            this.pAYMENT_QUEUE_VIEWBindingSource.DataMember = "PAYMENT_QUEUE_VIEW";
            this.pAYMENT_QUEUE_VIEWBindingSource.DataSource = this.dBDataSet;
            // 
            // pAYMENT_QUEUE_VIEWTableAdapter
            // 
            this.pAYMENT_QUEUE_VIEWTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.DOC_SUB_STATUSTableAdapter = null;
            this.tableAdapterManager.DOC_SUBMISSIONTableAdapter = null;
            this.tableAdapterManager.ENROLLMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.PAY_QUEUE_STATUSTableAdapter = null;
            this.tableAdapterManager.PAYMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.STUDENT_PAYMENTSTableAdapter = null;
            this.tableAdapterManager.SUBJ_DOC_SUBMTableAdapter = null;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // pAYMENT_QUEUE_VIEWBindingNavigator
            // 
            this.pAYMENT_QUEUE_VIEWBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.BindingSource = this.pAYMENT_QUEUE_VIEWBindingSource;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem,
            this.tsbtnCreateNew,
            this.tsbtnMarkAsAccept,
            this.tsbtnMarkAsDenied,
            this.toolStripSeparator1,
            this.tsbtnDeleteSelected});
            this.pAYMENT_QUEUE_VIEWBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.pAYMENT_QUEUE_VIEWBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.Name = "pAYMENT_QUEUE_VIEWBindingNavigator";
            this.pAYMENT_QUEUE_VIEWBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.Size = new System.Drawing.Size(1008, 25);
            this.pAYMENT_QUEUE_VIEWBindingNavigator.TabIndex = 0;
            this.pAYMENT_QUEUE_VIEWBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem
            // 
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.Enabled = false;
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.Image")));
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.Name = "pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem";
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.Text = "Save Data";
            this.pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem.Visible = false;
            // 
            // tsbtnCreateNew
            // 
            this.tsbtnCreateNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnCreateNew.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnCreateNew.Image")));
            this.tsbtnCreateNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCreateNew.Name = "tsbtnCreateNew";
            this.tsbtnCreateNew.Size = new System.Drawing.Size(23, 22);
            this.tsbtnCreateNew.Text = "Создать квитанцию";
            this.tsbtnCreateNew.Click += new System.EventHandler(this.tsbtnCreateNew_Click);
            // 
            // tsbtnMarkAsAccept
            // 
            this.tsbtnMarkAsAccept.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnMarkAsAccept.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnMarkAsAccept.Image")));
            this.tsbtnMarkAsAccept.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnMarkAsAccept.Name = "tsbtnMarkAsAccept";
            this.tsbtnMarkAsAccept.Size = new System.Drawing.Size(23, 22);
            this.tsbtnMarkAsAccept.Text = "Пометить как оплачено";
            this.tsbtnMarkAsAccept.Click += new System.EventHandler(this.tsbtnMarkAsAccept_Click);
            // 
            // tsbtnMarkAsDenied
            // 
            this.tsbtnMarkAsDenied.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnMarkAsDenied.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnMarkAsDenied.Image")));
            this.tsbtnMarkAsDenied.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnMarkAsDenied.Name = "tsbtnMarkAsDenied";
            this.tsbtnMarkAsDenied.Size = new System.Drawing.Size(23, 22);
            this.tsbtnMarkAsDenied.Text = "Оплата не выполнена";
            this.tsbtnMarkAsDenied.Click += new System.EventHandler(this.tsbtnMarkAsDenied_Click);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AutoGenerateColumns = false;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clPic,
            this.pQIDDataGridViewTextBoxColumn,
            this.pLASTNAMEDataGridViewTextBoxColumn,
            this.pFIRSTNAMEDataGridViewTextBoxColumn,
            this.pSECONDNAMEDataGridViewTextBoxColumn,
            this.pQPIDDataGridViewTextBoxColumn,
            this.pQSUMDataGridViewTextBoxColumn,
            this.pSQNAMEDataGridViewTextBoxColumn,
            this.pQPQSIDDataGridViewTextBoxColumn,
            this.pQDATEDataGridViewTextBoxColumn});
            this.dgvData.DataSource = this.pAYMENT_QUEUE_VIEWBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LimeGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.EnableHeadersVisualStyles = false;
            this.dgvData.GridColor = System.Drawing.Color.Silver;
            this.dgvData.Location = new System.Drawing.Point(0, 25);
            this.dgvData.MultiSelect = false;
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(1008, 656);
            this.dgvData.TabIndex = 19;
            // 
            // clPic
            // 
            this.clPic.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.clPic.HeaderText = "";
            this.clPic.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.clPic.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.clPic.Name = "clPic";
            this.clPic.ReadOnly = true;
            this.clPic.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clPic.Width = 20;
            // 
            // pQIDDataGridViewTextBoxColumn
            // 
            this.pQIDDataGridViewTextBoxColumn.DataPropertyName = "PQ_ID";
            this.pQIDDataGridViewTextBoxColumn.HeaderText = "PQ_ID";
            this.pQIDDataGridViewTextBoxColumn.Name = "pQIDDataGridViewTextBoxColumn";
            this.pQIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // pLASTNAMEDataGridViewTextBoxColumn
            // 
            this.pLASTNAMEDataGridViewTextBoxColumn.DataPropertyName = "P_LAST_NAME";
            this.pLASTNAMEDataGridViewTextBoxColumn.HeaderText = "Фамилия";
            this.pLASTNAMEDataGridViewTextBoxColumn.Name = "pLASTNAMEDataGridViewTextBoxColumn";
            // 
            // pFIRSTNAMEDataGridViewTextBoxColumn
            // 
            this.pFIRSTNAMEDataGridViewTextBoxColumn.DataPropertyName = "P_FIRST_NAME";
            this.pFIRSTNAMEDataGridViewTextBoxColumn.HeaderText = "Имя";
            this.pFIRSTNAMEDataGridViewTextBoxColumn.Name = "pFIRSTNAMEDataGridViewTextBoxColumn";
            // 
            // pSECONDNAMEDataGridViewTextBoxColumn
            // 
            this.pSECONDNAMEDataGridViewTextBoxColumn.DataPropertyName = "P_SECOND_NAME";
            this.pSECONDNAMEDataGridViewTextBoxColumn.HeaderText = "Отчество";
            this.pSECONDNAMEDataGridViewTextBoxColumn.Name = "pSECONDNAMEDataGridViewTextBoxColumn";
            // 
            // pQPIDDataGridViewTextBoxColumn
            // 
            this.pQPIDDataGridViewTextBoxColumn.DataPropertyName = "PQ_P_ID";
            this.pQPIDDataGridViewTextBoxColumn.HeaderText = "PQ_P_ID";
            this.pQPIDDataGridViewTextBoxColumn.Name = "pQPIDDataGridViewTextBoxColumn";
            this.pQPIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // pQSUMDataGridViewTextBoxColumn
            // 
            this.pQSUMDataGridViewTextBoxColumn.DataPropertyName = "PQ_SUM";
            this.pQSUMDataGridViewTextBoxColumn.HeaderText = "Сумма к оплате";
            this.pQSUMDataGridViewTextBoxColumn.Name = "pQSUMDataGridViewTextBoxColumn";
            // 
            // pSQNAMEDataGridViewTextBoxColumn
            // 
            this.pSQNAMEDataGridViewTextBoxColumn.DataPropertyName = "PSQ_NAME";
            this.pSQNAMEDataGridViewTextBoxColumn.HeaderText = "Статус";
            this.pSQNAMEDataGridViewTextBoxColumn.Name = "pSQNAMEDataGridViewTextBoxColumn";
            // 
            // pQPQSIDDataGridViewTextBoxColumn
            // 
            this.pQPQSIDDataGridViewTextBoxColumn.DataPropertyName = "PQ_PQS_ID";
            this.pQPQSIDDataGridViewTextBoxColumn.HeaderText = "PQ_PQS_ID";
            this.pQPQSIDDataGridViewTextBoxColumn.Name = "pQPQSIDDataGridViewTextBoxColumn";
            this.pQPQSIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // pQDATEDataGridViewTextBoxColumn
            // 
            this.pQDATEDataGridViewTextBoxColumn.DataPropertyName = "PQ_DATE";
            this.pQDATEDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.pQDATEDataGridViewTextBoxColumn.Name = "pQDATEDataGridViewTextBoxColumn";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnDeleteSelected
            // 
            this.tsbtnDeleteSelected.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnDeleteSelected.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnDeleteSelected.Image")));
            this.tsbtnDeleteSelected.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnDeleteSelected.Name = "tsbtnDeleteSelected";
            this.tsbtnDeleteSelected.Size = new System.Drawing.Size(23, 22);
            this.tsbtnDeleteSelected.Text = "Удалить";
            this.tsbtnDeleteSelected.Click += new System.EventHandler(this.tsbtnDeleteSelected_Click);
            // 
            // PaymentQueueView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 681);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.pAYMENT_QUEUE_VIEWBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PaymentQueueView";
            this.Text = "Очередь оплаты подавших документы на зачисление";
            this.Load += new System.EventHandler(this.PaymentQueueView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAYMENT_QUEUE_VIEWBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAYMENT_QUEUE_VIEWBindingNavigator)).EndInit();
            this.pAYMENT_QUEUE_VIEWBindingNavigator.ResumeLayout(false);
            this.pAYMENT_QUEUE_VIEWBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource pAYMENT_QUEUE_VIEWBindingSource;
        private DBDataSetTableAdapters.PAYMENT_QUEUE_VIEWTableAdapter pAYMENT_QUEUE_VIEWTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator pAYMENT_QUEUE_VIEWBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton pAYMENT_QUEUE_VIEWBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.DataGridViewImageColumn clPic;
        private System.Windows.Forms.DataGridViewTextBoxColumn pQIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pLASTNAMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pFIRSTNAMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pSECONDNAMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pQPIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pQSUMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pSQNAMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pQPQSIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pQDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton tsbtnCreateNew;
        private System.Windows.Forms.ToolStripButton tsbtnMarkAsAccept;
        private System.Windows.Forms.ToolStripButton tsbtnMarkAsDenied;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbtnDeleteSelected;
    }
}