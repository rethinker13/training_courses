﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class EnrollReceiptForm : Form
    {
        private string address;
        private string contract;
        private int course;
        private string firstName;
        private string info;
        private string lastName;
        private int pID;
        private string secondName;
        private int stID;
        private long _groupID;
        private string group_name;
        private string group_type;

        private decimal __sum;

        public decimal Sum
        {
            get
            {
                return __sum;
            }
        }

        public EnrollReceiptForm()
        {
            InitializeComponent();
        }

        public EnrollReceiptForm(long _groupID, int stID, int pID, string group_name, string lastName, 
            string firstName, string secondName, int course, string contract, string address, string info,
            string group_type) : this()
        {
            this._groupID = _groupID;
            this.stID = stID;
            this.pID = pID;
            this.lastName = lastName;
            this.firstName = firstName;
            this.secondName = secondName;
            this.course = course;
            this.contract = contract;
            this.address = address;
            this.info = info;
            this.group_name = group_name;
            this.group_type = group_type;

            StringBuilder sb = new StringBuilder();
            sb.Append(lastName);
            sb.Append(" ");
            sb.Append(firstName);
            sb.Append(" ");
            sb.Append(secondName);

            txtParent.Text = sb.ToString();
            txtAddress.Text = address;

            this.Text = "Квитанция оплаты: " + sb.ToString();
        }

        private void StudentReceiptForm_Load (object sender, EventArgs e)
        {
            //make_receipt();
        }

        private void make_receipt ()
        {
            if (String.IsNullOrEmpty(txtParent.Text))
            {
                MessageBox.Show("Введите корректное ФИО заказчика!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtAddress.Text))
            {
                MessageBox.Show("Введите адрес заказчика!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ((int)numContribution.Value <= 0)
            {
                MessageBox.Show("Введите корректную сумму!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String file = File.ReadAllText("reports\\stud_doc2.rdl");

            StringBuilder sb = new StringBuilder();
            sb.Append(lastName);
            sb.Append(" ");
            sb.Append(firstName);
            sb.Append(" ");
            sb.Append(secondName);

            file = file.Replace("_REP_NAME_", txtParent.Text);

            file = file.Replace("_REP_ADDRESS_", txtAddress.Text);

            __sum = numContribution.Value;

            file = file.Replace("_REP_SUM_", numContribution.Value.ToString());

            File.WriteAllText("reports\\stud_doc2_tmp.rdl", file);

            string filepath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "reports\\stud_doc2_tmp.rdl");
            rdlViewer1.SourceFile = new Uri(filepath);

            string exe_path = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);

            string group_path = "\\" + this.group_type + "\\";

            if (!Directory.Exists(exe_path + group_path))
                Directory.CreateDirectory(exe_path + group_path);
            if (!Directory.Exists(exe_path + group_path + group_name + "\\"))
                Directory.CreateDirectory(exe_path + group_path + group_name + "\\");
            if (!Directory.Exists(exe_path + group_path + group_name + "\\Квитанции\\"))
                Directory.CreateDirectory(exe_path + group_path + group_name + "\\Квитанции\\");

            rdlViewer1.SaveAs(exe_path + group_path + group_name + "\\Квитанции\\"
                + lastName + firstName + secondName + "_" + DateTime.Now.ToShortDateString() + ".pdf", fyiReporting.RDL.OutputPresentationType.PDF);
            rdlViewer1.Rebuild();
        }

        private void btnRebuild_Click(object sender, EventArgs e)
        {
            make_receipt();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
