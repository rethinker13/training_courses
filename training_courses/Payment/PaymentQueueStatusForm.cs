﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses.Payment
{
    public partial class PaymentQueueStatusForm : Form
    {
        public PaymentQueueStatusForm()
        {
            InitializeComponent();
        }

        private void pAY_QUEUE_STATUSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            this.Validate();
            this.pAY_QUEUE_STATUSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDataSet);

        }

        private void PaymentQueueStatusForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.PAY_QUEUE_STATUS' table. You can move, or remove it, as needed.
            this.pAY_QUEUE_STATUSTableAdapter.Fill(this.dBDataSet.PAY_QUEUE_STATUS);

        }
    }
}
