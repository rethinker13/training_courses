﻿namespace training_courses.Payment
{
    partial class PaymentQueueStatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentQueueStatusForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dBDataSet = new training_courses.DBDataSet();
            this.pAY_QUEUE_STATUSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAY_QUEUE_STATUSTableAdapter = new training_courses.DBDataSetTableAdapters.PAY_QUEUE_STATUSTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.pAY_QUEUE_STATUSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.clPic = new System.Windows.Forms.DataGridViewImageColumn();
            this.pQSIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pSQNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAY_QUEUE_STATUSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAY_QUEUE_STATUSBindingNavigator)).BeginInit();
            this.pAY_QUEUE_STATUSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pAY_QUEUE_STATUSBindingSource
            // 
            this.pAY_QUEUE_STATUSBindingSource.DataMember = "PAY_QUEUE_STATUS";
            this.pAY_QUEUE_STATUSBindingSource.DataSource = this.dBDataSet;
            // 
            // pAY_QUEUE_STATUSTableAdapter
            // 
            this.pAY_QUEUE_STATUSTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DOC_SUB_STATUSTableAdapter = null;
            this.tableAdapterManager.DOC_SUBMISSIONTableAdapter = null;
            this.tableAdapterManager.ENROLLMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.PAY_QUEUE_STATUSTableAdapter = this.pAY_QUEUE_STATUSTableAdapter;
            this.tableAdapterManager.PAYMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.STUDENT_PAYMENTSTableAdapter = null;
            this.tableAdapterManager.SUBJ_DOC_SUBMTableAdapter = null;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // pAY_QUEUE_STATUSBindingNavigator
            // 
            this.pAY_QUEUE_STATUSBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.pAY_QUEUE_STATUSBindingNavigator.BindingSource = this.pAY_QUEUE_STATUSBindingSource;
            this.pAY_QUEUE_STATUSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.pAY_QUEUE_STATUSBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.pAY_QUEUE_STATUSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem});
            this.pAY_QUEUE_STATUSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.pAY_QUEUE_STATUSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.pAY_QUEUE_STATUSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.pAY_QUEUE_STATUSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.pAY_QUEUE_STATUSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.pAY_QUEUE_STATUSBindingNavigator.Name = "pAY_QUEUE_STATUSBindingNavigator";
            this.pAY_QUEUE_STATUSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.pAY_QUEUE_STATUSBindingNavigator.Size = new System.Drawing.Size(704, 25);
            this.pAY_QUEUE_STATUSBindingNavigator.TabIndex = 0;
            this.pAY_QUEUE_STATUSBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 15);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // pAY_QUEUE_STATUSBindingNavigatorSaveItem
            // 
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("pAY_QUEUE_STATUSBindingNavigatorSaveItem.Image")));
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem.Name = "pAY_QUEUE_STATUSBindingNavigatorSaveItem";
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem.Text = "Save Data";
            this.pAY_QUEUE_STATUSBindingNavigatorSaveItem.Click += new System.EventHandler(this.pAY_QUEUE_STATUSBindingNavigatorSaveItem_Click);
            // 
            // dgvData
            // 
            this.dgvData.AutoGenerateColumns = false;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clPic,
            this.pQSIDDataGridViewTextBoxColumn,
            this.pSQNAMEDataGridViewTextBoxColumn});
            this.dgvData.DataSource = this.pAY_QUEUE_STATUSBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LimeGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.EnableHeadersVisualStyles = false;
            this.dgvData.GridColor = System.Drawing.Color.Silver;
            this.dgvData.Location = new System.Drawing.Point(0, 25);
            this.dgvData.MultiSelect = false;
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvData.Size = new System.Drawing.Size(704, 416);
            this.dgvData.TabIndex = 18;
            // 
            // clPic
            // 
            this.clPic.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.clPic.HeaderText = "";
            this.clPic.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.clPic.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.clPic.Name = "clPic";
            this.clPic.ReadOnly = true;
            this.clPic.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clPic.Width = 20;
            // 
            // pQSIDDataGridViewTextBoxColumn
            // 
            this.pQSIDDataGridViewTextBoxColumn.DataPropertyName = "PQS_ID";
            this.pQSIDDataGridViewTextBoxColumn.HeaderText = "PQS_ID";
            this.pQSIDDataGridViewTextBoxColumn.Name = "pQSIDDataGridViewTextBoxColumn";
            this.pQSIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // pSQNAMEDataGridViewTextBoxColumn
            // 
            this.pSQNAMEDataGridViewTextBoxColumn.DataPropertyName = "PSQ_NAME";
            this.pSQNAMEDataGridViewTextBoxColumn.HeaderText = "Наименование статуса";
            this.pSQNAMEDataGridViewTextBoxColumn.Name = "pSQNAMEDataGridViewTextBoxColumn";
            // 
            // PaymentQueueStatusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 441);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.pAY_QUEUE_STATUSBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PaymentQueueStatusForm";
            this.Text = "Статусы оплаты";
            this.Load += new System.EventHandler(this.PaymentQueueStatusForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAY_QUEUE_STATUSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAY_QUEUE_STATUSBindingNavigator)).EndInit();
            this.pAY_QUEUE_STATUSBindingNavigator.ResumeLayout(false);
            this.pAY_QUEUE_STATUSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource pAY_QUEUE_STATUSBindingSource;
        private DBDataSetTableAdapters.PAY_QUEUE_STATUSTableAdapter pAY_QUEUE_STATUSTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator pAY_QUEUE_STATUSBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton pAY_QUEUE_STATUSBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.DataGridViewImageColumn clPic;
        private System.Windows.Forms.DataGridViewTextBoxColumn pQSIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pSQNAMEDataGridViewTextBoxColumn;
    }
}