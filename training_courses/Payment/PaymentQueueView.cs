﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses.Payment
{
    public partial class PaymentQueueView : Form
    {

        private DBDataSetTableAdapters.T_PERSONTableAdapter personAdapter;
        private DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter docSubAdapter;
        private DBDataSetTableAdapters.PAYMENT_QUEUETableAdapter paymentAdapter;
        private DBDataSetTableAdapters.ENROLLMENT_QUEUETableAdapter eAdapter;
        private DBDataSetTableAdapters.SUBJ_DOC_SUBMTableAdapter sdsAdapter;

        public PaymentQueueView()
        {
            InitializeComponent();

            personAdapter = new DBDataSetTableAdapters.T_PERSONTableAdapter();
            personAdapter.ClearBeforeFill = true;

            docSubAdapter = new DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter();
            docSubAdapter.ClearBeforeFill = true;

            paymentAdapter = new DBDataSetTableAdapters.PAYMENT_QUEUETableAdapter();
            paymentAdapter.ClearBeforeFill = true;

            eAdapter = new DBDataSetTableAdapters.ENROLLMENT_QUEUETableAdapter();
            eAdapter.ClearBeforeFill = true;

            sdsAdapter = new DBDataSetTableAdapters.SUBJ_DOC_SUBMTableAdapter();
            sdsAdapter.ClearBeforeFill = true;
        }

        private void PaymentQueueView_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.PAYMENT_QUEUE_VIEW' table. You can move, or remove it, as needed.
            this.pAYMENT_QUEUE_VIEWTableAdapter.Fill(this.dBDataSet.PAYMENT_QUEUE_VIEW);
            updateTable();
        }

        private void tsbtnCreateNew_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);

                DBDataSet.DOC_SUBMISSIONDataTable dst = docSubAdapter.GetDataByP_ID(p_id);
                DBDataSet.DOC_SUBMISSIONRow dsr = dst.Rows[0] as DBDataSet.DOC_SUBMISSIONRow;

                EnrollReceiptForm erf = new EnrollReceiptForm(0, 0, 0, "Очередь оплаты на зачисление",
                    dgvData[2, dgvData.SelectedRows[0].Index].Value.ToString(),
                    dgvData[3, dgvData.SelectedRows[0].Index].Value.ToString(),
                    dgvData[4, dgvData.SelectedRows[0].Index].Value.ToString(),
                    0,
                    dsr.DS_CONTRACT,
                    dsr.DS_ADDRESS,
                    dsr.DS_INFO,
                    "Зачисление"
                    );
                if (erf.ShowDialog() == DialogResult.OK)
                {
                    decimal sum = erf.Sum;
                    paymentAdapter.Update(p_id, sum, 2, DateTime.Now, current_id);
                    docSubAdapter.UpdateQueryStatus(2, dsr.DS_ID);
                    PaymentQueueView_Load(sender, e);
                }
            }

        }

        private void tsbtnMarkAsAccept_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);

                DBDataSet.DOC_SUBMISSIONDataTable dst = docSubAdapter.GetDataByP_ID(p_id);
                DBDataSet.DOC_SUBMISSIONRow dsr = dst.Rows[0] as DBDataSet.DOC_SUBMISSIONRow;
                
                DBDataSet.SUBJ_DOC_SUBMDataTable sdst = sdsAdapter.GetDataBySDS_DS_ID(dsr.DS_ID);
                DBDataSet.SUBJ_DOC_SUBMRow sdsr = sdst.Rows[0] as DBDataSet.SUBJ_DOC_SUBMRow;
                if (sdst.Rows.Count == 0)
                {
                    MessageBox.Show("Невозможно перевести абитуриента в группу! Отсутствуют данные по предметам!",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                
                paymentAdapter.UpdateQueryStatus(3, current_id);
                docSubAdapter.UpdateQueryStatus(3, dsr.DS_ID);


                
                eAdapter.Insert(p_id, sdsr.SDS_ID);
                PaymentQueueView_Load(sender, e);

            }
        }

        private void tsbtnMarkAsDenied_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);

                DBDataSet.DOC_SUBMISSIONDataTable dst = docSubAdapter.GetDataByP_ID(p_id);
                DBDataSet.DOC_SUBMISSIONRow dsr = dst.Rows[0] as DBDataSet.DOC_SUBMISSIONRow;
                paymentAdapter.UpdateQueryStatus(4, current_id);
                docSubAdapter.UpdateQueryStatus(5, dsr.DS_ID);
                PaymentQueueView_Load(sender, e);
            }
        }

        private void updateTable()
        {
            foreach (DataGridViewRow row in dgvData.Rows)
            {
                //if (row.Cells[1].Value != null && row.Cells[1].Value.ToString() != "")
                //{
                //    DBDataSet.DOC_SUB_STATUSDataTable table = status_adapter.GetDataByID(Convert.ToInt32(row.Cells[7].Value));
                //    DBDataSet.DOC_SUB_STATUSRow st_row = table.Rows[0] as DBDataSet.DOC_SUB_STATUSRow;
                //    row.DefaultCellStyle.BackColor = Color.FromName(st_row.DSS_COLOR.Trim());
                //}
            }
        }

        private void tsbtnDeleteSelected_Click(object sender, EventArgs e)
        {
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);

                personAdapter.Delete(p_id);
                PaymentQueueView_Load(sender, e);
            }
        }
    }
}
