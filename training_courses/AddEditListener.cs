﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class AddEditListener : Form
    {
        private DBDataSetTableAdapters.T_PERSONTableAdapter personAdapter;
        private DBDataSetTableAdapters.T_LISTENERTableAdapter listenerAdapter;
        private long _groupID;
        private int _lID;
        private int _pID;
        private int _sID;
        private string _lastName;
        private string _firstName;
        private string _secondName;
        private string _edu;
        private string _contract;
        private int _time;
        private int _realTime;
        private bool _editMode;

        public AddEditListener (long groupID)
        {
            InitializeComponent();

            this._groupID = groupID;

            personAdapter = new DBDataSetTableAdapters.T_PERSONTableAdapter();
            personAdapter.ClearBeforeFill = true;

            listenerAdapter = new DBDataSetTableAdapters.T_LISTENERTableAdapter();
            listenerAdapter.ClearBeforeFill = true;

            personAdapter.Fill(dBDataSet.T_PERSON);
            listenerAdapter.Fill(dBDataSet.T_LISTENER);

            this.Validate();

            this.personAdapter.Update(this.dBDataSet);
            this.listenerAdapter.Update(this.dBDataSet);

            this.dBDataSet.T_PERSON.AcceptChanges();
            this.dBDataSet.T_LISTENER.AcceptChanges();

            this._editMode = false;
            this.Text = "Добавить запись";
        }

        public AddEditListener (long groupID, int lID, int pID, int sID, string lastName, 
            string firstName, string secondName, string edu, string contract, int time, int realTime) : this(groupID)
        {
            this._lID = lID;
            this._pID = pID;
            this._sID = sID;
            this._lastName = lastName;
            this._firstName = firstName;
            this._secondName = secondName;
            this._edu = edu;
            this._contract = contract;
            this._time = time;
            this._realTime = realTime;

            cmbSubject.SelectedValue = this._sID;
            txtLastName.Text = this._lastName;
            txtFirstName.Text = this._firstName;
            txtSecondName.Text = this._secondName;
            numContractTimes.Value = this._time;
            numRealTimes.Value = this._realTime;
            txtEduPlace.Text = this._edu;
            txtContractNumber.Text = this._contract;

            this._editMode = true;
            this.Text = "Редактировать запись";
        }

        private void btnCancel_Click (object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click (object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtLastName.Text) || String.IsNullOrEmpty(txtFirstName.Text)
                || String.IsNullOrEmpty(txtSecondName.Text))
            {
                MessageBox.Show("Введите корректное ФИО или заполните все поля!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtEduPlace.Text) )
            {
                MessageBox.Show("Введите учебное заведение!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtContractNumber.Text))
            {
                MessageBox.Show("Проверьте номер контракта!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ((int)numContractTimes.Value <= 0 || (int)numRealTimes.Value <= 0)
            {
                MessageBox.Show("Введите корректное число часов!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtLastName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Фамилия содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtFirstName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Имя содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtSecondName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Отчество содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if ((int)numContractTimes.Value < numRealTimes.Value)
            {
                MessageBox.Show("Количество посещенных часов не может быть больше отведенного времени!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!this._editMode)
            {
                DBDataSet.T_PERSONDataTable personTable = personAdapter.GetDataByName(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                long pID = 0;
                if (personTable.Rows.Count == 0)
                {
                    int res = personAdapter.Insert(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                    personTable = personAdapter.GetDataByName(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                    DBDataSet.T_PERSONRow personRow = (DBDataSet.T_PERSONRow)personTable.Rows[0];
                    pID = personRow.P_ID;
                }
                else
                {
                    DBDataSet.T_PERSONRow personRow = (DBDataSet.T_PERSONRow)personTable.Rows[0];
                    pID = personRow.P_ID;
                }

                listenerAdapter.Insert((int)pID, (int)(long)cmbSubject.SelectedValue, txtEduPlace.Text, txtContractNumber.Text, (int)numContractTimes.Value,
                    (int)numRealTimes.Value, (int)_groupID);
            }
            else
            {
                personAdapter.UpdateQueryByID(txtLastName.Text, txtFirstName.Text, txtSecondName.Text, this._pID);

                listenerAdapter.UpdateQueryByID(this._pID, (int)(long)cmbSubject.SelectedValue, txtEduPlace.Text, txtContractNumber.Text,
                    (int)numContractTimes.Value, (int)numRealTimes.Value, (int)this._groupID, this._lID);
            }
            this.Close();
        }

        private void AddEditListener_Load (object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_SUBJECT' table. You can move, or remove it, as needed.
            this.t_SUBJECTTableAdapter.Fill(this.dBDataSet.T_SUBJECT);

            cmbSubject.SelectedValue = this._sID;
        }
    }
}
