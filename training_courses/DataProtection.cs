﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace training_courses
{
    public class DataProtection
    {
        // Байтовый массив для дополнительной энтропии при использовании метода Protect.
        static byte[] s_aditionalEntropy = { 9, 8, 7, 6, 5 };

        public static byte[] Protect(byte[] data)
        {
            try
            {
                // Зашифруйте данные, используя DataProtectionScope.CurrentUser. 
                // Результат может быть расшифрован только тем же текущим пользователем.
                return ProtectedData.Protect(data, s_aditionalEntropy, DataProtectionScope.CurrentUser);
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("Ошибка при шифровании данных. См. лог вывода.");
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static byte[] Unprotect(byte[] data)
        {
            try
            {
                // Расшифруйте данные с помощью DataProtectionScope.CurrentUser.
                return ProtectedData.Unprotect(data, s_aditionalEntropy, DataProtectionScope.CurrentUser);
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("Ошибка при дешифровании данных. См. лог вывода.");
                Console.WriteLine(e.ToString());
                return null;
            }
        }
    }
}
