﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class ListenerReceiptForm : Form
    {
        private string contract;
        private string edu;
        private string firstName;
        private string lastName;
        private int lID;
        private int pID;
        private string secondName;
        private string group_name;

        private DBDataSetTableAdapters.T_LISTENER_PRICESTableAdapter lp_adapter;
        private int glID;

        public ListenerReceiptForm ()
        {
            InitializeComponent();

            lp_adapter = new DBDataSetTableAdapters.T_LISTENER_PRICESTableAdapter();
            lp_adapter.ClearBeforeFill = false;
        }

        public ListenerReceiptForm (int lID, int pID, string group_name, string lastName, 
            string firstName, string secondName, string edu, string contract) : this()
        {
            this.lID = lID;
            this.pID = pID;
            this.group_name = group_name;
            this.lastName = lastName;
            this.firstName = firstName;
            this.secondName = secondName;
            this.edu = edu;
            this.contract = contract;

            StringBuilder sb = new StringBuilder();
            sb.Append(lastName);
            sb.Append(" ");
            sb.Append(firstName);
            sb.Append(" ");
            sb.Append(secondName);

            this.Text = "Квитанция оплаты: " + sb.ToString();
        }

        public ListenerReceiptForm(int lID, int pID, string group_name, string lastName, string firstName, string secondName, 
            string edu, string contract, int glID) : this(lID, pID, group_name, lastName, firstName, secondName, edu, contract)
        {
            this.glID = glID;
        }

        private void ListenerReceiptForm_Load (object sender, EventArgs e)
        {
            //make_receipt();

            DBDataSet.T_LISTENER_PRICESDataTable table = lp_adapter.GetDataByGroupID(glID);
            if (table.Rows.Count > 0)
            {
                DBDataSet.T_LISTENER_PRICESRow row = table.Rows[0] as DBDataSet.T_LISTENER_PRICESRow;
                numContribution.Value = (decimal)row.TLP_PRICE;
            }
        }

        private void make_receipt ()
        {
            if (String.IsNullOrEmpty(txtParent.Text))
            {
                MessageBox.Show("Введите корректное ФИО заказчика!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtAddress.Text))
            {
                MessageBox.Show("Введите адрес заказчика!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ((int)numContribution.Value <= 0)
            {
                MessageBox.Show("Введите корректную сумму!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            String file = File.ReadAllText("reports\\stud_doc.rdl");
            
            file = file.Replace("_REP_CUST_NAME_", txtParent.Text);

            file = file.Replace("_REP_ADDRESS_", txtAddress.Text);

            file = file.Replace("_REP_SUM_", numContribution.Value.ToString());

            StringBuilder sb = new StringBuilder();
            sb.Append(lastName);
            sb.Append(" ");
            sb.Append(firstName);
            sb.Append(" ");
            sb.Append(secondName);

            file = file.Replace("_REP_NAME_", sb.ToString());

            //file = file.Replace("_REP_ADDRESS_", address);

            File.WriteAllText("reports\\stud_doc_tmp.rdl", file);

            string filepath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "reports\\stud_doc_tmp.rdl");
            rdlViewer1.SourceFile = new Uri(filepath);

            string exe_path = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);

            if (!Directory.Exists(exe_path + "\\Подготовительные группы\\"))
                Directory.CreateDirectory(exe_path + "\\Подготовительные группы\\");
            if (!Directory.Exists(exe_path + "\\Подготовительные группы\\" + group_name + "\\"))
                Directory.CreateDirectory(exe_path + "\\Подготовительные группы\\" + group_name + "\\");
            if (!Directory.Exists(exe_path + "\\Подготовительные группы\\" + group_name + "\\Квитанции\\"))
                Directory.CreateDirectory(exe_path + "\\Подготовительные группы\\" + group_name + "\\Квитанции\\");

            rdlViewer1.SaveAs(exe_path + "\\Подготовительные группы\\" + group_name + "\\Квитанции\\" 
                + lastName + firstName + secondName + "_" + DateTime.Now.ToShortDateString() + ".pdf", fyiReporting.RDL.OutputPresentationType.PDF);
            rdlViewer1.Rebuild();
            
        }

        private void btnRebuild_Click (object sender, EventArgs e)
        {
            make_receipt();
        }
    }
}
