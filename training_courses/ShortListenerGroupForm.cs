﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class ShortListenerGroupForm : Form
    {
        private long _groupID;
        private string _groupName;

        private DBDataSetTableAdapters.V_LISTENERSTableAdapter listenerAdapter;

        public ShortListenerGroupForm(long groupID, string groupName)
        {
            InitializeComponent();

            this._groupID = groupID;
            this._groupName = groupName;

            listenerAdapter = new DBDataSetTableAdapters.V_LISTENERSTableAdapter();
            listenerAdapter.ClearBeforeFill = true;
        }

        private void ShortListenerGroupForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.V_SHORT_LISTENERS' table. You can move, or remove it, as needed.
            this.v_SHORT_LISTENERSTableAdapter.FillByGlID(this.dBDataSet.V_SHORT_LISTENERS, (int)_groupID);

            if (AccessLevel.level == AccessLevel.Level.Courses)
            {
                tbReport.Visible = tbReceipt.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                tbReceipt.Visible = true;
                tbReport.Visible = false;
                tsEditLGroup.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Manager)
            {
                tbReceipt.Visible = false;
                tbReport.Visible = true;
            }

            if (AccessLevel.level == AccessLevel.Level.Engineer)
            {
                tbReceipt.Visible = false;
                tbReport.Visible = false;
            }
        }

        private void tsEditLGroup_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            ListenerForm listenerForm = new ListenerForm(_groupID, _groupName);
            foreach (Form form in ((FrmMain)this.ParentForm).MdiChildren)
            {
                if (form.GetType() == typeof(ListenerForm))
                    listenerForm = (ListenerForm)form;
            }
            listenerForm.Text = "Подготовительная группа " + _groupName;
            listenerForm.MdiParent = (FrmMain)this.ParentForm;
            listenerForm.Show();
        }

        private void tbReport_Click(object sender, EventArgs e)
        {
            if (v_SHORT_LISTENERSDataGridView.SelectedCells.Count == 0)
                return;

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int row_idx = v_SHORT_LISTENERSDataGridView.SelectedCells[0].RowIndex;
            DBDataSet.V_LISTENERSDataTable table = 
                this.listenerAdapter.GetDataByGlIDandPID((int)(long)v_SHORT_LISTENERSDataGridView.Rows[row_idx].Cells[1].Value, (int)_groupID);
            DBDataSet.V_LISTENERSRow row = table.Rows[0] as DBDataSet.V_LISTENERSRow;

            int lID = (int)(long)row.L_ID;
            int pID = (int)(long)row.P_ID;
            int sID = (int)(long)row.S_ID;
            string lastName = row.P_LAST_NAME;
            string firstName = row.P_FIRST_NAME;
            string secondName = row.P_SECOND_NAME;
            string edu = row.L_EDU_PLACE;
            string contract = row.L_CONTRACT_NUMBER;
            int time = (int)row.L_TIME;
            int realTime = (int)row.L_REAL_TIME;
            int glID = (int)(long)row.GL_ID;

            ContractForm contractForm = new ContractForm(lID, pID, lastName, firstName, secondName, edu, contract);
            contractForm.ShowDialog();

            ShortListenerGroupForm_Load(sender, e);
        }

        private void tbReceipt_Click(object sender, EventArgs e)
        {
            if (v_SHORT_LISTENERSDataGridView.SelectedCells.Count == 0)
                return;

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int row_idx = v_SHORT_LISTENERSDataGridView.SelectedCells[0].RowIndex;
            DBDataSet.V_LISTENERSDataTable table =
                this.listenerAdapter.GetDataByGlIDandPID((int)(long)v_SHORT_LISTENERSDataGridView.Rows[row_idx].Cells[1].Value, (int)_groupID);
            DBDataSet.V_LISTENERSRow row = table.Rows[0] as DBDataSet.V_LISTENERSRow;

            int lID = (int)(long)row.L_ID;
            int pID = (int)(long)row.P_ID;
            int sID = (int)(long)row.S_ID;
            string lastName = row.P_LAST_NAME;
            string firstName = row.P_FIRST_NAME;
            string secondName = row.P_SECOND_NAME;
            string edu = row.L_EDU_PLACE;
            string contract = row.L_CONTRACT_NUMBER;
            int time = (int)row.L_TIME;
            int realTime = (int)row.L_REAL_TIME;
            int glID = (int)(long)row.GL_ID;

            ListenerReceiptForm listenerReceiptForm = new ListenerReceiptForm(lID, pID, _groupName, lastName, firstName, 
                secondName, edu, contract, glID);
            listenerReceiptForm.ShowDialog();

            ShortListenerGroupForm_Load(sender, e);
        }
    }
}
