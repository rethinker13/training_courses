﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using training_courses.Enrollment;
using training_courses.Payment;
using training_courses.Submission;

namespace training_courses
{
    public partial class FrmMain : Form
    {
        private string user;

        public string dbPath { get; private set; }
        public string roleName { get; private set; }
        public bool waitLogin { get; private set; }
        public string CurrentDBName { get; internal set; }
        public string CurrentUserName { get; internal set; }

        public FrmMain ()
        {
            InitializeComponent();
        }

        public FrmMain (string user, string dbPath, string roleName) : this()
        { 
            this.user = user;
            this.dbPath = dbPath;
            this.roleName = roleName;
        }

        private void exitToolStripMenuItem_Click (object sender, EventArgs e)
        {
            waitLogin = true;
            this.Close();
        }

        private void FrmMain_Load (object sender, EventArgs e)
        {
            checkRole();

            this.WindowState = FormWindowState.Maximized;
            this.tsConnection.Text = "Подключение: " + dbPath;
            this.tsUser.Text = "Пользователь: " + user;
            this.tsRole.Text = "Роль: " + roleName;
        }

        private void checkRole ()
        {
            if (AccessLevel.level != AccessLevel.Level.Admin)
            {
                controlToolStripMenuItem.Visible = false;
            }
            if (AccessLevel.level == AccessLevel.Level.Courses)
            {
                studentMenuToolStripMenuItem.Visible = false;
                fullStudentsMenuToolStripMenuItem.Visible = false;
                subjectsToolStripMenuItem.Visible = false;
                accountingToolStripMenuItem.Visible = false;
                listenerMenuToolStripMenuItem.Visible = true;
                mastersToolStripMenuItem.Visible = false;
                sVEToolStripMenuItem.Visible = false;
                enrollmentToolStripMenuItem.Visible = false;
            }
            if (AccessLevel.level == AccessLevel.Level.Students)
            {
                studentMenuToolStripMenuItem.Visible = true;
                fullStudentsMenuToolStripMenuItem.Visible = true;
                subjectsToolStripMenuItem.Visible = false;
                accountingToolStripMenuItem.Visible = false;
                listenerMenuToolStripMenuItem.Visible = false;
                mastersToolStripMenuItem.Visible = false;
                sVEToolStripMenuItem.Visible = false;
                enrollmentToolStripMenuItem.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                studentMenuToolStripMenuItem.Visible = true;
                fullStudentsMenuToolStripMenuItem.Visible = true;
                listenerMenuToolStripMenuItem.Visible = true;
                subjectsToolStripMenuItem.Visible = false;
                accountingToolStripMenuItem.Visible = true;
                mastersToolStripMenuItem.Visible = true;
                sVEToolStripMenuItem.Visible = true;
                enrollmentToolStripMenuItem.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Manager)
            {
                studentMenuToolStripMenuItem.Visible = true;
                fullStudentsMenuToolStripMenuItem.Visible = true;
                listenerMenuToolStripMenuItem.Visible = true;
                subjectsToolStripMenuItem.Visible = true;
                mastersToolStripMenuItem.Visible = true;
                sVEToolStripMenuItem.Visible = true;
                accountingToolStripMenuItem.Visible = false;
                enrollmentQueueToolStripMenuItem.Visible = true;
                enrollmentViewToolStripMenuItem.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Engineer)
            {
                studentMenuToolStripMenuItem.Visible = true;
                fullStudentsMenuToolStripMenuItem.Visible = true;
                listenerMenuToolStripMenuItem.Visible = true;
                subjectsToolStripMenuItem.Visible = true;
                accountingToolStripMenuItem.Visible = false;
                mastersToolStripMenuItem.Visible = true;
                sVEToolStripMenuItem.Visible = true;
                submissionStatusToolStripMenuItem.Visible = false;
                submissionToolStripMenuItem.Visible = false;
                enrollmentViewToolStripMenuItem.Visible = true;
            }
        }

        private string connectionStatus ()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Подключение: ");
            sb.Append(dbPath);
            sb.Append(" Пользователь: ");
            sb.Append(user);
            sb.Append("; Роль: ");
            sb.Append(roleName);
            return sb.ToString();
        }

        private void showListenersToolStripMenuItem_Click (object sender, EventArgs e)
        {
            ListenerGroupForm listenerGroupForm = new ListenerGroupForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(ListenerGroupForm))
                    listenerGroupForm = (ListenerGroupForm)form;
            }
            listenerGroupForm.MdiParent = this;
            listenerGroupForm.Show();

        }

        private void subjectsToolStripMenuItem_Click (object sender, EventArgs e)
        {
            SubjectForm subjectForm = new SubjectForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(SubjectForm))
                    subjectForm = (SubjectForm)form;
            }
            subjectForm.MdiParent = this;
            subjectForm.Show();
        }

        private void showStudentsToolStripMenuItem_Click (object sender, EventArgs e)
        {
            StudentGroupForm studentGroupForm = new StudentGroupForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(StudentGroupForm))
                    studentGroupForm = (StudentGroupForm)form;
            }
            studentGroupForm.MdiParent = this;
            studentGroupForm.Show();
        }

        private void userManagementToolStripMenuItem_Click (object sender, EventArgs e)
        {
            UserForm userForm = new UserForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(UserForm))
                    userForm = (UserForm)form;
            }
            userForm.MdiParent = this;
            userForm.Show();
        }

        private void exitMenuItem_Click (object sender, EventArgs e)
        {
            waitLogin = true;
            this.Close();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            waitLogin = true;
        }

        private void fullGroupsAndStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FullStudentGroupForm studentGroupForm = new FullStudentGroupForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(FullStudentGroupForm))
                    studentGroupForm = (FullStudentGroupForm)form;
            }
            studentGroupForm.MdiParent = this;
            studentGroupForm.Show();
        }

        private void mastersGroupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MastersForm masters = new MastersForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(MastersForm))
                    masters = (MastersForm)form;
            }
            masters.MdiParent = this;
            masters.Show();
        }

        private void sveGroupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SVEForm sve = new SVEForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(SVEForm))
                    sve = (SVEForm)form;
            }
            sve.MdiParent = this;
            sve.Show();
        }

        private void listenerGroupPricesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListenerPricesForm lpf = new ListenerPricesForm();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(ListenerPricesForm))
                    lpf = (ListenerPricesForm)form;
            }
            lpf.MdiParent = this;
            lpf.Show();
        }

        private void studentGroupPricesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllStudentPricesForm spf = new AllStudentPricesForm(GroupType.Type.Student);
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(AllStudentPricesForm))
                    spf = (AllStudentPricesForm)form;
            }
            spf.MdiParent = this;
            spf.Show();
        }

        private void fullStudentGroupPricesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllStudentPricesForm spf = new AllStudentPricesForm(GroupType.Type.FullStudent);
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(AllStudentPricesForm))
                    spf = (AllStudentPricesForm)form;
            }
            spf.MdiParent = this;
            spf.Show();
        }

        private void magisterGroupPricesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllStudentPricesForm spf = new AllStudentPricesForm(GroupType.Type.Master);
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(AllStudentPricesForm))
                    spf = (AllStudentPricesForm)form;
            }
            spf.MdiParent = this;
            spf.Show();
        }

        private void sVEGroupPricesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllStudentPricesForm spf = new AllStudentPricesForm(GroupType.Type.SVE);
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(AllStudentPricesForm))
                    spf = (AllStudentPricesForm)form;
            }
            spf.MdiParent = this;
            spf.Show();
        }

        private void submissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubmissionView sv = new SubmissionView(); ;
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(SubmissionView))
                    sv = (SubmissionView)form;
            }
            sv.MdiParent = this;
            sv.Show();
        }

        private void submissionStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubmissionStatusForm ssf = new SubmissionStatusForm(); ;
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(SubmissionStatusForm))
                    ssf = (SubmissionStatusForm)form;
            }
            ssf.MdiParent = this;
            ssf.Show();
        }

        private void queuePaymentStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PaymentQueueStatusForm pqsf = new PaymentQueueStatusForm(); ;
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(PaymentQueueStatusForm))
                    pqsf = (PaymentQueueStatusForm)form;
            }
            pqsf.MdiParent = this;
            pqsf.Show();
        }

        private void eQPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PaymentQueueView pqv = new PaymentQueueView(); 
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(PaymentQueueView))
                    pqv = (PaymentQueueView)form;
            }
            pqv.MdiParent = this;
            pqv.Show();
        }

        private void enrollmentViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EnrollmentView ev = new EnrollmentView();
            foreach (Form form in this.MdiChildren)
            {
                if (form.GetType() == typeof(EnrollmentView))
                    ev = (EnrollmentView)form;
            }
            ev.MdiParent = this;
            ev.Show();
        }
    }
}
