﻿namespace training_courses
{
    partial class ShortListenerGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShortListenerGroupForm));
            this.dBDataSet = new training_courses.DBDataSet();
            this.v_SHORT_LISTENERSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.v_SHORT_LISTENERSTableAdapter = new training_courses.DBDataSetTableAdapters.V_SHORT_LISTENERSTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.v_SHORT_LISTENERSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tsEditLGroup = new System.Windows.Forms.ToolStripButton();
            this.tbReport = new System.Windows.Forms.ToolStripButton();
            this.tbReceipt = new System.Windows.Forms.ToolStripButton();
            this.v_SHORT_LISTENERSDataGridView = new System.Windows.Forms.DataGridView();
            this.GL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_SHORT_LISTENERSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_SHORT_LISTENERSBindingNavigator)).BeginInit();
            this.v_SHORT_LISTENERSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_SHORT_LISTENERSDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // v_SHORT_LISTENERSBindingSource
            // 
            this.v_SHORT_LISTENERSBindingSource.DataMember = "V_SHORT_LISTENERS";
            this.v_SHORT_LISTENERSBindingSource.DataSource = this.dBDataSet;
            // 
            // v_SHORT_LISTENERSTableAdapter
            // 
            this.v_SHORT_LISTENERSTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // v_SHORT_LISTENERSBindingNavigator
            // 
            this.v_SHORT_LISTENERSBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.v_SHORT_LISTENERSBindingNavigator.BindingSource = this.v_SHORT_LISTENERSBindingSource;
            this.v_SHORT_LISTENERSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.v_SHORT_LISTENERSBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.v_SHORT_LISTENERSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem,
            this.tsEditLGroup,
            this.tbReport,
            this.tbReceipt});
            this.v_SHORT_LISTENERSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.v_SHORT_LISTENERSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.v_SHORT_LISTENERSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.v_SHORT_LISTENERSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.v_SHORT_LISTENERSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.v_SHORT_LISTENERSBindingNavigator.Name = "v_SHORT_LISTENERSBindingNavigator";
            this.v_SHORT_LISTENERSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.v_SHORT_LISTENERSBindingNavigator.Size = new System.Drawing.Size(1008, 25);
            this.v_SHORT_LISTENERSBindingNavigator.TabIndex = 0;
            this.v_SHORT_LISTENERSBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // v_SHORT_LISTENERSBindingNavigatorSaveItem
            // 
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem.Enabled = false;
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("v_SHORT_LISTENERSBindingNavigatorSaveItem.Image")));
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem.Name = "v_SHORT_LISTENERSBindingNavigatorSaveItem";
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem.Text = "Save Data";
            this.v_SHORT_LISTENERSBindingNavigatorSaveItem.Visible = false;
            // 
            // tsEditLGroup
            // 
            this.tsEditLGroup.Image = global::training_courses.Properties.Resources.edit_128;
            this.tsEditLGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsEditLGroup.Name = "tsEditLGroup";
            this.tsEditLGroup.Size = new System.Drawing.Size(160, 22);
            this.tsEditLGroup.Text = "Редактирование группы";
            this.tsEditLGroup.Click += new System.EventHandler(this.tsEditLGroup_Click);
            // 
            // tbReport
            // 
            this.tbReport.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.tbReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbReport.Name = "tbReport";
            this.tbReport.Size = new System.Drawing.Size(159, 22);
            this.tbReport.Text = "Сформировать договор";
            this.tbReport.ToolTipText = "Сформировать договор";
            this.tbReport.Click += new System.EventHandler(this.tbReport_Click);
            // 
            // tbReceipt
            // 
            this.tbReceipt.Image = global::training_courses.Properties.Resources.if_icons_12_799766;
            this.tbReceipt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbReceipt.Name = "tbReceipt";
            this.tbReceipt.Size = new System.Drawing.Size(175, 22);
            this.tbReceipt.Text = "Сформировать квитанцию";
            this.tbReceipt.Click += new System.EventHandler(this.tbReceipt_Click);
            // 
            // v_SHORT_LISTENERSDataGridView
            // 
            this.v_SHORT_LISTENERSDataGridView.AllowUserToAddRows = false;
            this.v_SHORT_LISTENERSDataGridView.AutoGenerateColumns = false;
            this.v_SHORT_LISTENERSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.v_SHORT_LISTENERSDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GL_ID,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.v_SHORT_LISTENERSDataGridView.DataSource = this.v_SHORT_LISTENERSBindingSource;
            this.v_SHORT_LISTENERSDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.v_SHORT_LISTENERSDataGridView.Location = new System.Drawing.Point(0, 25);
            this.v_SHORT_LISTENERSDataGridView.MultiSelect = false;
            this.v_SHORT_LISTENERSDataGridView.Name = "v_SHORT_LISTENERSDataGridView";
            this.v_SHORT_LISTENERSDataGridView.Size = new System.Drawing.Size(1008, 629);
            this.v_SHORT_LISTENERSDataGridView.TabIndex = 1;
            // 
            // GL_ID
            // 
            this.GL_ID.DataPropertyName = "GL_ID";
            this.GL_ID.HeaderText = "GL_ID";
            this.GL_ID.Name = "GL_ID";
            this.GL_ID.ReadOnly = true;
            this.GL_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "P_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "P_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "P_LAST_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Фамилия";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "P_FIRST_NAME";
            this.dataGridViewTextBoxColumn3.HeaderText = "Имя";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "P_SECOND_NAME";
            this.dataGridViewTextBoxColumn4.HeaderText = "Отчество";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "SUBJECTS";
            this.dataGridViewTextBoxColumn5.HeaderText = "Посещаемые предметы";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // ShortListenerGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 654);
            this.Controls.Add(this.v_SHORT_LISTENERSDataGridView);
            this.Controls.Add(this.v_SHORT_LISTENERSBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShortListenerGroupForm";
            this.Text = "ShortListenerGroupForm";
            this.Load += new System.EventHandler(this.ShortListenerGroupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_SHORT_LISTENERSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_SHORT_LISTENERSBindingNavigator)).EndInit();
            this.v_SHORT_LISTENERSBindingNavigator.ResumeLayout(false);
            this.v_SHORT_LISTENERSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_SHORT_LISTENERSDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource v_SHORT_LISTENERSBindingSource;
        private DBDataSetTableAdapters.V_SHORT_LISTENERSTableAdapter v_SHORT_LISTENERSTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator v_SHORT_LISTENERSBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton v_SHORT_LISTENERSBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView v_SHORT_LISTENERSDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn GL_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.ToolStripButton tsEditLGroup;
        private System.Windows.Forms.ToolStripButton tbReport;
        private System.Windows.Forms.ToolStripButton tbReceipt;
    }
}