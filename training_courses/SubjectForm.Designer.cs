﻿namespace training_courses
{
    partial class SubjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubjectForm));
            this.dBDataSet = new training_courses.DBDataSet();
            this.t_SUBJECTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.t_SUBJECTTableAdapter = new training_courses.DBDataSetTableAdapters.T_SUBJECTTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.t_SUBJECTBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.t_SUBJECTBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.t_SUBJECTDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.S_HOUR_PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_SUBJECTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_SUBJECTBindingNavigator)).BeginInit();
            this.t_SUBJECTBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_SUBJECTDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // t_SUBJECTBindingSource
            // 
            this.t_SUBJECTBindingSource.DataMember = "T_SUBJECT";
            this.t_SUBJECTBindingSource.DataSource = this.dBDataSet;
            // 
            // t_SUBJECTTableAdapter
            // 
            this.t_SUBJECTTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DOC_SUB_STATUSTableAdapter = null;
            this.tableAdapterManager.DOC_SUBMISSIONTableAdapter = null;
            this.tableAdapterManager.ENROLLMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.PAY_QUEUE_STATUSTableAdapter = null;
            this.tableAdapterManager.PAYMENT_QUEUETableAdapter = null;
            this.tableAdapterManager.STUDENT_PAYMENTSTableAdapter = null;
            this.tableAdapterManager.SUBJ_DOC_SUBMTableAdapter = null;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = this.t_SUBJECTTableAdapter;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // t_SUBJECTBindingNavigator
            // 
            this.t_SUBJECTBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.t_SUBJECTBindingNavigator.BindingSource = this.t_SUBJECTBindingSource;
            this.t_SUBJECTBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.t_SUBJECTBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.t_SUBJECTBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.t_SUBJECTBindingNavigatorSaveItem});
            this.t_SUBJECTBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.t_SUBJECTBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.t_SUBJECTBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.t_SUBJECTBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.t_SUBJECTBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.t_SUBJECTBindingNavigator.Name = "t_SUBJECTBindingNavigator";
            this.t_SUBJECTBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.t_SUBJECTBindingNavigator.Size = new System.Drawing.Size(794, 25);
            this.t_SUBJECTBindingNavigator.TabIndex = 0;
            this.t_SUBJECTBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // t_SUBJECTBindingNavigatorSaveItem
            // 
            this.t_SUBJECTBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.t_SUBJECTBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("t_SUBJECTBindingNavigatorSaveItem.Image")));
            this.t_SUBJECTBindingNavigatorSaveItem.Name = "t_SUBJECTBindingNavigatorSaveItem";
            this.t_SUBJECTBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.t_SUBJECTBindingNavigatorSaveItem.Text = "Save Data";
            this.t_SUBJECTBindingNavigatorSaveItem.Click += new System.EventHandler(this.t_SUBJECTBindingNavigatorSaveItem_Click);
            // 
            // t_SUBJECTDataGridView
            // 
            this.t_SUBJECTDataGridView.AutoGenerateColumns = false;
            this.t_SUBJECTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.t_SUBJECTDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.S_HOUR_PRICE});
            this.t_SUBJECTDataGridView.DataSource = this.t_SUBJECTBindingSource;
            this.t_SUBJECTDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t_SUBJECTDataGridView.Location = new System.Drawing.Point(0, 25);
            this.t_SUBJECTDataGridView.Name = "t_SUBJECTDataGridView";
            this.t_SUBJECTDataGridView.Size = new System.Drawing.Size(794, 502);
            this.t_SUBJECTDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "S_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "S_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "S_NAME";
            this.dataGridViewTextBoxColumn2.FillWeight = 70F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Название";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // S_HOUR_PRICE
            // 
            this.S_HOUR_PRICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.S_HOUR_PRICE.DataPropertyName = "S_HOUR_PRICE";
            this.S_HOUR_PRICE.FillWeight = 30F;
            this.S_HOUR_PRICE.HeaderText = "Цена за 1 час";
            this.S_HOUR_PRICE.Name = "S_HOUR_PRICE";
            // 
            // SubjectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 527);
            this.Controls.Add(this.t_SUBJECTDataGridView);
            this.Controls.Add(this.t_SUBJECTBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SubjectForm";
            this.Text = "Предметы";
            this.Load += new System.EventHandler(this.SubjectForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_SUBJECTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_SUBJECTBindingNavigator)).EndInit();
            this.t_SUBJECTBindingNavigator.ResumeLayout(false);
            this.t_SUBJECTBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_SUBJECTDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource t_SUBJECTBindingSource;
        private DBDataSetTableAdapters.T_SUBJECTTableAdapter t_SUBJECTTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator t_SUBJECTBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton t_SUBJECTBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView t_SUBJECTDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn S_HOUR_PRICE;
    }
}