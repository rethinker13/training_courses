﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class ListenerPricesForm : Form
    {
        public ListenerPricesForm()
        {
            InitializeComponent();
        }

        private void t_LISTENER_PRICESBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.t_LISTENER_PRICESBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDataSet);

        }

        private void ListenerPricesForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_GROUP_LISTENER' table. You can move, or remove it, as needed.
            this.t_GROUP_LISTENERTableAdapter.Fill(this.dBDataSet.T_GROUP_LISTENER);
            // TODO: This line of code loads data into the 'dBDataSet.T_LISTENER_PRICES' table. You can move, or remove it, as needed.
            this.t_LISTENER_PRICESTableAdapter.Fill(this.dBDataSet.T_LISTENER_PRICES);

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            
        }
    }
}
