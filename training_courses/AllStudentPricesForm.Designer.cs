﻿namespace training_courses
{
    partial class AllStudentPricesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AllStudentPricesForm));
            this.dBDataSet = new training_courses.DBDataSet();
            this.t_GROUP_PRICESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.t_GROUP_PRICESTableAdapter = new training_courses.DBDataSetTableAdapters.T_GROUP_PRICESTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.t_GROUPTableAdapter = new training_courses.DBDataSetTableAdapters.T_GROUPTableAdapter();
            this.t_GROUP_PRICESBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.t_GROUP_PRICESBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.t_GROUP_PRICESDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tGROUPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_PRICESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_PRICESBindingNavigator)).BeginInit();
            this.t_GROUP_PRICESBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_PRICESDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tGROUPBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // t_GROUP_PRICESBindingSource
            // 
            this.t_GROUP_PRICESBindingSource.DataMember = "T_GROUP_PRICES";
            this.t_GROUP_PRICESBindingSource.DataSource = this.dBDataSet;
            // 
            // t_GROUP_PRICESTableAdapter
            // 
            this.t_GROUP_PRICESTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = this.t_GROUP_PRICESTableAdapter;
            this.tableAdapterManager.T_GROUPTableAdapter = this.t_GROUPTableAdapter;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // t_GROUPTableAdapter
            // 
            this.t_GROUPTableAdapter.ClearBeforeFill = true;
            // 
            // t_GROUP_PRICESBindingNavigator
            // 
            this.t_GROUP_PRICESBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.t_GROUP_PRICESBindingNavigator.BindingSource = this.t_GROUP_PRICESBindingSource;
            this.t_GROUP_PRICESBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.t_GROUP_PRICESBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.t_GROUP_PRICESBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.t_GROUP_PRICESBindingNavigatorSaveItem});
            this.t_GROUP_PRICESBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.t_GROUP_PRICESBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.t_GROUP_PRICESBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.t_GROUP_PRICESBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.t_GROUP_PRICESBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.t_GROUP_PRICESBindingNavigator.Name = "t_GROUP_PRICESBindingNavigator";
            this.t_GROUP_PRICESBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.t_GROUP_PRICESBindingNavigator.Size = new System.Drawing.Size(717, 25);
            this.t_GROUP_PRICESBindingNavigator.TabIndex = 0;
            this.t_GROUP_PRICESBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // t_GROUP_PRICESBindingNavigatorSaveItem
            // 
            this.t_GROUP_PRICESBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.t_GROUP_PRICESBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("t_GROUP_PRICESBindingNavigatorSaveItem.Image")));
            this.t_GROUP_PRICESBindingNavigatorSaveItem.Name = "t_GROUP_PRICESBindingNavigatorSaveItem";
            this.t_GROUP_PRICESBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.t_GROUP_PRICESBindingNavigatorSaveItem.Text = "Save Data";
            this.t_GROUP_PRICESBindingNavigatorSaveItem.Click += new System.EventHandler(this.t_GROUP_PRICESBindingNavigatorSaveItem_Click);
            // 
            // t_GROUP_PRICESDataGridView
            // 
            this.t_GROUP_PRICESDataGridView.AutoGenerateColumns = false;
            this.t_GROUP_PRICESDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.t_GROUP_PRICESDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.t_GROUP_PRICESDataGridView.DataSource = this.t_GROUP_PRICESBindingSource;
            this.t_GROUP_PRICESDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t_GROUP_PRICESDataGridView.Location = new System.Drawing.Point(0, 25);
            this.t_GROUP_PRICESDataGridView.Name = "t_GROUP_PRICESDataGridView";
            this.t_GROUP_PRICESDataGridView.Size = new System.Drawing.Size(717, 488);
            this.t_GROUP_PRICESDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TGP_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "TGP_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TGP_GROUP";
            this.dataGridViewTextBoxColumn2.DataSource = this.tGROUPBindingSource;
            this.dataGridViewTextBoxColumn2.DisplayMember = "GR_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Группа";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn2.ValueMember = "GR_ID";
            // 
            // tGROUPBindingSource
            // 
            this.tGROUPBindingSource.DataMember = "T_GROUP";
            this.tGROUPBindingSource.DataSource = this.dBDataSet;
            this.tGROUPBindingSource.Filter = "";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "TGP_PRICE";
            this.dataGridViewTextBoxColumn3.HeaderText = "Цена";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // AllStudentPricesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 513);
            this.Controls.Add(this.t_GROUP_PRICESDataGridView);
            this.Controls.Add(this.t_GROUP_PRICESBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AllStudentPricesForm";
            this.Text = "Цены групп заочного обучения";
            this.Load += new System.EventHandler(this.StudentPricesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_PRICESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_PRICESBindingNavigator)).EndInit();
            this.t_GROUP_PRICESBindingNavigator.ResumeLayout(false);
            this.t_GROUP_PRICESBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_PRICESDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tGROUPBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource t_GROUP_PRICESBindingSource;
        private DBDataSetTableAdapters.T_GROUP_PRICESTableAdapter t_GROUP_PRICESTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator t_GROUP_PRICESBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton t_GROUP_PRICESBindingNavigatorSaveItem;
        private DBDataSetTableAdapters.T_GROUPTableAdapter t_GROUPTableAdapter;
        private System.Windows.Forms.DataGridView t_GROUP_PRICESDataGridView;
        private System.Windows.Forms.BindingSource tGROUPBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}