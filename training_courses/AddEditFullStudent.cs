﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class AddEditFullStudent : Form
    {
        private DBDataSetTableAdapters.T_PERSONTableAdapter personAdapter;
        private DBDataSetTableAdapters.T_STUDENTTableAdapter fullStudentAdapter;

        private long _groupID;
        private int _stID;
        private int _pID;
        private string _lastName;
        private string _firstName;
        private string _secondName;
        private int _course;
        private string _contract;
        private string _address;
        private string _info;
        private bool _editMode;

        public AddEditFullStudent (long groupID, int course)
        {
            InitializeComponent();

            this._groupID = groupID;
            this._course = course;
            numCourse.Value = this._course;

            personAdapter = new DBDataSetTableAdapters.T_PERSONTableAdapter();
            personAdapter.ClearBeforeFill = true;

            fullStudentAdapter = new DBDataSetTableAdapters.T_STUDENTTableAdapter();
            fullStudentAdapter.ClearBeforeFill = true;

            personAdapter.Fill(dBDataSet.T_PERSON);
            fullStudentAdapter.Fill(dBDataSet.T_STUDENT, (int)this._groupID);

            this.Validate();

            this.personAdapter.Update(this.dBDataSet);
            this.fullStudentAdapter.Update(this.dBDataSet);

            this.dBDataSet.T_PERSON.AcceptChanges();
            this.dBDataSet.T_STUDENT.AcceptChanges();

            this._editMode = false;
            this.Text = "Добавить запись";
        }

        public AddEditFullStudent (long groupID, int stID, int pID, string lastName,
           string firstName, string secondName, int course, string contract, string address, string info) : this(groupID, course)
        {
            this._stID = stID;
            this._pID = pID;  
            this._lastName = lastName;
            this._firstName = firstName;
            this._secondName = secondName;
            this._contract = contract;
            this._course = course;
            this._address = address;
            this._info = info;

            txtLastName.Text = this._lastName;
            txtFirstName.Text = this._firstName;
            txtSecondName.Text = this._secondName;
            txtContract.Text = this._contract;
            numCourse.Value = this._course;
            txtAddress.Text = this._address;
            txtInfo.Text = this._info;

            this._editMode = true;
            this.Text = "Редактировать запись";
        }

        private void btnCancel_Click (object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click (object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtLastName.Text) || String.IsNullOrEmpty(txtFirstName.Text)
                || String.IsNullOrEmpty(txtSecondName.Text))
            {
                MessageBox.Show("Введите корректное ФИО или заполните все поля!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ((int)numCourse.Value <= 0)
            {
                MessageBox.Show("Введите корректный курс!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtContract.Text))
            {
                MessageBox.Show("Введите номер контракта!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (String.IsNullOrEmpty(txtAddress.Text))
            {
                MessageBox.Show("Введите адрес!",
                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtLastName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Фамилия содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtFirstName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Имя содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Regex.IsMatch(txtSecondName.Text, @"^[a-zA-Z-а-яА-Я]+$"))
            {
                MessageBox.Show("Отчество содержит недопустимые символы!",
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!this._editMode)
            {
                DBDataSet.T_PERSONDataTable personTable = personAdapter.GetDataByName(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                long pID = 0;
                if (personTable.Rows.Count == 0)
                {
                    int res = personAdapter.Insert(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                    personTable = personAdapter.GetDataByName(txtLastName.Text, txtFirstName.Text, txtSecondName.Text);
                    DBDataSet.T_PERSONRow personRow = (DBDataSet.T_PERSONRow)personTable.Rows[0];
                    pID = personRow.P_ID;
                }
                else
                {
                    DBDataSet.T_PERSONRow personRow = (DBDataSet.T_PERSONRow)personTable.Rows[0];
                    pID = personRow.P_ID;
                }

                fullStudentAdapter.Insert(txtContract.Text, (int)numCourse.Value, null, (int)_groupID, (int)pID,
                    txtInfo.Text, txtAddress.Text);
            }
            else
            {
                personAdapter.UpdateQueryByID(txtLastName.Text, txtFirstName.Text, txtSecondName.Text, this._pID);

                fullStudentAdapter.UpdateQueryByID(txtContract.Text, (int)numCourse.Value, null, (int)_groupID, (int)this._pID,
                    txtInfo.Text, txtAddress.Text, this._stID);
            }
            this.Close();
        }
    }
}
