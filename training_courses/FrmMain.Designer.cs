﻿namespace training_courses
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullStudentsMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullGroupsAndStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listenerMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showListenersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mastersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mastersGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sVEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sveGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listenerGroupPricesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentGroupPricesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullStudentGroupPricesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.magisterGroupPricesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sVEGroupPricesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.queuePaymentStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enrollmentQueueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eQPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enrollmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.submissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.submissionStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsConnection = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsRole = new System.Windows.Forms.ToolStripStatusLabel();
            this.enrollmentViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.studentMenuToolStripMenuItem,
            this.fullStudentsMenuToolStripMenuItem,
            this.listenerMenuToolStripMenuItem,
            this.mastersToolStripMenuItem,
            this.sVEToolStripMenuItem,
            this.accountingToolStripMenuItem,
            this.enrollmentToolStripMenuItem,
            this.controlToolStripMenuItem,
            this.exitMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1264, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Image = global::training_courses.Properties.Resources.properties;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.fileToolStripMenuItem.Text = "Система";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::training_courses.Properties.Resources.quit;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // studentMenuToolStripMenuItem
            // 
            this.studentMenuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showStudentsToolStripMenuItem});
            this.studentMenuToolStripMenuItem.Name = "studentMenuToolStripMenuItem";
            this.studentMenuToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.studentMenuToolStripMenuItem.Text = "Заочное обучение";
            // 
            // showStudentsToolStripMenuItem
            // 
            this.showStudentsToolStripMenuItem.Name = "showStudentsToolStripMenuItem";
            this.showStudentsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.showStudentsToolStripMenuItem.Text = "Группы и студенты";
            this.showStudentsToolStripMenuItem.Click += new System.EventHandler(this.showStudentsToolStripMenuItem_Click);
            // 
            // fullStudentsMenuToolStripMenuItem
            // 
            this.fullStudentsMenuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fullGroupsAndStudentsToolStripMenuItem});
            this.fullStudentsMenuToolStripMenuItem.Name = "fullStudentsMenuToolStripMenuItem";
            this.fullStudentsMenuToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.fullStudentsMenuToolStripMenuItem.Text = "Очное обучение";
            // 
            // fullGroupsAndStudentsToolStripMenuItem
            // 
            this.fullGroupsAndStudentsToolStripMenuItem.Name = "fullGroupsAndStudentsToolStripMenuItem";
            this.fullGroupsAndStudentsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.fullGroupsAndStudentsToolStripMenuItem.Text = "Группы и студенты";
            this.fullGroupsAndStudentsToolStripMenuItem.Click += new System.EventHandler(this.fullGroupsAndStudentsToolStripMenuItem_Click);
            // 
            // listenerMenuToolStripMenuItem
            // 
            this.listenerMenuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showListenersToolStripMenuItem,
            this.subjectsToolStripMenuItem});
            this.listenerMenuToolStripMenuItem.Name = "listenerMenuToolStripMenuItem";
            this.listenerMenuToolStripMenuItem.Size = new System.Drawing.Size(161, 20);
            this.listenerMenuToolStripMenuItem.Text = "Подготовительные курсы";
            // 
            // showListenersToolStripMenuItem
            // 
            this.showListenersToolStripMenuItem.Name = "showListenersToolStripMenuItem";
            this.showListenersToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.showListenersToolStripMenuItem.Text = "Группы";
            this.showListenersToolStripMenuItem.Click += new System.EventHandler(this.showListenersToolStripMenuItem_Click);
            // 
            // subjectsToolStripMenuItem
            // 
            this.subjectsToolStripMenuItem.Name = "subjectsToolStripMenuItem";
            this.subjectsToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.subjectsToolStripMenuItem.Text = "Предметы";
            this.subjectsToolStripMenuItem.Click += new System.EventHandler(this.subjectsToolStripMenuItem_Click);
            // 
            // mastersToolStripMenuItem
            // 
            this.mastersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mastersGroupsToolStripMenuItem});
            this.mastersToolStripMenuItem.Name = "mastersToolStripMenuItem";
            this.mastersToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.mastersToolStripMenuItem.Text = "Магистры";
            // 
            // mastersGroupsToolStripMenuItem
            // 
            this.mastersGroupsToolStripMenuItem.Name = "mastersGroupsToolStripMenuItem";
            this.mastersGroupsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.mastersGroupsToolStripMenuItem.Text = "Группы";
            this.mastersGroupsToolStripMenuItem.Click += new System.EventHandler(this.mastersGroupsToolStripMenuItem_Click);
            // 
            // sVEToolStripMenuItem
            // 
            this.sVEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sveGroupsToolStripMenuItem});
            this.sVEToolStripMenuItem.Name = "sVEToolStripMenuItem";
            this.sVEToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.sVEToolStripMenuItem.Text = "СПО";
            // 
            // sveGroupsToolStripMenuItem
            // 
            this.sveGroupsToolStripMenuItem.Name = "sveGroupsToolStripMenuItem";
            this.sveGroupsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.sveGroupsToolStripMenuItem.Text = "Группы";
            this.sveGroupsToolStripMenuItem.Click += new System.EventHandler(this.sveGroupsToolStripMenuItem_Click);
            // 
            // accountingToolStripMenuItem
            // 
            this.accountingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listenerGroupPricesToolStripMenuItem,
            this.studentGroupPricesToolStripMenuItem,
            this.fullStudentGroupPricesToolStripMenuItem,
            this.magisterGroupPricesToolStripMenuItem,
            this.sVEGroupPricesToolStripMenuItem,
            this.queuePaymentStatusToolStripMenuItem,
            this.enrollmentQueueToolStripMenuItem});
            this.accountingToolStripMenuItem.Name = "accountingToolStripMenuItem";
            this.accountingToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.accountingToolStripMenuItem.Text = "Бухгалтерия";
            // 
            // listenerGroupPricesToolStripMenuItem
            // 
            this.listenerGroupPricesToolStripMenuItem.Name = "listenerGroupPricesToolStripMenuItem";
            this.listenerGroupPricesToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.listenerGroupPricesToolStripMenuItem.Text = "Учет подготовительных групп";
            this.listenerGroupPricesToolStripMenuItem.Click += new System.EventHandler(this.listenerGroupPricesToolStripMenuItem_Click);
            // 
            // studentGroupPricesToolStripMenuItem
            // 
            this.studentGroupPricesToolStripMenuItem.Name = "studentGroupPricesToolStripMenuItem";
            this.studentGroupPricesToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.studentGroupPricesToolStripMenuItem.Text = "Учет заочного обучения";
            this.studentGroupPricesToolStripMenuItem.Click += new System.EventHandler(this.studentGroupPricesToolStripMenuItem_Click);
            // 
            // fullStudentGroupPricesToolStripMenuItem
            // 
            this.fullStudentGroupPricesToolStripMenuItem.Name = "fullStudentGroupPricesToolStripMenuItem";
            this.fullStudentGroupPricesToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.fullStudentGroupPricesToolStripMenuItem.Text = "Учет очного обучения";
            this.fullStudentGroupPricesToolStripMenuItem.Click += new System.EventHandler(this.fullStudentGroupPricesToolStripMenuItem_Click);
            // 
            // magisterGroupPricesToolStripMenuItem
            // 
            this.magisterGroupPricesToolStripMenuItem.Name = "magisterGroupPricesToolStripMenuItem";
            this.magisterGroupPricesToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.magisterGroupPricesToolStripMenuItem.Text = "Учет магистров";
            this.magisterGroupPricesToolStripMenuItem.Click += new System.EventHandler(this.magisterGroupPricesToolStripMenuItem_Click);
            // 
            // sVEGroupPricesToolStripMenuItem
            // 
            this.sVEGroupPricesToolStripMenuItem.Name = "sVEGroupPricesToolStripMenuItem";
            this.sVEGroupPricesToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.sVEGroupPricesToolStripMenuItem.Text = "Учет СПО";
            this.sVEGroupPricesToolStripMenuItem.Click += new System.EventHandler(this.sVEGroupPricesToolStripMenuItem_Click);
            // 
            // queuePaymentStatusToolStripMenuItem
            // 
            this.queuePaymentStatusToolStripMenuItem.Name = "queuePaymentStatusToolStripMenuItem";
            this.queuePaymentStatusToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.queuePaymentStatusToolStripMenuItem.Text = "Статусы оплаты";
            this.queuePaymentStatusToolStripMenuItem.Click += new System.EventHandler(this.queuePaymentStatusToolStripMenuItem_Click);
            // 
            // enrollmentQueueToolStripMenuItem
            // 
            this.enrollmentQueueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eQPToolStripMenuItem});
            this.enrollmentQueueToolStripMenuItem.Name = "enrollmentQueueToolStripMenuItem";
            this.enrollmentQueueToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.enrollmentQueueToolStripMenuItem.Text = "Поступающие";
            // 
            // eQPToolStripMenuItem
            // 
            this.eQPToolStripMenuItem.Name = "eQPToolStripMenuItem";
            this.eQPToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.eQPToolStripMenuItem.Text = "Очередь оплаты и смена статусов";
            this.eQPToolStripMenuItem.Click += new System.EventHandler(this.eQPToolStripMenuItem_Click);
            // 
            // enrollmentToolStripMenuItem
            // 
            this.enrollmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.submissionToolStripMenuItem,
            this.submissionStatusToolStripMenuItem,
            this.enrollmentViewToolStripMenuItem});
            this.enrollmentToolStripMenuItem.Name = "enrollmentToolStripMenuItem";
            this.enrollmentToolStripMenuItem.Size = new System.Drawing.Size(142, 20);
            this.enrollmentToolStripMenuItem.Text = "Зачисление студентов";
            // 
            // submissionToolStripMenuItem
            // 
            this.submissionToolStripMenuItem.Name = "submissionToolStripMenuItem";
            this.submissionToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.submissionToolStripMenuItem.Text = "Подача документов";
            this.submissionToolStripMenuItem.Click += new System.EventHandler(this.submissionToolStripMenuItem_Click);
            // 
            // submissionStatusToolStripMenuItem
            // 
            this.submissionStatusToolStripMenuItem.Name = "submissionStatusToolStripMenuItem";
            this.submissionStatusToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.submissionStatusToolStripMenuItem.Text = "Статусы подачи";
            this.submissionStatusToolStripMenuItem.Click += new System.EventHandler(this.submissionStatusToolStripMenuItem_Click);
            // 
            // controlToolStripMenuItem
            // 
            this.controlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userManagementToolStripMenuItem});
            this.controlToolStripMenuItem.Name = "controlToolStripMenuItem";
            this.controlToolStripMenuItem.Size = new System.Drawing.Size(134, 20);
            this.controlToolStripMenuItem.Text = "Администрирование";
            // 
            // userManagementToolStripMenuItem
            // 
            this.userManagementToolStripMenuItem.Name = "userManagementToolStripMenuItem";
            this.userManagementToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.userManagementToolStripMenuItem.Text = "Управление пользователями";
            this.userManagementToolStripMenuItem.Click += new System.EventHandler(this.userManagementToolStripMenuItem_Click);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.exitMenuItem.Image = global::training_courses.Properties.Resources.quit;
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(124, 20);
            this.exitMenuItem.Text = "Звершить сеанс";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsConnection,
            this.tsUser,
            this.tsRole});
            this.statusStrip1.Location = new System.Drawing.Point(0, 659);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1264, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsConnection
            // 
            this.tsConnection.Image = global::training_courses.Properties.Resources.db;
            this.tsConnection.Name = "tsConnection";
            this.tsConnection.Size = new System.Drawing.Size(104, 17);
            this.tsConnection.Text = "Подключение:";
            // 
            // tsUser
            // 
            this.tsUser.Image = global::training_courses.Properties.Resources.student;
            this.tsUser.Name = "tsUser";
            this.tsUser.Size = new System.Drawing.Size(103, 17);
            this.tsUser.Text = "Пользователь:";
            // 
            // tsRole
            // 
            this.tsRole.Image = global::training_courses.Properties.Resources.students;
            this.tsRole.Name = "tsRole";
            this.tsRole.Size = new System.Drawing.Size(53, 17);
            this.tsRole.Text = "Роль:";
            // 
            // enrollmentViewToolStripMenuItem
            // 
            this.enrollmentViewToolStripMenuItem.Name = "enrollmentViewToolStripMenuItem";
            this.enrollmentViewToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.enrollmentViewToolStripMenuItem.Text = "Распределение по группам";
            this.enrollmentViewToolStripMenuItem.Click += new System.EventHandler(this.enrollmentViewToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Учет подготовительных курсов и заочного обучения";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem studentMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listenerMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showListenersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tsConnection;
        private System.Windows.Forms.ToolStripMenuItem controlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fullStudentsMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fullGroupsAndStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mastersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mastersGroupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sVEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sveGroupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tsUser;
        private System.Windows.Forms.ToolStripStatusLabel tsRole;
        private System.Windows.Forms.ToolStripMenuItem accountingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listenerGroupPricesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentGroupPricesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fullStudentGroupPricesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem magisterGroupPricesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sVEGroupPricesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enrollmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submissionStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem queuePaymentStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enrollmentQueueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eQPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enrollmentViewToolStripMenuItem;
    }
}

