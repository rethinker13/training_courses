﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class PasswordConfirmForm : Form
    {
        public PasswordConfirmForm()
        {
            InitializeComponent();
        }

        private void tsbtnOK_Click(object sender, EventArgs e)
        {
            if (Encoding.UTF8.GetString(DataProtection.Unprotect(Global.user.Password)) == txtPassword.Text)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Введен неверный пароль!",
                   "Ошибка подтверждения пароля", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void tsbtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
