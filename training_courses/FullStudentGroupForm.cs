﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class FullStudentGroupForm : Form
    {
        public FullStudentGroupForm()
        {
            InitializeComponent();

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                tbExcel.Visible = false;
            }            
        }

        private void t_F_GROUPBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {

                this.Validate();
                this.t_F_GROUPBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dBDataSet);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    Application.ProductName, ex.Message);
                message.ShowDialog();
            }

            FullStudentGroupForm_Load(sender, e);
        }

        private void FullStudentGroupForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_F_GROUP' table. You can move, or remove it, as needed.
            this.t_F_GROUPTableAdapter.FillByFullGroups(this.dBDataSet.T_GROUP);
            if (AccessLevel.level == AccessLevel.Level.Students)
            {
                tbExcel.Visible = false;               
            }
            if (AccessLevel.level == AccessLevel.Level.Manager)
            {
                t_F_GROUPDataGridView.ReadOnly = true;
            }
        }

        private void tsEditGroup_Click(object sender, EventArgs e)
        {
            if (t_F_GROUPDataGridView.SelectedCells.Count != 0)
            {
                if (t_F_GROUPDataGridView.SelectedCells[0].RowIndex != -1)
                {
                    int idx = t_F_GROUPDataGridView.SelectedCells[0].RowIndex;
                    if (t_F_GROUPDataGridView.Rows[idx].Cells[0].Value != null && t_F_GROUPDataGridView.Rows[idx].Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        long groupID = (long)t_F_GROUPDataGridView.Rows[idx].Cells[0].Value;
                        string groupName = t_F_GROUPDataGridView.Rows[idx].Cells[1].Value.ToString();
                        FullStudentForm studentForm = new FullStudentForm(groupID, groupName);
                        foreach (Form form in ((FrmMain)this.ParentForm).MdiChildren)
                        {
                            if (form.GetType() == typeof(FullStudentForm))
                                studentForm = (FullStudentForm)form;
                        }
                        studentForm.MdiParent = (FrmMain)this.ParentForm;
                        studentForm.Show();
                    }
                }
            }
        }

        private void tbExcel_Click(object sender, EventArgs e)
        {
            if (t_F_GROUPDataGridView.CurrentRow != null)
            {
                if (t_F_GROUPDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (t_F_GROUPDataGridView.CurrentRow.Cells[0].Value != null && t_F_GROUPDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        long groupID = (long)(int)t_F_GROUPDataGridView.CurrentRow.Cells[0].Value;
                        if (t_F_GROUPDataGridView.CurrentRow.Cells[2].Value == null || t_F_GROUPDataGridView.CurrentRow.Cells[2].Value.ToString() == "")
                            return;
                        byte[] xlsBin = (byte[])t_F_GROUPDataGridView.CurrentRow.Cells[2].Value;

                        string group_name = t_F_GROUPDataGridView.CurrentRow.Cells[1].Value.ToString();
                        if (!Directory.Exists(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Очные группы\\"))
                            Directory.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Очные группы\\");
                        if (!Directory.Exists(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Очные группы\\" +
                            group_name + "\\"))
                            Directory.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Очные группы\\" +
                            group_name + "\\");

                        string tmpFile = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Очные группы\\" +
                             group_name + "\\Студенты " + group_name + ".xls";
                        File.WriteAllBytes(tmpFile, xlsBin);

                        Process.Start(tmpFile);

                    }
                }
            }
        }

        private void t_F_GROUPDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (t_F_GROUPDataGridView.CurrentRow != null)
            {
                t_F_GROUPDataGridView.CurrentRow.Cells[3].Value = 0;
                t_F_GROUPDataGridView.CurrentRow.Cells[4].Value = 0;
                t_F_GROUPDataGridView.CurrentRow.Cells[5].Value = 0;
            }
        }
    }
}
