﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace training_courses
{
    /// <summary>
    /// Глобальный класс общего назначения
    /// </summary>
    class Global
    {
        /// <summary>
        /// Файл с настройками подключения к БД
        /// </summary>
        public static string CONNECTION_SETTINGS_FILE = "system.dat";

        /// <summary>
        /// Текущий пользователь системы
        /// </summary>
        public static User user = null;

        /// <summary>
        /// Ссылка на главную форму системы
        /// </summary>
        public static FrmMain MainWindow = null;

        /// <summary>
        /// Структура строки подключения к БД
        /// </summary>
        public struct ConnectionString
        {
            public string ServerAddress;
            public string ServerLogin;
            public string ServerPassword;
            public string DataBaseName;
            public string ServerPort;
            public bool Connected;
            /// <summary>
            /// Установка параметров строки подключения к БД
            /// </summary>
            /// <param name="__server_name_">Имя (адрес) сервера</param>
            /// <param name="__login_">Логин СУБД</param>
            /// <param name="__password_">Пароль СУБД</param>
            /// <param name="__db_">Имя БД</param>
            /// <param name="__port_">Порт</param>
            public void Set(string __server_name_, string __login_, string __password_,
                string __db_, string __port_)
            {
                ServerAddress = __server_name_;
                ServerLogin = __login_;
                ServerPassword = __password_;
                DataBaseName = __db_;
                ServerPort = __port_;
            }
            /// <summary>
            /// Сброс флага подключения к БД
            /// </summary>
            public void ResetConnection() { Connected = false; }
            /// <summary>
            /// Установка флага подключения к БД
            /// </summary>
            public void Successfully() { Connected = true; }
        }

        /// <summary>
        /// Параметры строки подключения к БД
        /// </summary>
        public static ConnectionString DB_CS;

        /// <summary>
        /// Получение настроек подключения из файла
        /// </summary>
        /// <param name="__filename_">Файл с настройками</param>
        /// <returns>Структура с параметрами подключения к БД</returns>
        public static ConnectionString LoadConnectionSettings(string __filename_)
        {
            // Новая строка подключения
            ConnectionString NewCS = new ConnectionString();
            // Попытка прочитать данные
            try
            {
                //fs = new FileStream(__filename_, FileMode.Open);
                //sr = new StreamReader(fs);
                byte[] bytes = File.ReadAllBytes(__filename_);
                string strings = Encoding.UTF8.GetString(DataProtection.Unprotect(bytes));
                StringReader str = new StringReader(strings);
                
                // Чтение данных
                NewCS.ServerAddress = str.ReadLine().Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries)[1];
                NewCS.ServerLogin = str.ReadLine().Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries)[1];
                NewCS.ServerPassword = str.ReadLine().Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries)[1];
                NewCS.DataBaseName = str.ReadLine().Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries)[1];
                NewCS.ServerPort = str.ReadLine().Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries)[1];
                // Закрыть файл
                str.Close();
                // Возврат значения
                return NewCS;
            }
            catch (Exception)
            {
                NewCS.ServerAddress = "localhost";
                NewCS.DataBaseName = "C:\\db\\training.fdb";
                NewCS.ServerPort = "3050";
                return NewCS;
            }
        }

        /// <summary>
        /// Сохранение настроек подключения в файл
        /// </summary>
        public static void SaveConnectionSettings(string __filename_)
        {
            // Поток записи данных в файл
            StringWriter str = null;
            // Попытка записать данные
            try
            {
                str = new StringWriter();
                str.WriteLine("address=" + DB_CS.ServerAddress);
                str.WriteLine("login=" + DB_CS.ServerLogin);
                str.WriteLine("password=" + DB_CS.ServerPassword);
                str.WriteLine("db=" + DB_CS.DataBaseName);
                str.WriteLine("port=" + DB_CS.ServerPort);
                byte[] bytes = DataProtection.Protect(Encoding.UTF8.GetBytes(str.ToString()));
                //fs = new FileStream(__filename_, FileMode.Create);
                //sw = new StreamWriter(fs);
                // Запись данных
                //sw.Write(bytes);
                File.WriteAllBytes(__filename_, bytes);
                // Закрыть файл
                str.Close();
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Попытка установить подключение к БД
        /// </summary>        
        /// <returns>Результат подключения</returns>
        public static bool SetConnectionWithDB()
        {
            // Попытка подключиться
            try
            {
                // Настройка подключения
                string connString = "server=" + DB_CS.ServerAddress +
                                    ";uid=" + DB_CS.ServerLogin +
                                    ";database=" + DB_CS.DataBaseName +
                                    ";charset=WIN1251" +
                                    ";port=" + DB_CS.ServerPort +
                                    ";password=" + DB_CS.ServerPassword + ";";
                Properties.Settings.Default["ConnectionString"] = connString;
                // Попытка прочитать данные
                DBDataSetTableAdapters.T_USERTableAdapter test = new DBDataSetTableAdapters.T_USERTableAdapter();
                DBDataSet.T_USERDataTable table = test.GetData();
                // Если данные прочитаны, значит попытка успешна
                DB_CS.Successfully();
                //MainWindow.CurrentDBName = DB_CS.DataBaseName;
                table.Dispose();
                test.Dispose();
                return DB_CS.Connected;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неправильно указаны параметры подключения к БД!" +
                    Environment.NewLine + ex.Message, Application.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Показать форму настройки подключения к БД
        /// </summary>
        public static void ShowConnectionSettings()
        {
            // Предварительный сброс флага подключения к БД
            DB_CS.ResetConnection();
            //MainWindow.CurrentDBName = "unknown";
            // Очистить данные авторизованного пользователя на форме
            //MainWindow.CurrentUserName = "Неизвестный пользователь";
            // Сброс данных пользователя
            user = null;
            // Создание формы подключения к БД
            dialogDatabaseConnection ddbcf = new dialogDatabaseConnection();
            // Показать форму подключения к БД
            ddbcf.ShowDialog();
        }

        /// <summary>
        /// Показать форму авторизации пользователя
        /// </summary>
        public static void ShowLoginForm()
        {
            if (DB_CS.Connected)
            {
                // Очистить данные авторизованного пользователя на форме
                MainWindow.CurrentUserName = "Неизвестный пользователь";
                // Сброс данных пользователя
                user = null;
                // Создание формы авторизации пользователя
                LoginForm daf = new LoginForm();
                // Показать форму авторизации пользователя
                daf.ShowDialog();
            }
            else
            {
                // Сообщение о необходимости подключения к БД
                MessageBox.Show("Сначала необходимо подключиться к базе данных!",
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                MainWindow.CurrentDBName = "unknown";
                // Показать форму настройки подключения к БД
                ShowConnectionSettings();
            }
        }

        public static bool UserCheck()
        {
            PasswordConfirmForm pcf = new PasswordConfirmForm();
            if (pcf.ShowDialog() == DialogResult.OK)
                return true;
            else
                return false;
            return true;
        }
    }
}