﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace training_courses
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main ()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //LoginForm loginForm = new LoginForm();
            //DialogResult res = loginForm.ShowDialog();
            //if (res != DialogResult.OK)
            //{
            //    return;
            //}
            Application.Run(new LoginForm());
        }
    }
}
