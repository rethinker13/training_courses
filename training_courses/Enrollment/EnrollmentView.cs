﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses.Enrollment
{
    public partial class EnrollmentView : Form
    {
        private DBDataSetTableAdapters.ENROLLMENT_QUEUETableAdapter eAdapter;
        private DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter docSubAdapter;
        private DBDataSetTableAdapters.PAYMENT_QUEUETableAdapter paymentAdapter;
        private DBDataSetTableAdapters.T_PERSONTableAdapter personAdapter;

        public EnrollmentView()
        {
            InitializeComponent();

            eAdapter = new DBDataSetTableAdapters.ENROLLMENT_QUEUETableAdapter();
            eAdapter.ClearBeforeFill = true;

            docSubAdapter = new DBDataSetTableAdapters.DOC_SUBMISSIONTableAdapter();
            docSubAdapter.ClearBeforeFill = true;

            paymentAdapter = new DBDataSetTableAdapters.PAYMENT_QUEUETableAdapter();
            paymentAdapter.ClearBeforeFill = true;

            personAdapter = new DBDataSetTableAdapters.T_PERSONTableAdapter();
            personAdapter.ClearBeforeFill = true;
        }

        private void EnrollmentView_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.ENROLLMENT_QUEUE_VIEW' table. You can move, or remove it, as needed.
            this.eNROLLMENT_QUEUE_VIEWTableAdapter.Fill(this.dBDataSet.ENROLLMENT_QUEUE_VIEW);

        }

        private void tsbMove_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                string l_name = Convert.ToString(dgvData[2, dgvData.SelectedRows[0].Index].Value);
                string f_name = Convert.ToString(dgvData[3, dgvData.SelectedRows[0].Index].Value);
                string s_name = Convert.ToString(dgvData[4, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);
                int ds_id = Convert.ToInt32(dgvData[6, dgvData.SelectedRows[0].Index].Value);
                DateTime datetime = Convert.ToDateTime(dgvData[7, dgvData.SelectedRows[0].Index].Value);
                int course = Convert.ToInt32(dgvData[8, dgvData.SelectedRows[0].Index].Value);
                string number = Convert.ToString(dgvData[9, dgvData.SelectedRows[0].Index].Value);
                string address = Convert.ToString(dgvData[10, dgvData.SelectedRows[0].Index].Value);
                string info = Convert.ToString(dgvData[11, dgvData.SelectedRows[0].Index].Value);

                EnrollMoveForm emf = new EnrollMoveForm(current_id, l_name, f_name, s_name,
                    p_id, ds_id, datetime, course, number, address, info);
                if (emf.ShowDialog() == DialogResult.OK)
                {
                    docSubAdapter.UpdateQueryStatus(4, ds_id);
                    eAdapter.Delete(current_id);
                    EnrollmentView_Load(sender, e);
                }
            }

        }

        private void tsbDenied_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                string l_name = Convert.ToString(dgvData[2, dgvData.SelectedRows[0].Index].Value);
                string f_name = Convert.ToString(dgvData[3, dgvData.SelectedRows[0].Index].Value);
                string s_name = Convert.ToString(dgvData[4, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);
                int ds_id = Convert.ToInt32(dgvData[6, dgvData.SelectedRows[0].Index].Value);
                DateTime datetime = Convert.ToDateTime(dgvData[7, dgvData.SelectedRows[0].Index].Value);
                int course = Convert.ToInt32(dgvData[8, dgvData.SelectedRows[0].Index].Value);
                string number = Convert.ToString(dgvData[9, dgvData.SelectedRows[0].Index].Value);
                string address = Convert.ToString(dgvData[10, dgvData.SelectedRows[0].Index].Value);
                string info = Convert.ToString(dgvData[11, dgvData.SelectedRows[0].Index].Value);

                docSubAdapter.UpdateQueryStatus(5, ds_id);
                EnrollmentView_Load(sender, e);
            }
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                string l_name = Convert.ToString(dgvData[2, dgvData.SelectedRows[0].Index].Value);
                string f_name = Convert.ToString(dgvData[3, dgvData.SelectedRows[0].Index].Value);
                string s_name = Convert.ToString(dgvData[4, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);
                int ds_id = Convert.ToInt32(dgvData[6, dgvData.SelectedRows[0].Index].Value);
                DateTime datetime = Convert.ToDateTime(dgvData[7, dgvData.SelectedRows[0].Index].Value);
                int course = Convert.ToInt32(dgvData[8, dgvData.SelectedRows[0].Index].Value);
                string number = Convert.ToString(dgvData[9, dgvData.SelectedRows[0].Index].Value);
                string address = Convert.ToString(dgvData[10, dgvData.SelectedRows[0].Index].Value);
                string info = Convert.ToString(dgvData[11, dgvData.SelectedRows[0].Index].Value);

                eAdapter.Delete(current_id);
                docSubAdapter.UpdateQueryStatus(5, ds_id);

                EnrollmentView_Load(sender, e);
            }
        }

        private void tsbtnDeleteSelected_Click(object sender, EventArgs e)
        {
            int current_id = 0;
            if (dgvData.SelectedRows.Count > 0)
            {
                current_id = Convert.ToInt32(dgvData[1, dgvData.SelectedRows[0].Index].Value);
                int p_id = Convert.ToInt32(dgvData[5, dgvData.SelectedRows[0].Index].Value);

                personAdapter.Delete(p_id);
                EnrollmentView_Load(sender, e);
            }
        }
    }
}
