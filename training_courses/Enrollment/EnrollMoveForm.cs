﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses.Enrollment
{
    public partial class EnrollMoveForm : Form
    {
        private DBDataSetTableAdapters.T_STUDENTTableAdapter sAdapter;

        private string address;
        private int course;
        private int current_id;
        private DateTime datetime;
        private int ds_id;
        private string f_name;
        private string info;
        private string l_name;
        private string number;
        private int p_id;
        private string s_name;

        public EnrollMoveForm()
        {
            InitializeComponent();
        }

        public EnrollMoveForm(int current_id, string l_name, string f_name, string s_name, int p_id, int ds_id, 
            DateTime datetime, int course, string number, string address, string info) : this()
        {
            this.current_id = current_id;
            this.l_name = l_name;
            this.f_name = f_name;
            this.s_name = s_name;
            this.p_id = p_id;
            this.ds_id = ds_id;
            this.datetime = datetime;
            this.course = course;
            this.number = number;
            this.address = address;
            this.info = info;

            sAdapter = new DBDataSetTableAdapters.T_STUDENTTableAdapter();
            sAdapter.ClearBeforeFill = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Global.UserCheck())
                {
                    MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                       "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                object o = cmbGroup.SelectedValue;
                if (o == null)
                    return;
                int group_id = (int)(long)o;
                if (group_id != -1)
                {
                    sAdapter.Insert(this.number, this.course, null, group_id, this.p_id, this.info, this.address);
                    MessageBox.Show("Выполнение оперцаии завершено успешно! ",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Выполнение оперцаии невозможно! " + ex.Message,
                  Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void EnrollMoveForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_GROUP' table. You can move, or remove it, as needed.
            this.t_GROUPTableAdapter.Fill(this.dBDataSet.T_GROUP);

        }
    }
}
