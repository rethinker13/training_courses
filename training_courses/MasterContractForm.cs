﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class MasterContractForm : Form
    {
        public string _customer { get; private set; }
        public string _student { get; private set; }
        public DateTime _date { get; private set; }
        public int _course { get; private set; }
        public string _contr_number { get; private set; }
        public string _cust_face { get; private set; }
        public string _basis { get; private set; }
        public string _speciality_spec { get; private set; }
        public string _variant { get; private set; }
        public string _diplom { get; private set; }
        public string _tr_period { get; private set; }
        public string _tr_period_a { get; private set; }
        public string _role { get; private set; }
        public string _sp { get; private set; }
        public string _np { get; private set; }
        public string _pinfo { get; private set; }
        public string _address { get; private set; }
        public string _f_address { get; private set; }
        public string _phone { get; private set; }
        public string _bdate { get; private set; }
        public string _head { get; private set; }
        public string _addinfo { get; private set; }
        public string _semestr { get; private set; }
        public string _year { get; private set; }
        public string _year1 { get; private set; }
        public string _allsum { get; private set; }
        public string _subsum { get; private set; }
        public string _semsum { get; private set; }

        public MasterContractForm(string contr_number, string cust_face, string basis,
            string speciality_spec, string variant,
            string tr_period, string tr_period_a, string diplom,
            string role,
            string sp,
            string np,
            string pinfo,
            string address,
            string f_address,
            string phone,
            string bdate,
            string head,
            string addinfo,
            string semestr,
            string year,
            string year1,
            string allsum,
            string subsum,
            string semsum,
            string customer, string student, DateTime date, int course)
        {
            InitializeComponent();
            this._contr_number = contr_number;
            this._cust_face = cust_face;
            this._basis = basis;
            this._speciality_spec = speciality_spec;
            this._variant = variant;
            this._tr_period = tr_period;
            this._tr_period_a = tr_period_a;
            this._diplom = diplom;
            this._role = role;
            this._sp = sp;
            this._np = np;
            this._pinfo = pinfo;
            this._address = address;
            this._f_address = f_address;
            this._phone = phone;
            this._bdate = bdate;
            this._head = head;
            this._addinfo = addinfo;
            this._semestr = semestr;
            this._year = year;
            this._year1 = year1;
            this._allsum = allsum;
            this._subsum = subsum;
            this._semsum = semsum;
            this._customer = customer;
            this._student = student;
            this._date = date;
            this._course = course;            

            reportViewer1.LocalReport.SetParameters(SetParameter());
            reportViewer1.RefreshReport();
        }

        private void MasterContractForm_Load(object sender, EventArgs e)
        {
            
        }

        private IEnumerable<ReportParameter> SetParameter()
        {
            return new List<ReportParameter>
            {
                new ReportParameter("ContrNumber", _contr_number, false),
                new ReportParameter("CustFace", _cust_face, false),
                new ReportParameter("Customer", _customer, false),
                new ReportParameter("Basis", _basis, false),
                new ReportParameter("Speciality_spec", _speciality_spec, false),
                new ReportParameter("Variant", _variant, false),
                new ReportParameter("Tr_Period", _tr_period, false),
                new ReportParameter("Tr_Period_A", _tr_period_a, false),
                new ReportParameter("Diplom", _diplom, false),
                new ReportParameter("Role", _role, false),
                new ReportParameter("SP", _sp, false),
                new ReportParameter("NP", _np, false),
                new ReportParameter("PInfo", _pinfo, false),
                new ReportParameter("Address", _address, false),
                new ReportParameter("F_Address", _f_address, false),
                new ReportParameter("Phone", _phone, false),
                new ReportParameter("BDate", _bdate, false),
                new ReportParameter("Head", _head, false),
                new ReportParameter("AddInfo", _addinfo, false),
                new ReportParameter("Semestr", _semestr, false),
                new ReportParameter("Year", _year, false),
                new ReportParameter("Year1", _year1, false),
                new ReportParameter("AllSum", _allsum, false),
                new ReportParameter("SubSum", _subsum, false),
                new ReportParameter("SemSum", _semsum, false),
                new ReportParameter("Student", _student, false),
                new ReportParameter("Date", _date.ToShortDateString(), false),
                new ReportParameter("Course", _course.ToString(), false)
            };
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            reportViewer1.LocalReport.SetParameters(SetParameter());
            reportViewer1.RefreshReport();
        }
    }
}
