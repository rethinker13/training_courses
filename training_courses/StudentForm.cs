﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class StudentForm : Form
    {
        private long _groupID;
        private string _groupName;

        private DBDataSetTableAdapters.T_GROUPTableAdapter groupAdapter;
        private DBDataSetTableAdapters.T_STUDENTTableAdapter studentsAdapter;

        public StudentForm (long groupID, string groupName)
        {
            InitializeComponent();

            this._groupID = groupID;
            this._groupName = groupName;

            groupAdapter = new DBDataSetTableAdapters.T_GROUPTableAdapter();
            groupAdapter.ClearBeforeFill = true;

            studentsAdapter = new DBDataSetTableAdapters.T_STUDENTTableAdapter();
            studentsAdapter.ClearBeforeFill = true;

            groupAdapter.Fill(dBDataSet.T_GROUP);
            studentsAdapter.Fill(dBDataSet.T_STUDENT, (int)_groupID);

            this.Validate();

            this.groupAdapter.Update(this.dBDataSet);
            this.studentsAdapter.Update(this.dBDataSet);

            this.dBDataSet.T_GROUP.AcceptChanges();
            this.dBDataSet.T_STUDENT.AcceptChanges();

            this.Text = "Группа заочного обучения: " + groupName;
        }

        private void StudentForm_Load (object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.V_STUDENTS' table. You can move, or remove it, as needed.
            this.v_STUDENTSTableAdapter.FillByGRID(this.dBDataSet.V_STUDENTS, (int)_groupID);

            if (AccessLevel.level == AccessLevel.Level.Students)
            {
                tbExcelExport.Visible = tbReceipt.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                tbReceipt.Visible = true;
                tbAddStudent.Visible = tbEditStudent.Visible = tbDeleteStudent.Visible = false;
            
                tbExcelExport.Visible = false;
                numCourse.Enabled = false;
                btnUpdateCourse.Enabled = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Manager)
            {
                tbReceipt.Visible = false;
                tbAddStudent.Visible = tbEditStudent.Visible = tbDeleteStudent.Visible = false;
               
                tbExcelExport.Visible = true;
                numCourse.Enabled = false;
                btnUpdateCourse.Enabled = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Engineer)
            {
                tbReceipt.Visible = false;
                tbAddStudent.Visible = tbEditStudent.Visible = tbDeleteStudent.Visible = true;
               
                tbExcelExport.Visible = true;
                numCourse.Enabled = true;
                btnUpdateCourse.Enabled = true;
            }

            DBDataSet.T_STUDENTDataTable table = studentsAdapter.GetData((int)_groupID);
            if (table.Rows.Count != 0)
            {
                DBDataSet.T_STUDENTRow row = table.Rows[0] as DBDataSet.T_STUDENTRow;
                numCourse.Value = row.ST_COURSE;
            }
        }

        private void tbAddStudent_Click (object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DBDataSet.T_STUDENTDataTable table = studentsAdapter.GetData((int)_groupID);
            if (table.Rows.Count != 0)
            {
                DBDataSet.T_STUDENTRow row = table.Rows[0] as DBDataSet.T_STUDENTRow;
                numCourse.Value = row.ST_COURSE;
            }
            AddEditStudent addStudentForm = new AddEditStudent(_groupID, (int)numCourse.Value);
            addStudentForm.ShowDialog();

            StudentForm_Load(sender, e);
        }

        private void tbEditStudent_Click (object sender, EventArgs e)
        {
            if (v_STUDENTSDataGridView.CurrentRow != null)
            {
                if (v_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null && v_STUDENTSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        int stID = (int)(long)v_STUDENTSDataGridView.CurrentRow.Cells[0].Value;
                        int groupID = (int)(long)v_STUDENTSDataGridView.CurrentRow.Cells[1].Value;
                        string lastName = v_STUDENTSDataGridView.CurrentRow.Cells[2].Value.ToString();
                        string firstName = v_STUDENTSDataGridView.CurrentRow.Cells[3].Value.ToString();
                        string secondName = v_STUDENTSDataGridView.CurrentRow.Cells[4].Value.ToString();
                        int course = (int)v_STUDENTSDataGridView.CurrentRow.Cells[5].Value;
                        string contract = v_STUDENTSDataGridView.CurrentRow.Cells[6].Value.ToString();
                        // 7

                        string address = v_STUDENTSDataGridView.CurrentRow.Cells[8].Value.ToString();
                        string info = v_STUDENTSDataGridView.CurrentRow.Cells[9].Value.ToString();
                        
                        int pID = (int)(long)v_STUDENTSDataGridView.CurrentRow.Cells[10].Value;

                        AddEditStudent addStudentForm = new AddEditStudent(_groupID, stID, pID,
                            lastName, firstName, secondName, course, contract, address, info);
                        addStudentForm.ShowDialog();

                        StudentForm_Load(sender, e);
                    }
                }
            }
        }

        private void tbDeleteStudent_Click (object sender, EventArgs e)
        {
            if (v_STUDENTSDataGridView.CurrentRow != null)
            {
                if (v_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null && v_STUDENTSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        studentsAdapter.Delete((int)(long)v_STUDENTSDataGridView.CurrentRow.Cells[0].Value);

                        StudentForm_Load(sender, e);
                    }
                }
            }
        }

        private void tbExcelExport_Click (object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string tmpFile = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + DateTime.Now.ToLongDateString() + "studentGroupTmp.xls";
            ExcelExport ex = new ExcelExport();
            ex.ExportToExcel(this.v_STUDENTSDataGridView, _groupName, tmpFile);

            byte[] xlsBin = File.ReadAllBytes(tmpFile);
            groupAdapter.UpdateQueryXlsBin(xlsBin, (int)_groupID);

            File.Delete(tmpFile);

            StudentForm_Load(sender, e);
        }

        private void tbReceipt_Click (object sender, EventArgs e)
        {
            if (v_STUDENTSDataGridView.CurrentRow != null)
            {
                if (v_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_STUDENTSDataGridView.CurrentRow.Cells[0].Value != null && v_STUDENTSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        int stID = (int)(long)v_STUDENTSDataGridView.CurrentRow.Cells[0].Value;
                        int groupID = (int)(long)v_STUDENTSDataGridView.CurrentRow.Cells[1].Value;
                        string lastName = v_STUDENTSDataGridView.CurrentRow.Cells[2].Value.ToString();
                        string firstName = v_STUDENTSDataGridView.CurrentRow.Cells[3].Value.ToString();
                        string secondName = v_STUDENTSDataGridView.CurrentRow.Cells[4].Value.ToString();
                        int course = (int)v_STUDENTSDataGridView.CurrentRow.Cells[5].Value;
                        string contract = v_STUDENTSDataGridView.CurrentRow.Cells[6].Value.ToString();
                        // 7

                        string address = v_STUDENTSDataGridView.CurrentRow.Cells[8].Value.ToString();
                        string info = v_STUDENTSDataGridView.CurrentRow.Cells[9].Value.ToString();

                        int pID = (int)(long)v_STUDENTSDataGridView.CurrentRow.Cells[10].Value;

                        StudentReceiptForm studentReceiptForm = new StudentReceiptForm(_groupID, stID, pID, _groupName,
                            lastName, firstName, secondName, course, contract, address, info, "Заочные группы");
                        studentReceiptForm.ShowDialog();

                        StudentForm_Load(sender, e);
                    }
                }
            }
        }

        private void btnUpdateCourse_Click(object sender, EventArgs e)
        {
            studentsAdapter.UpdateCourse((int)numCourse.Value, (int)_groupID);
            StudentForm_Load(sender, e);
        }
    }
}
