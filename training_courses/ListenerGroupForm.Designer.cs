﻿namespace training_courses
{
    partial class ListenerGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListenerGroupForm));
            this.dBDataSet = new training_courses.DBDataSet();
            this.t_GROUP_LISTENERBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.t_GROUP_LISTENERTableAdapter = new training_courses.DBDataSetTableAdapters.T_GROUP_LISTENERTableAdapter();
            this.tableAdapterManager = new training_courses.DBDataSetTableAdapters.TableAdapterManager();
            this.t_GROUP_LISTENERBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.t_GROUP_LISTENERBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tsEditLGroup = new System.Windows.Forms.ToolStripButton();
            this.tbExcel = new System.Windows.Forms.ToolStripButton();
            this.t_GROUP_LISTENERDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_LISTENERBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_LISTENERBindingNavigator)).BeginInit();
            this.t_GROUP_LISTENERBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_LISTENERDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDataSet
            // 
            this.dBDataSet.DataSetName = "DBDataSet";
            this.dBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // t_GROUP_LISTENERBindingSource
            // 
            this.t_GROUP_LISTENERBindingSource.DataMember = "T_GROUP_LISTENER";
            this.t_GROUP_LISTENERBindingSource.DataSource = this.dBDataSet;
            // 
            // t_GROUP_LISTENERTableAdapter
            // 
            this.t_GROUP_LISTENERTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.T_GROUP_LISTENERTableAdapter = this.t_GROUP_LISTENERTableAdapter;
            this.tableAdapterManager.T_GROUP_PRICESTableAdapter = null;
            this.tableAdapterManager.T_GROUPTableAdapter = null;
            this.tableAdapterManager.T_LISTENER_PRICESTableAdapter = null;
            this.tableAdapterManager.T_LISTENERTableAdapter = null;
            this.tableAdapterManager.T_PERSONTableAdapter = null;
            this.tableAdapterManager.T_ROLETableAdapter = null;
            this.tableAdapterManager.T_STUDENTTableAdapter = null;
            this.tableAdapterManager.T_SUBJECTTableAdapter = null;
            this.tableAdapterManager.T_USERTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = training_courses.DBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // t_GROUP_LISTENERBindingNavigator
            // 
            this.t_GROUP_LISTENERBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.t_GROUP_LISTENERBindingNavigator.BindingSource = this.t_GROUP_LISTENERBindingSource;
            this.t_GROUP_LISTENERBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.t_GROUP_LISTENERBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.t_GROUP_LISTENERBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.t_GROUP_LISTENERBindingNavigatorSaveItem,
            this.tsEditLGroup,
            this.tbExcel});
            this.t_GROUP_LISTENERBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.t_GROUP_LISTENERBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.t_GROUP_LISTENERBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.t_GROUP_LISTENERBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.t_GROUP_LISTENERBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.t_GROUP_LISTENERBindingNavigator.Name = "t_GROUP_LISTENERBindingNavigator";
            this.t_GROUP_LISTENERBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.t_GROUP_LISTENERBindingNavigator.Size = new System.Drawing.Size(861, 25);
            this.t_GROUP_LISTENERBindingNavigator.TabIndex = 0;
            this.t_GROUP_LISTENERBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // t_GROUP_LISTENERBindingNavigatorSaveItem
            // 
            this.t_GROUP_LISTENERBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.t_GROUP_LISTENERBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("t_GROUP_LISTENERBindingNavigatorSaveItem.Image")));
            this.t_GROUP_LISTENERBindingNavigatorSaveItem.Name = "t_GROUP_LISTENERBindingNavigatorSaveItem";
            this.t_GROUP_LISTENERBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.t_GROUP_LISTENERBindingNavigatorSaveItem.Text = "Save Data";
            this.t_GROUP_LISTENERBindingNavigatorSaveItem.Click += new System.EventHandler(this.t_GROUP_LISTENERBindingNavigatorSaveItem_Click);
            // 
            // tsEditLGroup
            // 
            this.tsEditLGroup.Image = global::training_courses.Properties.Resources.edit_128;
            this.tsEditLGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsEditLGroup.Name = "tsEditLGroup";
            this.tsEditLGroup.Size = new System.Drawing.Size(128, 22);
            this.tsEditLGroup.Text = "Просмотр группы";
            this.tsEditLGroup.Click += new System.EventHandler(this.tsEditLGroup_Click);
            // 
            // tbExcel
            // 
            this.tbExcel.Image = global::training_courses.Properties.Resources.excel;
            this.tbExcel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbExcel.Name = "tbExcel";
            this.tbExcel.Size = new System.Drawing.Size(164, 22);
            this.tbExcel.Text = "Просмотр Excel таблицы";
            this.tbExcel.ToolTipText = "Просмотр Excel таблицы";
            this.tbExcel.Click += new System.EventHandler(this.tbExcel_Click);
            // 
            // t_GROUP_LISTENERDataGridView
            // 
            this.t_GROUP_LISTENERDataGridView.AutoGenerateColumns = false;
            this.t_GROUP_LISTENERDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.t_GROUP_LISTENERDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.t_GROUP_LISTENERDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewImageColumn1});
            this.t_GROUP_LISTENERDataGridView.DataSource = this.t_GROUP_LISTENERBindingSource;
            this.t_GROUP_LISTENERDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.t_GROUP_LISTENERDataGridView.Location = new System.Drawing.Point(0, 25);
            this.t_GROUP_LISTENERDataGridView.Name = "t_GROUP_LISTENERDataGridView";
            this.t_GROUP_LISTENERDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.t_GROUP_LISTENERDataGridView.Size = new System.Drawing.Size(861, 601);
            this.t_GROUP_LISTENERDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "GL_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "GL_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "GL_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Группа";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.DataPropertyName = "GL_XLS";
            this.dataGridViewImageColumn1.HeaderText = "Excel таблица";
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Visible = false;
            // 
            // ListenerGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 626);
            this.Controls.Add(this.t_GROUP_LISTENERDataGridView);
            this.Controls.Add(this.t_GROUP_LISTENERBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ListenerGroupForm";
            this.Text = "Подготовительные группы";
            this.Load += new System.EventHandler(this.ListenerGroupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_LISTENERBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_LISTENERBindingNavigator)).EndInit();
            this.t_GROUP_LISTENERBindingNavigator.ResumeLayout(false);
            this.t_GROUP_LISTENERBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_GROUP_LISTENERDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDataSet dBDataSet;
        private System.Windows.Forms.BindingSource t_GROUP_LISTENERBindingSource;
        private DBDataSetTableAdapters.T_GROUP_LISTENERTableAdapter t_GROUP_LISTENERTableAdapter;
        private DBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator t_GROUP_LISTENERBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton t_GROUP_LISTENERBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView t_GROUP_LISTENERDataGridView;
        private System.Windows.Forms.ToolStripButton tsEditLGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ToolStripButton tbExcel;
    }
}