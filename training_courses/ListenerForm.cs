﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class ListenerForm : Form
    {
        private long _groupID;
        private string _groupName;

        private DBDataSetTableAdapters.T_LISTENERTableAdapter listenerAdapter;
        private DBDataSetTableAdapters.T_GROUP_LISTENERTableAdapter groupListenerAdapter;

        public ListenerForm (long groupID, string groupName)
        {
            InitializeComponent();

            this._groupID = groupID;
            this._groupName = groupName;

            listenerAdapter = new DBDataSetTableAdapters.T_LISTENERTableAdapter();
            listenerAdapter.ClearBeforeFill = true;

            groupListenerAdapter = new DBDataSetTableAdapters.T_GROUP_LISTENERTableAdapter();
            groupListenerAdapter.ClearBeforeFill = true;

            listenerAdapter.Fill(dBDataSet.T_LISTENER);
            groupListenerAdapter.Fill(dBDataSet.T_GROUP_LISTENER);

            this.Validate();

            this.listenerAdapter.Update(this.dBDataSet);
            this.groupListenerAdapter.Update(this.dBDataSet);

            this.dBDataSet.T_LISTENER.AcceptChanges();
            this.dBDataSet.T_GROUP_LISTENER.AcceptChanges();
        }

        private void Listeners_Load (object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.V_LISTENERS' table. You can move, or remove it, as needed.
            this.v_LISTENERSTableAdapter.FillByGLID(this.dBDataSet.V_LISTENERS, (int)_groupID);

            if (AccessLevel.level == AccessLevel.Level.Courses)
            {
                tbExcelExport.Visible = tbReport.Visible = tbReceipt.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Accounting)
            {
                tbReceipt.Visible = true;
                tbAddListener.Visible = tbEditListener.Visible = tbDeleteListener.Visible = false;
                tbReport.Visible = false;
                tbExcelExport.Visible = false;
            }

            if (AccessLevel.level == AccessLevel.Level.Manager)
            {
                tbReceipt.Visible = false;
                tbAddListener.Visible = tbEditListener.Visible = tbDeleteListener.Visible = false;
                tbReport.Visible = true;
                tbExcelExport.Visible = true;
            }

            if (AccessLevel.level == AccessLevel.Level.Engineer)
            {
                tbReceipt.Visible = false;
                tbAddListener.Visible = tbEditListener.Visible = tbDeleteListener.Visible = true;
                tbReport.Visible = false;
                tbExcelExport.Visible = true;
            }
        }

        private void tbAddListener_Click (object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            AddEditListener addListenerForm = new AddEditListener(_groupID);
            addListenerForm.ShowDialog();

            Listeners_Load(sender, e);
            
        }

        private void tbDeleteListener_Click (object sender, EventArgs e)
        {
            if (v_LISTENERSDataGridView.CurrentRow != null)
            {
                if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null && v_LISTENERSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        listenerAdapter.Delete((int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[0].Value);

                        Listeners_Load(sender, e);
                    }
                }
            }
        }

        private void tbEditListener_Click (object sender, EventArgs e)
        {
            if (v_LISTENERSDataGridView.CurrentRow != null)
            {
                if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null && v_LISTENERSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        int lID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[0].Value;
                        int pID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[1].Value;
                        int sID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[2].Value;
                        string lastName = v_LISTENERSDataGridView.CurrentRow.Cells[3].Value.ToString();
                        string firstName = v_LISTENERSDataGridView.CurrentRow.Cells[4].Value.ToString();
                        string secondName = v_LISTENERSDataGridView.CurrentRow.Cells[5].Value.ToString();                        
                        string edu = v_LISTENERSDataGridView.CurrentRow.Cells[6].Value.ToString();
                        string contract = v_LISTENERSDataGridView.CurrentRow.Cells[7].Value.ToString();
                        int time = (int)v_LISTENERSDataGridView.CurrentRow.Cells[8].Value;
                        int realTime = (int)v_LISTENERSDataGridView.CurrentRow.Cells[9].Value;
                        int glID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[10].Value;

                        AddEditListener addListenerForm = new AddEditListener(_groupID, lID, pID, sID,
                            lastName, firstName, secondName, edu, contract, time, realTime);
                        addListenerForm.ShowDialog();

                        Listeners_Load(sender, e);
                    }
                }
            }
        }

        private void tbExcelExport_Click (object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string tmpFile = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + DateTime.Now.ToLongDateString() + "listenerGroupTmp.xls";
            ExcelExport ex = new ExcelExport();
            ex.ExportToExcel(this.v_LISTENERSDataGridView, _groupName, tmpFile);

            byte[] xlsBin = File.ReadAllBytes(tmpFile);
            groupListenerAdapter.UpdateQueryExcelFile(xlsBin, (int)_groupID);

            File.Delete(tmpFile);

            Listeners_Load(sender, e);
        }

        private void tbReport_Click (object sender, EventArgs e)
        {
            if (v_LISTENERSDataGridView.CurrentRow != null)
            {
                if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null && v_LISTENERSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        int lID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[0].Value;
                        int pID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[1].Value;
                        int sID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[2].Value;
                        string lastName = v_LISTENERSDataGridView.CurrentRow.Cells[3].Value.ToString();
                        string firstName = v_LISTENERSDataGridView.CurrentRow.Cells[4].Value.ToString();
                        string secondName = v_LISTENERSDataGridView.CurrentRow.Cells[5].Value.ToString();
                        string edu = v_LISTENERSDataGridView.CurrentRow.Cells[6].Value.ToString();
                        string contract = v_LISTENERSDataGridView.CurrentRow.Cells[7].Value.ToString();
                        int time = (int)v_LISTENERSDataGridView.CurrentRow.Cells[8].Value;
                        int realTime = (int)v_LISTENERSDataGridView.CurrentRow.Cells[9].Value;
                        int glID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[10].Value;

                        ContractForm contractForm = new ContractForm(lID, pID, lastName, firstName, secondName, edu, contract);
                        contractForm.ShowDialog();

                        Listeners_Load(sender, e);
                    }
                }
            }            
        }

        private void tbReceipt_Click (object sender, EventArgs e)
        {
            if (v_LISTENERSDataGridView.CurrentRow != null)
            {
                if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (v_LISTENERSDataGridView.CurrentRow.Cells[0].Value != null && v_LISTENERSDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        int lID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[0].Value;
                        int pID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[1].Value;
                        int sID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[2].Value;
                        string lastName = v_LISTENERSDataGridView.CurrentRow.Cells[3].Value.ToString();
                        string firstName = v_LISTENERSDataGridView.CurrentRow.Cells[4].Value.ToString();
                        string secondName = v_LISTENERSDataGridView.CurrentRow.Cells[5].Value.ToString();
                        string edu = v_LISTENERSDataGridView.CurrentRow.Cells[6].Value.ToString();
                        string contract = v_LISTENERSDataGridView.CurrentRow.Cells[7].Value.ToString();
                        int time = (int)v_LISTENERSDataGridView.CurrentRow.Cells[8].Value;
                        int realTime = (int)v_LISTENERSDataGridView.CurrentRow.Cells[9].Value;
                        int glID = (int)(long)v_LISTENERSDataGridView.CurrentRow.Cells[10].Value;

                        ListenerReceiptForm listenerReceiptForm = new ListenerReceiptForm(lID, pID, _groupName, lastName, firstName, secondName, edu, contract, glID);
                        listenerReceiptForm.ShowDialog();

                        Listeners_Load(sender, e);
                    }
                }
            }
        }
    }
}
