﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    class QueryDatabase
    {
        public static String cs = Properties.Settings.Default.ConnectionString;

        public static void buildConnectionString (string user, string password, object dbPath)
        {
            Properties.Settings.Default.ConnectionString =
                "character set=WIN1251;data source=localhost;" +
                // TODO add db path selction
                "initial catalog=" + dbPath +
                ";user id=" + user + ";password=" + password;
            refreshConnectionString();
        }

        public static void refreshConnectionString()
        {
            cs = Properties.Settings.Default.ConnectionString;
        }

        public static DialogResult tryConnect(String str)
        {
            try
            {
                FbConnection conn = new FbConnection(str);
                conn.Open();
                conn.Close();
                return DialogResult.OK;
            }
            catch
            {
                return DialogResult.Cancel;
            }
        }

        public static DataTable getData(string command)
        {
            try
            {
                FbDataAdapter DA = new FbDataAdapter(command, cs);
                DataTable DT = new DataTable();
                DA.Fill(DT);

                return DT;
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails.Show("Невозможно выполнить выборку", "Ошибка выборки", ex.Message);
                return null;
            }
        }

        public static string getValue(string command)
        {
            try
            {
                FbDataAdapter DA = new FbDataAdapter(command, cs);
                DataTable DT = new DataTable();
                DA.Fill(DT);
                DataRow row = DT.Rows[0];
                return row[0].ToString();
            }
            catch
            {
                return null;
            }
        }

        public static bool dataManipulation(string command)
        {
            try
            {
                FbConnection Conn = new FbConnection(cs);
                Conn.Open();
                FbTransaction Trans = Conn.BeginTransaction();
                FbCommand fbcom = new FbCommand(command, Conn, Trans);
                fbcom.ExecuteNonQuery();
                fbcom.Transaction.Commit();
                Conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public static bool dataManipulationWithoutExc(string command)
        {
            try
            {
                FbConnection Conn = new FbConnection(cs);
                Conn.Open();
                FbTransaction Trans = Conn.BeginTransaction();
                FbCommand fbcom = new FbCommand(command, Conn, Trans);
                fbcom.ExecuteNonQuery();
                fbcom.Transaction.Commit();
                Conn.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static String insertData (string command)
        {
            try
            {
                FbConnection Conn = new FbConnection(cs);
                Conn.Open();
                FbTransaction Trans = Conn.BeginTransaction();
                FbCommand fbcom = new FbCommand(command, Conn, Trans);
                String res = fbcom.ExecuteScalar().ToString();
                fbcom.Transaction.Commit();
                Conn.Close();
                return res;
            }
            catch
            {
                MessageBox.Show("Невозможно выполнить вставку данных", "Ошибка вставки",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show(ex.ToString());
                return "-1";
            }
        }
    }
}
