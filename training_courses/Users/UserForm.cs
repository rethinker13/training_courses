﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class UserForm : Form
    {
        public UserForm ()
        {
            InitializeComponent();
        }

        private void t_USERBindingNavigatorSaveItem_Click (object sender, EventArgs e)
        {
            try
            {
                if (!Global.UserCheck())
                {
                    MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                       "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                this.Validate();
                this.t_USERBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dBDataSet);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    Application.ProductName, ex.Message);
                message.ShowDialog();
            }
        }

        private void UserForm_Load (object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_ROLE' table. You can move, or remove it, as needed.
            this.t_ROLETableAdapter.Fill(this.dBDataSet.T_ROLE);
            // TODO: This line of code loads data into the 'dBDataSet.T_USER' table. You can move, or remove it, as needed.
            this.t_USERTableAdapter.Fill(this.dBDataSet.T_USER);

        }

        private void bindingNavigatorAddNewItem_Click (object sender, EventArgs e)
        {
            return;
        }

        private void tbAddUser_Click (object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            AddUserForm addForm = new AddUserForm();
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("insert into t_user(user_name, role_id, user_password_hash) values(");
                sb.Append("'");
                sb.Append(addForm.user);
                sb.Append("'");
                sb.Append(",");
                sb.Append("'");
                sb.Append(addForm.role);
                sb.Append("'");
                sb.Append(",");
                sb.Append("'");
                sb.Append(addForm.savedPasswordHash);
                sb.Append("'");
                sb.Append(")");
                if (QueryDatabase.dataManipulation(sb.ToString()))
                {
                    t_USERBindingNavigatorSaveItem_Click(sender, e);
                    MessageBox.Show("Пользователь успешно добавлен!",
                        "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UserForm_Load(sender, e);
                }
            }
        }

        private void toolStripButton1_Click (object sender, EventArgs e)
        {
            if (t_USERDataGridView.SelectedRows.Count == 0)
                return;

            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            long userId = (long)t_USERDataGridView.SelectedRows[0].Cells[0].Value;
            string userName = (string)t_USERDataGridView.SelectedRows[0].Cells[1].Value;
            StringBuilder sb = new StringBuilder();
            sb.Append("select user_name, role_id, user_password_hash from t_user where upper(t_user.user_id) = ");
            sb.Append("'");
            sb.Append(userId);
            sb.Append("'");
            DataTable data = QueryDatabase.getData(sb.ToString());

            DataRow row = data.Rows[0];
            long roleId = (long)row["role_id"];
            AddUserForm addForm = new AddUserForm(userName, roleId);
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                sb.Clear();
                sb.Append("update t_user set role_id = ");
                sb.Append("'");
                sb.Append(addForm.role);
                sb.Append("'");

                sb.Append(" where user_id = ");
                sb.Append("'");
                sb.Append(userId);
                sb.Append("'");
                if (QueryDatabase.dataManipulation(sb.ToString()))
                {
                    t_USERBindingNavigatorSaveItem_Click(sender, e);
                    MessageBox.Show("Пользователь успешно отредактирован!",
                        "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UserForm_Load(sender, e);
                }
            }
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (!Global.UserCheck())
            {
                MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                   "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);                
                return;
            }
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if(t_USERDataGridView.CurrentRow != null)
            {
                if (t_USERDataGridView.CurrentRow.Cells[0].Value != null)
                {
                    if (t_USERDataGridView.CurrentRow.Cells[0].Value != null && t_USERDataGridView.CurrentRow.Cells[0].Value.ToString() != "-1")
                    {
                        if (!Global.UserCheck())
                        {
                            MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                               "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        t_USERTableAdapter.Delete((int)(long)t_USERDataGridView.CurrentRow.Cells[0].Value);

                        UserForm_Load(sender, e);
                    }
                }
            }
        }
    }
}
