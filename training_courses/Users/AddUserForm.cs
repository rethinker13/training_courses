﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class AddUserForm : Form
    {
        private string userName;
        private long roleId;

        private bool editMode = false;

        public string user { get; private set; }
        public string savedPasswordHash { get; private set; }
        public long role { get; private set; }

        public AddUserForm ()
        {
            InitializeComponent();
        }

        public AddUserForm (string userName, long roleId) : this()
        {
            this.userName = userName;
            this.roleId = roleId;
            editMode = true;
            txtUser.Text = this.userName;
            txtUser.Enabled = false;
            txtPassword.Enabled = false;
            txtConfirmPassword.Enabled = false;
        }

        private void btnOk_Click (object sender, EventArgs e)
        {
            if (!editMode)
            {
                if (String.IsNullOrEmpty(txtUser.Text))
                {
                    MessageBox.Show("Пользователь не может быть пустым!",
                       Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (!Regex.IsMatch(txtUser.Text, @"^[a-zA-Z0-9_а-яА-Я]+$"))
                {
                    MessageBox.Show("Имя пользователя содержит недопустимые символы!",
                       Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (String.IsNullOrEmpty(txtPassword.Text))
                {
                    MessageBox.Show("Пароль не может быть пустым!",
                       Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (txtPassword.Text.Length < 3)
                {
                    MessageBox.Show("Слишком короткий пароль!",
                       Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (txtPassword.Text != txtConfirmPassword.Text)
                {
                    MessageBox.Show("Пароли не совпадают!",
                       Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                
                if (cmbRole.SelectedValue == null)
                {
                    MessageBox.Show("Роль не выбрана!",
                       Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                user = txtUser.Text;

                string password = txtPassword.Text;

                role = (long)cmbRole.SelectedValue;

                byte[] salt;
                new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

                var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
                byte[] hash = pbkdf2.GetBytes(20);

                byte[] hashBytes = new byte[36];
                Array.Copy(salt, 0, hashBytes, 0, 16);
                Array.Copy(hash, 0, hashBytes, 16, 20);

                savedPasswordHash = Convert.ToBase64String(hashBytes);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                role = (long)cmbRole.SelectedValue;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click (object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void AddUserForm_Load (object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_ROLE' table. You can move, or remove it, as needed.
            this.t_ROLETableAdapter.Fill(this.dBDataSet.T_ROLE);

            cmbRole.SelectedValue = roleId;
        }
    }
}
