﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training_courses
{
    class User
    {
        private string _name;
        private byte[] _password;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public byte[] Password
        {
            get
            {
                return _password;
            }
        }

        public User(string name, byte[] password)
        {
            this._name = name;
            this._password = password;
        }

    }
}
