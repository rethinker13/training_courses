﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training_courses
{
    class AccessLevel
    {
        public enum Level
        {
            Admin,
            Courses,
            Students,
            Manager,
            Engineer,
            Accounting
        }

        public static Level level;
    }
}
