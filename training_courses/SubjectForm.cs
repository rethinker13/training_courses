﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class SubjectForm : Form
    {
        public SubjectForm ()
        {
            InitializeComponent();
        }

        private void t_SUBJECTBindingNavigatorSaveItem_Click (object sender, EventArgs e)
        {
            try
            {
                if (!Global.UserCheck())
                {
                    MessageBox.Show("Пользователь не прошел проверку! Операция отклонена!",
                       "Ошибка верификации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                this.Validate();
                this.t_SUBJECTBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dBDataSet);
            }
            catch (Exception ex)
            {
                MessageBoxWithDetails message = new MessageBoxWithDetails("Пожалуйста, проверьте корректность введенных данных!",
                    System.Windows.Forms.Application.ProductName, ex.Message);
                message.ShowDialog();
            }
        }

        private void SubjectForm_Load (object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBDataSet.T_SUBJECT' table. You can move, or remove it, as needed.
            this.t_SUBJECTTableAdapter.Fill(this.dBDataSet.T_SUBJECT);

        }
    }
}
