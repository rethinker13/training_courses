﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace training_courses
{
    public partial class AllStudentPricesForm : Form
    {
        private GroupType.Type groupType;

        public AllStudentPricesForm()
        {
            InitializeComponent();
        }

        public AllStudentPricesForm(GroupType.Type type) : this()
        {
            this.groupType = type;
        }

        private void t_GROUP_PRICESBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.t_GROUP_PRICESBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDataSet);

        }

        private void StudentPricesForm_Load(object sender, EventArgs e)
        {
            if (groupType == GroupType.Type.Student)
            {
                tGROUPBindingSource.Filter = "IS_SVE = 0 AND IS_MASTERS = 0 AND T_GROUP_EXT = 1";
                this.t_GROUPTableAdapter.FillExtGroups(this.dBDataSet.T_GROUP);
                this.t_GROUP_PRICESTableAdapter.FillByExtGroups(this.dBDataSet.T_GROUP_PRICES);
            }
            else if (groupType == GroupType.Type.FullStudent)
            {
                tGROUPBindingSource.Filter = "IS_SVE = 0 AND IS_MASTERS = 0 AND T_GROUP_EXT = 0";
                this.t_GROUPTableAdapter.FillByFullGroups(this.dBDataSet.T_GROUP);
                this.t_GROUP_PRICESTableAdapter.FillByFullGroups(this.dBDataSet.T_GROUP_PRICES);
            }
            else if (groupType == GroupType.Type.Master)
            {
                tGROUPBindingSource.Filter = "IS_SVE = 0 AND IS_MASTERS = 1 AND T_GROUP_EXT = 0";
                this.t_GROUPTableAdapter.FillMasters(this.dBDataSet.T_GROUP);
                this.t_GROUP_PRICESTableAdapter.FillByMasters(this.dBDataSet.T_GROUP_PRICES);
            }
            else if (groupType == GroupType.Type.SVE)
            {
                tGROUPBindingSource.Filter = "IS_SVE = 1 AND IS_MASTERS = 0 AND T_GROUP_EXT = 0";
                this.t_GROUPTableAdapter.FillSVE(this.dBDataSet.T_GROUP);
                this.t_GROUP_PRICESTableAdapter.FillBySVE(this.dBDataSet.T_GROUP_PRICES);
            }
            
        }
    }
}
